/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#pragma once
 // TODO: Review



#include <string>
#include <memory>
#include "NtpNtsInterface.h"

class NtsUnicastServiceImpl;	// forward declaration

class NtsUnicastService
{

public:

	NtsUnicastService();
	~NtsUnicastService();
	NtsUnicastService (NtsUnicastService const &) = delete; // prevent copying of the NTS service
	NtsUnicastService &operator=(NtsUnicastService const &) = delete;

	void initialization(std::string const &globalConfigFile);
	void reset(); // delete all NTS data and restart the service
	void shutdown();

	// for Client and Server
	void processingNtpMessage(NtpNtsInterface &receivedPackage, NtpNtsInterface &newResponsePackage);

	// for Client only
	void processingReceivedNtpMessage(NtpNtsInterface &receivedPackage);
	void createRequestMessage(NtpNtsInterface &newResponsePackage);

	bool isInit();
	bool isClientMode();
	bool isServerMode();

private:
	const std::unique_ptr<NtsUnicastServiceImpl> m_ntsUnicastServiceImpl;
};

