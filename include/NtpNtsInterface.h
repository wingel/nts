/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#pragma once
 // TODO: Review

#include <iostream>
#include <string>
#include <vector>

/*
 * NtpNtsInterface v5 (07.02.2017)
 */

/*
 * todo: Redesign der Klasse
 *
 * Umwandeln in ein Kontext-Objekt: NtpPacketCtx
 * 	 - NTP-Funktionen und NTS-Funktionen voneinander trennen (z.B. durch separate Objekte)
 */


class NtpNtsInterface
{
public:

	NtpNtsInterface() = default;
	virtual ~NtpNtsInterface() = default;

	virtual double getTransmitTimestamp() const = 0;
	virtual double getOriginTimestamp() const = 0;
	virtual std::string getIpAddress() const = 0;
	virtual void updateTransmitTimestamp() = 0;

	virtual unsigned int getExtensionFieldCount() const = 0;
	virtual unsigned short getExtensionFieldType(unsigned int index) const = 0; 						// index beginnt bei 0
	virtual std::vector<unsigned char> getExtensionFieldValue(unsigned int index) const = 0; 			// index beginnt bei 0
	virtual void addExtensionField(unsigned short fieldType, std::vector<unsigned char> value) = 0;
	virtual void delExtensionField(unsigned int index) = 0;

	virtual std::vector<unsigned char> getSerializedPackage() const = 0;

	// additional package information (is used by NTS)
	virtual void setUseMessageForTimeSync(bool state) = 0;
	virtual bool getUseMessageForTimeSync() const = 0;

	virtual void setSendResponseMessage(bool state) = 0;
	virtual bool getSendResponseMessage() const = 0;

	virtual void setGenerateResponseMessage(bool state) = 0;
	virtual bool getGenerateResponseMessage() const = 0;
};


// END OF FILE
