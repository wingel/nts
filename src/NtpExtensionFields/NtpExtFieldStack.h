/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#pragma once

#include <vector>
#include "NtpExtensionField.h"

class NtpExtFieldStack
{
public:
	NtpExtFieldStack() = default;
	NtpExtFieldStack(std::vector<unsigned char> const &serializedData);
	~NtpExtFieldStack() = default;

	unsigned int getStackSize() const;
	const NtpExtensionField & getExtField(unsigned int index) const;
	unsigned int findExtField(NtpExtFieldType extFieldType, unsigned int startIndex = 1) const;
	void addExtField(NtpExtensionField const &extField);
	void clear();
	
	unsigned int countExtFieldType(NtpExtFieldType extFieldType) const;

	std::vector<unsigned char> getSerializedData() const;
	void fromSerializedData(std::vector<unsigned char> const &serializedData);

private:
	const size_t c_extFieldHeaderSize = 4;
	const size_t c_extFieldMinSize = 16; // see RFC 7822
	std::vector<NtpExtensionField> m_extFieldStack;
};

