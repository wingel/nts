#include "ExtF_NtsCookie.h"
#include "../Logger/Logger.h"

/**
* @brief	Clears the content and sets the Extension Field header according the rules of this type.
*/
void ExtField::NtsCookie::format(NtpExtensionField &ntpExtField)
{
	ntpExtField.clear();
	ntpExtField.setFieldType(NtpExtFieldType::NTS_Cookie);
}


/**
* @brief	Checks if the Extension Field is valid and compliant with the NTS specification.
*/
bool ExtField::NtsCookie::isValid(NtpExtensionField const &ntpExtField)
{
	bool status = true;

	if (ntpExtField.getFieldType() != NtpExtFieldType::NTS_Cookie)
	{
		LOG_ERROR("wrong Field Type of the Extension Field: '{}'", ntpExtField.getFieldTypeVal());
		status = false;
	}

	if (ntpExtField.getValueLength() <= 0)
	{
		LOG_ERROR("invalid length of the Value field: {} bytes", ntpExtField.getValueLength());
		status = false;
	}
	return status;
}


unsigned short ExtField::NtsCookie::getCookieSize(NtpExtensionField const &ntpExtField)
{
	return ntpExtField.getValueLength();
}


const std::vector<unsigned char> & ExtField::NtsCookie::getCookie(NtpExtensionField const &ntpExtField)
{
	return ntpExtField.getValue();
}


/**
* @brief	Sets the Cookie as a copy of the delivered parameter.
*/
void ExtField::NtsCookie::setCookie(NtpExtensionField &ntpExtField, std::vector<unsigned char> const &cookie)
{
	ntpExtField.setValue(cookie);
}
