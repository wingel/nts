#include "ExtF_UniqueIdentifier.h"
#include "../Logger/Logger.h"

void ExtField::UniqueIdentifier::format(NtpExtensionField &ntpExtField)
{
	ntpExtField.clear();
	ntpExtField.setFieldType(NtpExtFieldType::Unique_Identifier);
}

bool ExtField::UniqueIdentifier::isValid(NtpExtensionField const &ntpExtField)
{
	bool status = true;

	if (ntpExtField.getFieldType() != NtpExtFieldType::Unique_Identifier)
	{
		LOG_WARN("ExtF_UniqueIdentifier: wrong type (is '{}')", ntpExtField.getFieldTypeVal());
		status = false;
	}

	if (ntpExtField.getValueLength() > c_maxValueSize)
	{
		LOG_WARN("ExtF_UniqueIdentifier: wrong length (max: {})", c_maxValueSize);
		status = false;
	}

	if (ntpExtField.getValueLength() <= 0)
	{
		LOG_WARN("ExtF_UniqueIdentifier: wrong length (is zero)");
		status = false;
	}
	return status;
}

unsigned short ExtField::UniqueIdentifier::getUniqueIdSize(NtpExtensionField const &ntpExtField)
{
	return ntpExtField.getValueLength();
}

const std::vector<unsigned char> & ExtField::UniqueIdentifier::getUniqueId(NtpExtensionField const &ntpExtField)
{
	return ntpExtField.getValue();
}

void ExtField::UniqueIdentifier::setUniqueId(NtpExtensionField &ntpExtField, std::vector<unsigned char> const &uniqueId)
{
	ntpExtField.setValue(uniqueId);
}
