#include "ExtF_NtsAuthAndEncEF.h"
#include "../Logger/Logger.h"
#include "../AEAD.h"

#ifndef min
#define min(a,b)  (((a) < (b)) ? (a) : (b))
#endif

/**
* @brief	Clears the content and sets the Extension Field header according the rules of this type.
*/
void ExtField::NtsAuthAndEncEF::format(NtpExtensionField &ntpExtField)
{
	ntpExtField.clear();
	ntpExtField.setFieldType(NtpExtFieldType::NTS_Authenticator_and_Encrypted_Extension_Fields);
}


/**
* @brief	Checks if the Extension Field is valid and compliant with the NTS specification.
*/
bool ExtField::NtsAuthAndEncEF::isValid(NtpExtensionField const &ntpExtField)
{
	bool status = true;

	if (ntpExtField.getFieldType() != NtpExtFieldType::NTS_Authenticator_and_Encrypted_Extension_Fields)
	{
		LOG_ERROR("wrong Field Type of the Extension Field: '{}'", ntpExtField.getFieldTypeVal());
		status = false;
	}
	
	// TODO: check Field length, min Nonce size, min cipher size, padding size

	return status;
}


/**
* @brief	Performs an AEAD operation and fills all fields in this extension.
*
* @param [in]	plaintext	Contains all NTP Extension Fields to be encrypted.
*
* @return	Returns 'true' on success and 'false' on error
*/
bool ExtField::NtsAuthAndEncEF::performAead(NtpExtensionField & ntpExtField,
											AeadAlgorithm aeadAlgorithm, 
											std::vector<unsigned char> const & key, 
											std::vector<unsigned char> const & plaintext, 
											std::vector<unsigned char> const & nonce, 
											std::vector<unsigned char> const & associatedData)
{
	// perform some checks
	if (nonce.size() == 0)
	{
		LOG_ERROR("invalid size of 'nonce'");
		return false;
	}

	if (associatedData.size() == 0)
	{
		LOG_ERROR("invalid size of 'associatedData'");
		return false;
	}

	// AEAD operation
	std::vector<unsigned char> aeadOut;
	if (AEAD::directEncrypt(aeadAlgorithm, key, plaintext, aeadOut, nonce, associatedData) == false)
	{
		LOG_ERROR("AEAD operation failed");
		return false;
	}

	unsigned short noncePadding = 0;
	unsigned short ciphertextPadding = 0;

	if (nonce.size() % 4 != 0)
	{
		noncePadding = 4 - (nonce.size() % 4);
	}

	if (aeadOut.size() % 4 != 0)
	{
		ciphertextPadding = 4 - (aeadOut.size() % 4);
	}

	unsigned int N_MAX = 0;
	if (AEAD::isSupported(aeadAlgorithm) == true)
	{
		N_MAX = AEAD::getMaxNonceLength(aeadAlgorithm);
	}
	else
	{
		LOG_ERROR("AeadAlgorithm is not supported");
		return false;
	}

	size_t N_LEN = nonce.size() + noncePadding;
	size_t N_REQ = min(16, N_MAX);

	if (N_REQ % 4 != 0) // add padding
	{
		N_REQ += 4 - (N_REQ % 4);
	}

	size_t additionalPadding = 0;


	if (N_LEN >= N_REQ)
	{
		additionalPadding = 0;
	}
	else
	{
		additionalPadding = N_REQ - N_LEN;
	}

	std::vector<unsigned char> content(4);
	size_t contentSize;
	contentSize = 4 + N_LEN + aeadOut.size() + ciphertextPadding + additionalPadding; // 4 bytes: both length fields
	content.reserve(contentSize);

	
	// write 'Nonce Length'
	size_t nonceLength = static_cast<unsigned short>(nonce.size());
	content.at(0) |= (nonceLength >> 8) & 0xFF;
	content.at(1) |= nonceLength & 0xFF;

	// write 'Ciphertext Length'
	size_t ciphertextLength = static_cast<unsigned short>(aeadOut.size());
	content.at(2) |= (ciphertextLength >> 8) & 0xFF;
	content.at(3) |= ciphertextLength & 0xFF;


	// write 'Nonce'
	content.insert(content.end(), nonce.begin(), nonce.end());
	if (noncePadding != 0)
	{
		content.resize(content.size() + noncePadding);
	}

	// write 'Ciphertext'
	content.insert(content.end(), aeadOut.begin(), aeadOut.end());
	if (ciphertextPadding != 0)
	{
		content.resize(content.size() + ciphertextPadding);
	}
	

	// write 'Additional Padding'
	if (additionalPadding != 0)
	{
		content.resize(content.size() + additionalPadding);
	}


	// TODO: direkt beschreiben anstatt copy?
	// fill extension field
	// LOG_INFO("size: " << content.size() << "\t  capacity: " << content.capacity();
	ntpExtField.setValue(content);

	return true;
}


/**
* @brief	Performs an AEAD operation and fills all fields in this extension.
*
* @param [in]	plaintext	Contains all NTP Extension Fields to be encrypted.
*
* @return	Returns 'true' on success and 'false' on error
*/
bool ExtField::NtsAuthAndEncEF::performAead(NtpExtensionField & ntpExtField, 
											unsigned short aeadAlgorithm, 
											std::vector<unsigned char> const & key, 
											std::vector<unsigned char> const & plaintext, 
											std::vector<unsigned char> const & nonce, 
											std::vector<unsigned char> const & associatedData)
{
	return performAead(ntpExtField, static_cast<AeadAlgorithm>(aeadAlgorithm), key, plaintext, nonce, associatedData);
}


/**
* @brief	Performs an AEAD operation and fills all fields in this extension.
*
* @return	Returns 'true' on success and 'false' on error
*/
bool ExtField::NtsAuthAndEncEF::performAead(NtpExtensionField & ntpExtField, 
											AeadAlgorithm aeadAlgorithm, 
											std::vector<unsigned char> const & key, 
											std::vector<unsigned char> const & nonce, 
											std::vector<unsigned char> const & associatedData)
{
	return performAead(ntpExtField, aeadAlgorithm, key, std::vector<unsigned char>(), nonce, associatedData);
}


/**
* @brief	Performs an AEAD operation and fills all fields in this extension.
*
* @return	Returns 'true' on success and 'false' on error
*/
bool ExtField::NtsAuthAndEncEF::performAead(NtpExtensionField & ntpExtField, 
											unsigned short aeadAlgorithm, 
											std::vector<unsigned char> const & key, 
											std::vector<unsigned char> const & nonce, 
											std::vector<unsigned char> const & associatedData)
{
	return performAead(ntpExtField, static_cast<AeadAlgorithm>(aeadAlgorithm), key, std::vector<unsigned char>(), nonce, associatedData);
}


unsigned short ExtField::NtsAuthAndEncEF::getNonceLength(NtpExtensionField const & ntpExtField)
{
	std::vector<unsigned char> const &rawContent = ntpExtField.getValue();
	size_t contentSize = rawContent.size();

	if (contentSize < 2) // size of the 'Nonce Length' field (2 bytes)
	{
		LOG_ERROR("invalid size of the 'Nonce Length' field");
		return 0;
	}

	unsigned short nonceLength = (rawContent.at(0) << 8) | rawContent.at(1);
	return nonceLength;
}


/*
  returns the nonce without any padding
*/
std::vector<unsigned char> ExtField::NtsAuthAndEncEF::getNonce(NtpExtensionField const & ntpExtField)
{
	std::vector<unsigned char> const &rawContent = ntpExtField.getValue();
	size_t contentSize = rawContent.size();

	unsigned short nonceLength = getNonceLength(ntpExtField);
	unsigned short dataLength = 4 + nonceLength; // 4 bytes: skip both length fields

	if (contentSize < dataLength)
	{
		LOG_ERROR("invalid nonce length");
		return std::vector<unsigned char>();
	}

	std::vector<unsigned char> nonce(rawContent.begin() + 4, rawContent.begin() + dataLength);
	return nonce;
}


unsigned short ExtField::NtsAuthAndEncEF::getCiphertextLength(NtpExtensionField const & ntpExtField)
{
	std::vector<unsigned char> const &rawContent = ntpExtField.getValue();
	size_t contentSize = rawContent.size();

	if (contentSize < 4) // 4 bytes: both length fields
	{
		LOG_ERROR("invalid size of the 'Ciphertext Length' field");
		return 0;
	}

	unsigned short ciphertextLength = (rawContent.at(2) << 8) | rawContent.at(3);
	return ciphertextLength;
}


std::vector<unsigned char> ExtField::NtsAuthAndEncEF::getCiphertext(NtpExtensionField const & ntpExtField)
{
	std::vector<unsigned char> const &rawContent = ntpExtField.getValue();
	size_t contentSize = rawContent.size();

	unsigned short nonceLength = getNonceLength(ntpExtField);
	unsigned short noncePadding = 0;

	if (nonceLength % 4 != 0)
	{
		noncePadding = 4 - (nonceLength % 4);
	}

	unsigned short ciphertextLength = getCiphertextLength(ntpExtField);
	unsigned short dataLength = 4 + nonceLength + noncePadding + ciphertextLength; // 4 bytes: skip both length fields

	if (contentSize < dataLength)
	{
		LOG_ERROR("invalid size of the 'Ciphertext' field");
		return std::vector<unsigned char>();
	}

	unsigned int startPos = 4 + nonceLength + noncePadding;
	unsigned int endPos = dataLength;

	std::vector<unsigned char> ciphertext(rawContent.begin() + startPos, rawContent.begin() + endPos);
	return ciphertext;
}


unsigned short ExtField::NtsAuthAndEncEF::getPaddingLength(NtpExtensionField const & ntpExtField)
{
	std::vector<unsigned char> const &rawContent = ntpExtField.getValue();
	size_t contentSize = rawContent.size();

	unsigned short nonceLength = getNonceLength(ntpExtField);
	unsigned short noncePadding = 0;

	if (nonceLength % 4 != 0)
	{
		noncePadding = 4 - (nonceLength % 4);
	}

	unsigned short ciphertextLength = getCiphertextLength(ntpExtField);
	unsigned short ciphertextPadding = 0;

	if (ciphertextLength % 4 != 0)
	{
		ciphertextPadding = 4 - (ciphertextLength % 4);
	}

	unsigned short dataLength = 4 + nonceLength + noncePadding + ciphertextLength + ciphertextPadding; // 4 bytes: skip both length fields

	if (contentSize < dataLength)
	{
		LOG_ERROR("invalid size of the extension field");
		return 0;
	}

	return static_cast<unsigned short>(contentSize - dataLength);
}