#include "NtpExtFieldStack.h"
#include "../Logger/Logger.h"

NtpExtFieldStack::NtpExtFieldStack(std::vector<unsigned char> const & serializedData)
{
	fromSerializedData(serializedData);
}


unsigned int NtpExtFieldStack::getStackSize() const
{
	return static_cast<unsigned int>(m_extFieldStack.size());
}


/**
* @param [in]	index	Index of the Extension Field in the stack (starts at 1).
*/
const NtpExtensionField & NtpExtFieldStack::getExtField(unsigned int index) const
{
	if ((index < 1) || (index > m_extFieldStack.size()))
	{
		LOG_ERROR("index is out of range");
		return m_extFieldStack.at(0);
	}
	return m_extFieldStack.at(index - 1);
}


/**
* @param [in]	type		The Field Type to search for.
* @param [in]	startIndex	The first index be considered in the search (starts at 1).
*
* @return	The index of the first match or zero if the Field Type wasn't found.
*/
unsigned int NtpExtFieldStack::findExtField(NtpExtFieldType extFieldType, unsigned int startIndex) const
{
	for (unsigned int i = startIndex; i <= getStackSize(); i++)
	{
		if (extFieldType == getExtField(i).getFieldType())
		{
			return i;
		}
	}
	return 0;
}


/**
* @brief	Adds an Extension Field as a copy at the end of the stack.
*/
void NtpExtFieldStack::addExtField(NtpExtensionField const & extField)
{
	m_extFieldStack.push_back(extField);
}


/**
* @brief	Counts the Extension Fields that contain the given Field Type
*/
unsigned int NtpExtFieldStack::countExtFieldType(NtpExtFieldType extFieldType) const
{
	unsigned int counter = 0;
	for (auto extField : m_extFieldStack)
	{ 
		if (extField.getFieldType() == extFieldType)
		{
			counter++;
		}
	}
	return counter;
}


/**
* @brief	Deletes all Extension Field entries in the stack.
*/
void NtpExtFieldStack::clear()
{
	m_extFieldStack.clear();
}


/**
* @brief	Serializes the entire stack into a binary data stream.
*/
std::vector<unsigned char> NtpExtFieldStack::getSerializedData() const
{
	std::vector<unsigned char> stackStream;
	for (auto const& extField : m_extFieldStack)
	{
		std::vector<unsigned char> rawRecord;
		rawRecord = extField.getSerializedData();
		stackStream.insert(stackStream.end(), rawRecord.begin(), rawRecord.end());
	}

	return stackStream;
}


/**
* @brief	Parses the given binary stream into a stack of Extension Fields.
*/
void NtpExtFieldStack::fromSerializedData(std::vector<unsigned char> const & serializedData)
{
	unsigned short fieldType = 0;
	unsigned short valueLength = 0;
	std::vector<unsigned char> extFieldValue;
	std::vector<NtpExtensionField> extFieldStack;
	size_t index = 0;
	size_t payloadStart = 0;
	size_t payloadEnd = 0;

	while (true)
	{
		if ((serializedData.size() - index) < c_extFieldMinSize)
		{
			LOG_ERROR("the binary data stream (raw Extension Field stack) is incomplete or damaged");
			return;
		}

		// parse the next ext field from stream
		fieldType = ((serializedData.at(index) & 0xFF) << 8) | serializedData.at(index + 1);
		valueLength = static_cast<unsigned short>(((serializedData.at(index + 2) << 8) | serializedData.at(index + 3)) - c_extFieldHeaderSize);
		payloadStart = index + c_extFieldHeaderSize;
		payloadEnd = payloadStart + valueLength;
		if (payloadEnd > serializedData.size())
		{
			LOG_ERROR("stack content is corrupted (size mismatch)");
			return;
		}
		extFieldValue.assign(serializedData.begin() + payloadStart, serializedData.begin() + payloadEnd);

		// add extracted Extension Field to the stack
		NtpExtensionField extractedExtField(fieldType, extFieldValue);
		extFieldStack.push_back(extractedExtField);
		index += c_extFieldHeaderSize + valueLength;

		// move all records on success
		if ((serializedData.size() - index) == 0) // all bytes processed
		{
			m_extFieldStack = std::move(extFieldStack);
			return;
		}
	}
}

