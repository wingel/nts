/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#pragma once

#include "NtpExtensionField.h"


/*
	0                   1                   2                   3
	0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |          Nonce Length         |      Ciphertext Length        |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                                                               |
   |          Nonce, including up to 3 octets padding              |
   |                                                               |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                                                               |
   |        Ciphertext, including up to 3 octets padding           |
   |                                                               |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                                                               |
   |                      Additional Padding                       |
   |                                                               |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
*/


namespace ExtField
{
	namespace NtsAuthAndEncEF
	{
		void format(NtpExtensionField &ntpExtField);
		bool isValid(NtpExtensionField const &ntpExtField);

		// sets Nonce, Ciphertext and Padding
		bool performAead(NtpExtensionField &ntpExtField,
						 AeadAlgorithm aeadAlgorithm,
						 std::vector <unsigned char> const &key,
						 std::vector <unsigned char> const &plaintext,
						 std::vector <unsigned char> const &nonce,
						 std::vector <unsigned char> const &associatedData);

		bool performAead(NtpExtensionField &ntpExtField,
						 unsigned short aeadAlgorithm,
						 std::vector <unsigned char> const &key,
						 std::vector <unsigned char> const &plaintext,
						 std::vector <unsigned char> const &nonce,
						 std::vector <unsigned char> const &associatedData);

		bool performAead(NtpExtensionField &ntpExtField,
						 AeadAlgorithm aeadAlgorithm,
						 std::vector <unsigned char> const &key,
						 std::vector <unsigned char> const &nonce,
						 std::vector <unsigned char> const &associatedData);

		bool performAead(NtpExtensionField &ntpExtField,
						 unsigned short aeadAlgorithm,
						 std::vector <unsigned char> const &key,
						 std::vector <unsigned char> const &nonce,
						 std::vector <unsigned char> const &associatedData);

		// Nonce
		unsigned short getNonceLength(NtpExtensionField const &ntpExtField);
		std::vector <unsigned char> getNonce(NtpExtensionField const &ntpExtField);

		// Ciphertext
		unsigned short getCiphertextLength(NtpExtensionField const &ntpExtField);
		std::vector <unsigned char> getCiphertext(NtpExtensionField const &ntpExtField);

		// additional Padding
		unsigned short getPaddingLength(NtpExtensionField const &ntpExtField);
	}
}