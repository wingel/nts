#include "NtpExtFieldStackVerification.h"
#include "../Logger/Logger.h"
#include "ExtF_UniqueIdentifier.h"
#include "ExtF_NtsAuthAndEncEF.h"
#include "ExtF_NtsCookie.h"
#include "ExtF_NtsCookiePlaceholder.h"


/**
* @brief	Checks if the stack is valid and compliant with the NTS specification.
*
* @param [in]	stackType		The type of the stack. For example: 'NtpMode::Client_mode' if the argument 'extFieldStack' 
*                               is an NTPv4 client request (unicast, mode 3).
* @param [in]	verifyItems		Performs an additional sanity check of all Extension Fields in the stack.
* @param [in]	receivedNtsNak	Only for server responses (mode 4): Must be set to 'true' if the NTP message contains an NTS NAK. 
*/
bool NtpExtFieldStackVerification::isStackValid(NtpExtFieldStack const & extFieldStack, NtpMode stackType, bool verifyItems, bool receivedNtsNak)
{
	bool status = true;
	if (stackType == NtpMode::Client_mode)
	{
		if (verifyUniqueIdEF(extFieldStack) == false)
		{
			status = false;
		}

		if (verifyCookieEF(extFieldStack, stackType) == false)
		{
			status = false;
		}

		if (verifyPlaceholderEF(extFieldStack, stackType) == false)
		{
			status = false;
		}

		if (verifyAuthAndEncEF(extFieldStack, stackType) == false)
		{
			status = false;
		}
	}
	else if (stackType == NtpMode::Server_mode)
	{
		if (verifyUniqueIdEF(extFieldStack) == false)
		{
			status = false;
		}

		if (receivedNtsNak == true)
		{
			// only the 'Unique Identifier' extension field is allowed
			if ((extFieldStack.findExtField(NtpExtFieldType::NTS_Cookie) != 0) ||
				(extFieldStack.findExtField(NtpExtFieldType::NTS_Cookie_Placeholder) != 0) ||
				(extFieldStack.findExtField(NtpExtFieldType::NTS_Authenticator_and_Encrypted_Extension_Fields) != 0))
			{
				LOG_ERROR("the stack contains wrong nts extensions (an NTS NAK was received)");
				status = false;
			}
		}
		else
		{
			if (verifyCookieEF(extFieldStack, stackType) == false)
			{
				status = false;
			}

			if (verifyPlaceholderEF(extFieldStack, stackType) == false)
			{
				status = false;
			}

			if (verifyAuthAndEncEF(extFieldStack, stackType) == false)
			{
				status = false;
			}
		}

	}
	else
	{
		LOG_ERROR("this NTPv4 mode is not supported");
		status = false;
	}

	if (verifyItems == true)
	{
		if (verifyStackItems(extFieldStack) == false)
		{
			status = false;
		}
	}

	return status;
}


/**
* @return	Returns 'true' if the number and position of the specific Extension Field match
*           with the NTS specification
*/
bool NtpExtFieldStackVerification::verifyUniqueIdEF(NtpExtFieldStack const & extFieldStack)
{
	// request stack (client):  exact one Extension Field is allowed
	// response stack (server): exact one Extension Field is allowed

	bool status = true;
	int stackSize = extFieldStack.getStackSize();
	unsigned int counter = 0;

	for (int i = 1; i <= stackSize; i++)
	{
		if (extFieldStack.getExtField(i).getFieldType() == NtpExtFieldType::Unique_Identifier)
		{
			counter++;
		}
	}
	if (counter == 0)
	{
		LOG_ERROR("missing extension field: 'Unique Identifier'");
		status = false;
	}
	else if (counter > 1)
	{
		LOG_ERROR("multiple extension fields received: 'Unique Identifier'");
		status = false;
	}

	return status;
}


/**
* @return	Returns 'true' if the number and position of the specific Extension Field match
*           with the NTS specification
*/
bool NtpExtFieldStackVerification::verifyCookieEF(NtpExtFieldStack const & extFieldStack, NtpMode stackType)
{
	// request stack (client):  exact one Extension Field is allowed
	// response stack (server): Cookie EFs are not allowed (all Cookie EFs must be inside of the AuthAndEncEF EF)

	bool status = true;
	int stackSize = extFieldStack.getStackSize();
	unsigned int counter = 0;

	// count cookies
	for (int i = 1; i <= stackSize; i++)
	{
		if (extFieldStack.getExtField(i).getFieldType() == NtpExtFieldType::NTS_Cookie)
		{
			counter++;
		}
	}

	if ((stackType != NtpMode::Client_mode) && (stackType != NtpMode::Server_mode))
	{
		LOG_ERROR("this NTPv4 mode is not supported");
		status = false;
	}
	else if ((counter != 0) && (stackType == NtpMode::Server_mode))
	{
		LOG_ERROR("the server response contains non-encrypted 'NTS Cookie' extension fields");
		status = false;
	}
	else if ((counter == 0) && (stackType == NtpMode::Client_mode))
	{
		LOG_ERROR("missing extension field: 'NTS Cookie'");
		status = false;
	}
	else if ((counter > 1) && (stackType == NtpMode::Client_mode))
	{
		LOG_ERROR("multiple extension fields received: 'NTS Cookie'");
	}

	return status;
}


/**
* @return	Returns 'true' if the number and position of the specific Extension Field match
*           with the NTS specification
*/
bool NtpExtFieldStackVerification::verifyPlaceholderEF(NtpExtFieldStack const & extFieldStack, NtpMode stackType)
{
	// request stack (client):  no limitation
	// response stack (server): Cookie Placeholder EFs are not allowed

	bool status = true;

	if ((stackType == NtpMode::Server_mode) && (extFieldStack.findExtField(NtpExtFieldType::NTS_Cookie_Placeholder) != 0))
	{
		LOG_ERROR("the server response contains 'NTS Cookie Placeholder' extension fields");
		status = false;
	}

	return status;
}


/**
* @return	Returns 'true' if the number and position of the specific Extension Field match
*           with the NTS specification
*/
bool NtpExtFieldStackVerification::verifyAuthAndEncEF(NtpExtFieldStack const & extFieldStack, NtpMode stackType)
{
	// request stack (client):  exact one Extension Field is allowed; Must be the last NTS extension field in the stack.
	// response stack (server): exact one Extension Field is allowed; Must be the last NTS extension field in the stack.

	bool status = true;
	int stackSize = extFieldStack.getStackSize();
	unsigned int counter = 0;

	// count AuthAndEncEFs
	for (int i = 1; i <= stackSize; i++)
	{
		if (extFieldStack.getExtField(i).getFieldType() == NtpExtFieldType::NTS_Authenticator_and_Encrypted_Extension_Fields)
		{
			counter++;
		}
	}

	if ((stackType != NtpMode::Client_mode) && (stackType != NtpMode::Server_mode))
	{
		LOG_ERROR("this NTPv4 mode is not supported");
		status = false;
	}
	else if (counter == 0)
	{
		LOG_ERROR("missing extension field: 'NTS Authenticator and Encrypted Extension Fields'");
		status = false;
	}
	else if (counter > 1)
	{
		if (stackType == NtpMode::Client_mode)
		{
			LOG_ERROR("multiple extension fields received: 'NTS Authenticator and Encrypted Extension Fields'");
			status = false;
		}
	}

	// verify Extension Field order
	unsigned int indexUniqueId = extFieldStack.findExtField(NtpExtFieldType::Unique_Identifier);
	unsigned int indexCookie = extFieldStack.findExtField(NtpExtFieldType::NTS_Cookie);
	unsigned int indexPlaceholder = extFieldStack.findExtField(NtpExtFieldType::NTS_Cookie_Placeholder);
	unsigned int indexAuthAndEnc = extFieldStack.findExtField(NtpExtFieldType::NTS_Authenticator_and_Encrypted_Extension_Fields);

	if ((indexAuthAndEnc < indexUniqueId) || (indexAuthAndEnc < indexCookie) || (indexAuthAndEnc < indexPlaceholder))
	{
		LOG_ERROR("wrong order of the NTS Extension Fields");
		status = false;
	}

	return status;
}


/**
* @brief	Performs a sanity check of all Extension Fields in the stack (stack.isValid()).
*
* @return	Returns 'true' if all Records pass the verification.
*/
bool NtpExtFieldStackVerification::verifyStackItems(NtpExtFieldStack const & stack)
{
	bool status = true;

	int stackSize = stack.getStackSize();
	for (int i = 1; i <= stackSize; i++)
	{
		const NtpExtensionField &ef = stack.getExtField(i);
		NtpExtFieldType type = ef.getFieldType();

		switch (type)
		{
		case NtpExtFieldType::Unique_Identifier:
			if (ExtField::UniqueIdentifier::isValid(ef) == false)
			{
				status = false;
			}
			break;

		case NtpExtFieldType::NTS_Cookie:
			if (ExtField::NtsCookie::isValid(ef) == false)
			{
				status = false;
			}
			break;

		case NtpExtFieldType::NTS_Cookie_Placeholder:
			if (ExtField::NtsCookiePlaceholder::isValid(ef) == false)
			{
				status = false;
			}
			break;

		case NtpExtFieldType::NTS_Authenticator_and_Encrypted_Extension_Fields:
			if (ExtField::NtsAuthAndEncEF::isValid(ef) == false)
			{
				status = false;
			}
			break;

		default:
			LOG_WARN("unknown NTS Extension Field found. Field Type: {}", ef.getFieldTypeVal());
			status = true; // ignore unknown extension field
		}
	}
	return status;
}
