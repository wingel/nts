/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#pragma once

#include "NtpExtFieldStack.h"


class NtpExtFieldStackVerification
{
public:
	NtpExtFieldStackVerification() = default;
	~NtpExtFieldStackVerification() = default;

	bool isStackValid(NtpExtFieldStack const & extFieldStack, NtpMode stackType, bool verifyItems, bool receivedNtsNak = false);

private:

	bool verifyUniqueIdEF(NtpExtFieldStack const & extFieldStack);
	bool verifyCookieEF(NtpExtFieldStack const & extFieldStack, NtpMode stackType);
	bool verifyPlaceholderEF(NtpExtFieldStack const & extFieldStack, NtpMode stackType);
	bool verifyAuthAndEncEF(NtpExtFieldStack const & extFieldStack, NtpMode stackTypee);

	bool verifyStackItems(NtpExtFieldStack const & stack);
};

