/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#pragma once

#include <vector>
#include "../NtsConfig.h"


/*
   This implementation is conform with RFC 7822 (NTPv4 Extension Fields).

   Construction of a NTPv4 Extension Field:
   
    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |          Field Type           |            Length             |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   .                                                               .
   .                            Value                              .
   .                                                               .
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                       Padding (as needed)                     |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   

   Short information:
   ----------------------------
   The Length field covers the entire Extension Field (EF), including the Length and Padding fields. 
   While the minimum field length is 4 words (16 octets), a maximum field length remains to be 
   established. 

   more details: https://tools.ietf.org/html/rfc7822
*/

class NtpExtensionField
{
public:
	NtpExtensionField() = default;
	NtpExtensionField(unsigned short fieldType, std::vector<unsigned char> const &value);
	NtpExtensionField(NtpExtFieldType fieldType, std::vector<unsigned char> const &value);
	NtpExtensionField(std::vector<unsigned char> const &serializedData);
	~NtpExtensionField() = default;

	// Field Type
	unsigned short getFieldTypeVal() const;
	NtpExtFieldType getFieldType() const;
	void setFieldType(unsigned short fieldType);
	void setFieldType(NtpExtFieldType fieldType);

	// Length and Value
	unsigned short getValueLength() const;
	const std::vector<unsigned char> & getValue() const;
	void setValue(std::vector<unsigned char> const &value);

	// Misc
	std::vector<unsigned char> getSerializedData(size_t minSize = 16) const;
	void fromSerializedData(std::vector<unsigned char> const &serializedData);
	bool isValid();
	void clear();
	void reserve(size_t bytes);

private:
	const size_t c_extFieldHeaderSize = 4;
	const size_t c_maxValueSize = 65532; // // 2^16 - 4 (EF header size)


	unsigned short m_fieldType;
	std::vector<unsigned char> m_value;
};
