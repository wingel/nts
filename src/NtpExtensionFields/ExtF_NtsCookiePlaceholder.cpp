#include "ExtF_NtsCookiePlaceholder.h"
#include "../Logger/Logger.h"

/**
* @brief	Clears the content and sets the Extension Field header according the rules of this type.
*/
void ExtField::NtsCookiePlaceholder::format(NtpExtensionField &ntpExtField)
{
	ntpExtField.clear();
	ntpExtField.setFieldType(NtpExtFieldType::NTS_Cookie_Placeholder);
}


/**
* @brief	Checks if the Extension Field is valid and compliant with the NTS specification.
*/
bool ExtField::NtsCookiePlaceholder::isValid(NtpExtensionField const &ntpExtField)
{
	bool status = true;

	if (ntpExtField.getFieldType() != NtpExtFieldType::NTS_Cookie_Placeholder)
	{
		LOG_ERROR("wrong Field Type of the Extension Field: '{}'", ntpExtField.getFieldTypeVal());
		status = false;
	}

	if (ntpExtField.getValueLength() <= 0)
	{
		LOG_ERROR("invalid length of the Value field: {} bytes", ntpExtField.getValueLength());
		status = false;
	}
	return status;
}


unsigned short ExtField::NtsCookiePlaceholder::getPlaceholderSize(NtpExtensionField const &ntpExtField)
{
	return ntpExtField.getValueLength();
}


/**
* @brief	Sets the size of the NTS Placeholder (the content is empty)
*/
void ExtField::NtsCookiePlaceholder::setPlaceholderSize(NtpExtensionField &ntpExtField, unsigned short size)
{
	std::vector<unsigned char> placeholder(size);
	ntpExtField.setValue(placeholder);
}
