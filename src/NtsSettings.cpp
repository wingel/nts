#include "NtsSettings.h"
#include "ConfigFile.h"
#include "./Logger/Logger.h"
#include "cryptoFunctions.h"
#include "generalFunctions.h"



NtsSettings readConfigFile(std::string const & configFile)
{
	NtsSettings ntsSettings;
	ConfigFile cfgFile(configFile);
	std::string cfgString;
	unsigned long cfgULong;
	//bool cfgBool;
	

	//	// ============================== Section: general ==============================

	// [general] -> ntsServiceMode
	cfgString.clear();
	if (cfgFile.readString(cfgString, "general", "ntsServiceMode") == false)
	{
		LOG_WARN("Failed to get 'general.ntsServiceMode' from config file ({})", configFile);
	}
	else
	{
		std::transform(cfgString.begin(), cfgString.end(), cfgString.begin(), ::tolower);
		if (cfgString == "client")
		{
			ntsSettings.general.ntsServiceMode = NtsMode::client;
		}
		else if (cfgString == "server")
		{
			ntsSettings.general.ntsServiceMode = NtsMode::server;
		}
		else
		{
			LOG_WARN("invalid value: 'general.ntsServiceMode' from config file ({})", configFile);
			ntsSettings.general.ntsServiceMode = NtsMode::not_set;
		}
	}

	//// ============================== Section: console_logging ======================

	//// [console_logging] -> enableLogToConsole
	//cfgBool = false;
	//if (cfgFile.readBool(cfgBool, "console_logging", "enableLogToConsole") == false)
	//{
	//	LOG_WARN("Failed to get 'console_logging.enableLogToConsole' from config file ({})", configFile);
	//}
	//else
	//{
	//	ntsSettings.console_logging.enableLogToConsole = cfgBool;
	//}

	//// [console_logging] -> logLevel
	//cfgString.clear();
	//if (cfgFile.readString(cfgString, "console_logging", "logLevel") == false)
	//{
	//	LOG_WARN("Failed to get 'console_logging.logLevel' from config file ({})", configFile);
	//}
	//else
	//{
	//	if (cfgString != "")
	//	{
	//		ntsSettings.console_logging.logLevel = cfgString;
	//	}
	//	else
	//	{
	//		LOG_WARN("invalid value: 'console_logging.logLevel' from config file ({})", configFile);
	//	}
	//}

	//// ============================== Section: file_logging ======================

	//// [file_logging] -> enableLogToFile
	//cfgBool = false;
	//if (cfgFile.readBool(cfgBool, "file_logging", "enableLogToFile") == false)
	//{
	//	LOG_WARN("Failed to get 'file_logging.enableLogToFile' from config file ({})", configFile);
	//}
	//else
	//{
	//	ntsSettings.file_logging.enableLogToFile = cfgBool;
	//}

	//// [file_logging] -> logLevel
	//cfgString.clear();
	//if (cfgFile.readString(cfgString, "file_logging", "logLevel") == false)
	//{
	//	LOG_WARN("Failed to get 'file_logging.logLevel' from config file ({})", configFile);
	//}
	//else
	//{
	//	if (cfgString != "")
	//	{
	//		ntsSettings.file_logging.logLevel = cfgString;
	//	}
	//	else
	//	{
	//		LOG_WARN("invalid value: 'file_logging.logLevel' from config file ({})", configFile);
	//	}
	//}

	//// [file_logging] -> fileName
	//cfgString.clear();
	//if (cfgFile.readString(cfgString, "file_logging", "fileName") == false)
	//{
	//	LOG_WARN("Failed to get 'file_logging.fileName' from config file ({})", configFile);
	//}
	//else
	//{
	//	if (cfgString != "")
	//	{
	//		ntsSettings.file_logging.fileName = cfgString;
	//	}
	//	else
	//	{
	//		LOG_WARN("invalid value: 'file_logging.fileName' from config file ({})", configFile);
	//	}
	//}

	//// [file_logging] -> targetLogFolder
	//cfgString.clear();
	//if (cfgFile.readString(cfgString, "file_logging", "targetLogFolder") == false)
	//{
	//	LOG_WARN("Failed to get 'file_logging.targetLogFolder' from config file ({})", configFile);
	//}
	//else
	//{
	//	ntsSettings.file_logging.targetLogFolder = cfgString;
	//}

	//// [file_logging] -> enableLogRotate
	//cfgBool = false;
	//if (cfgFile.readBool(cfgBool, "file_logging", "enableLogRotate") == false)
	//{
	//	LOG_WARN("Failed to get 'file_logging.enableLogRotate' from config file ({})", configFile);
	//}
	//else
	//{
	//	ntsSettings.file_logging.enableLogRotate = cfgBool;
	//}

	//// [file_logging] -> maxLogFiles
	//cfgULong = 0;
	//if (cfgFile.readULong(cfgULong, "file_logging", "maxLogFiles") == false)
	//{
	//	LOG_WARN("Failed to get 'file_logging.maxLogFiles' from config file ({})", configFile);
	//}
	//else
	//{
	//	if (cfgULong > 0)
	//	{
	//		ntsSettings.file_logging.maxLogFiles = cfgULong;
	//	}
	//	else
	//	{
	//		LOG_WARN("invalid value: 'file_logging.cfgULong' from config file ({})", configFile);
	//	}
	//}

	//// [file_logging] -> maxFileSize
	//cfgULong = 0;
	//if (cfgFile.readULong(cfgULong, "file_logging", "maxFileSize") == false)
	//{
	//	LOG_WARN("Failed to get 'file_logging.maxFileSize' from config file ({})", configFile);
	//}
	//else
	//{
	//	if (cfgULong > 0)
	//	{
	//		ntsSettings.file_logging.maxFileSize = cfgULong;
	//	}
	//	else
	//	{
	//		LOG_WARN("invalid value: 'file_logging.maxFileSize' from config file ({})", configFile);
	//	}
	//}

	//// [file_logging] -> minFreeSpace
	//cfgULong = 0;
	//if (cfgFile.readULong(cfgULong, "file_logging", "minFreeSpace") == false)
	//{
	//	LOG_WARN("Failed to get 'file_logging.minFreeSpace' from config file ({})", configFile);
	//}
	//else
	//{
	//	if (cfgULong > 0)
	//	{
	//		ntsSettings.file_logging.minFreeSpace = cfgULong;
	//	}
	//	else
	//	{
	//		LOG_WARN("invalid value: 'file_logging.minFreeSpace' from config file ({})", configFile);
	//	}
	//}

	//// [file_logging] -> maxFolderSize
	//cfgULong = 0;
	//if (cfgFile.readULong(cfgULong, "file_logging", "maxFolderSize") == false)
	//{
	//	LOG_WARN("Failed to get 'file_logging.maxFolderSize' from config file ({})", configFile);
	//}
	//else
	//{
	//	if (cfgULong > 0)
	//	{
	//		ntsSettings.file_logging.maxFolderSize = cfgULong;
	//	}
	//	else
	//	{
	//		LOG_WARN("invalid value: 'file_logging.maxFolderSize' from config file ({})", configFile);
	//	}
	//}

	//// ============================== Section: NTPv4 ================================

	//// [NTPv4] -> ntpEF_uniqueId
	//cfgULong = 0;
	//if (cfgFile.readULong(cfgULong, "NTPv4", "ntpEF_uniqueId") == false)
	//{
	//	LOG_WARN("Failed to get 'NTPv4.ntpEF_uniqueId' from config file ({})", configFile);
	//}
	//else
	//{
	//	if (cfgULong <= 0xFFFF)
	//	{
	//		ntsSettings.NTPv4.ntpEF_uniqueId = static_cast<unsigned short>(cfgULong);
	//	}
	//	else
	//	{
	//		LOG_WARN("invalid value ( >65535 ): 'NTPv4.ntpEF_uniqueId' from config file ({})", configFile);
	//		ntsSettings.NTPv4.ntpEF_uniqueId = 0;
	//	}
	//}

	//// [NTPv4] -> ntpEF_ntsCookie
	//cfgULong = 0;
	//if (cfgFile.readULong(cfgULong, "NTPv4", "ntpEF_ntsCookie") == false)
	//{
	//	LOG_WARN("Failed to get 'NTPv4.ntpEF_ntsCookie' from config file ({})", configFile);
	//}
	//else
	//{
	//	if (cfgULong <= 0xFFFF)
	//	{
	//		ntsSettings.NTPv4.ntpEF_ntsCookie = static_cast<unsigned short>(cfgULong);
	//	}
	//	else
	//	{
	//		LOG_WARN("invalid value ( >65535 ): 'NTPv4.ntpEF_ntsCookie' from config file ({})", configFile);
	//		ntsSettings.NTPv4.ntpEF_ntsCookie = 0;
	//	}
	//}

	//// [NTPv4] -> ntpEF_ntsCookiePlaceholder
	//cfgULong = 0;
	//if (cfgFile.readULong(cfgULong, "NTPv4", "ntpEF_ntsCookiePlaceholder") == false)
	//{
	//	LOG_WARN("Failed to get 'NTPv4.ntpEF_ntsCookiePlaceholder' from config file ({})", configFile);
	//}
	//else
	//{
	//	if (cfgULong <= 0xFFFF)
	//	{
	//		ntsSettings.NTPv4.ntpEF_ntsCookiePlaceholder = static_cast<unsigned short>(cfgULong);
	//	}
	//	else
	//	{
	//		LOG_WARN("invalid value ( >65535 ): 'NTPv4.ntpEF_ntsCookiePlaceholder' from config file ({})", configFile);
	//		ntsSettings.NTPv4.ntpEF_ntsCookiePlaceholder = 0;
	//	}
	//}

	//// [NTPv4] -> ntpEF_ntsAuthAndEncEFs
	//cfgULong = 0;
	//if (cfgFile.readULong(cfgULong, "NTPv4", "ntpEF_ntsAuthAndEncEFs") == false)
	//{
	//	LOG_WARN("Failed to get 'NTPv4.ntpEF_ntsAuthAndEncEFs' from config file ({})", configFile);
	//}
	//else
	//{
	//	if (cfgULong <= 0xFFFF)
	//	{
	//		ntsSettings.NTPv4.ntpEF_ntsAuthAndEncEFs = static_cast<unsigned short>(cfgULong);
	//	}
	//	else
	//	{
	//		LOG_WARN("invalid value ( >65535 ): 'NTPv4.ntpEF_ntsAuthAndEncEFs' from config file ({})", configFile);
	//		ntsSettings.NTPv4.ntpEF_ntsAuthAndEncEFs = 0;
	//	}
	//}

	//// ============================== Section: server ===============================

	// [server] -> tlsPort
	cfgULong = 0;
	if (cfgFile.readULong(cfgULong, "server", "tlsPort") == false)
	{
		LOG_WARN("Failed to get 'server.tlsPort' from config file ({})", configFile);
	}
	else
	{
		if (cfgULong <= 0xFFFF)
		{
			ntsSettings.server.tlsPort = static_cast<unsigned short>(cfgULong);
		}
		else
		{
			LOG_WARN("invalid value ( >65535 ): 'server.tlsPort' from config file ({})", configFile);
			ntsSettings.server.tlsPort = 0;
		}
	}

	// [server] -> aeadAlgoForMasterKey
	cfgString.clear();
	if (cfgFile.readString(cfgString, "server", "aeadAlgoForMasterKey") == false)
	{
		LOG_WARN("Failed to get 'server.aeadAlgoForMasterKey' from config file ({})", configFile);
	}
	else
	{
		AeadAlgorithm aead = getAeadAlgorithmFromStr(cfgString);
		if (isSupported(aead) == true)
		{
			ntsSettings.server.aeadAlgoForMasterKey = aead;
		}
		else
		{
			LOG_WARN("algorithm not supported: 'server.aeadAlgoForMasterKey' from config file ({})", configFile);
			ntsSettings.server.aeadAlgoForMasterKey = AeadAlgorithm::NOT_DEFINED;
		}
	}

	//// [server] -> mdAlgoForMasterKeyHkdf
	//cfgString.clear();
	//if (cfgFile.readString(cfgString, "server", "mdAlgoForMasterKeyHkdf") == false)
	//{
	//	LOG_WARN("Failed to get 'server.mdAlgoForMasterKeyHkdf' from config file ({})", configFile);
	//}
	//else
	//{
	//	MessageDigestAlgo md = getMdAlgorithmFromStr(cfgString);
	//	if (isSupported(md) == true)
	//	{
	//		ntsSettings.server.mdAlgoForMasterKeyHkdf = md;
	//	}
	//	else
	//	{
	//		LOG_WARN("algorithm not supported: 'server.mdAlgoForMasterKeyHkdf' from config file ({})", configFile);
	//		ntsSettings.server.mdAlgoForMasterKeyHkdf = MessageDigestAlgo::Not_Defined;
	//	}
	//}

	//// [server] -> amountOfCookies
	//cfgULong = 0;
	//if (cfgFile.readULong(cfgULong, "server", "amountOfCookies") == false)
	//{
	//	LOG_WARN("Failed to get 'server.amountOfCookies' from config file ({})", configFile);
	//}
	//else
	//{
	//	// TODO set limit?
	//	ntsSettings.server.amountOfCookies = static_cast<unsigned short>(cfgULong);
	//}

	// [server] -> keyRotationHistory
	cfgULong = 0;
	if (cfgFile.readULong(cfgULong, "server", "keyRotationHistory") == false)
	{
		LOG_WARN("Failed to get 'server.keyRotationHistory' from config file ({})", configFile);
	}
	else
	{
		ntsSettings.server.keyRotationHistory = static_cast<unsigned short>(cfgULong);
	}

	// [server] -> keyRotationIntervalSeconds
	cfgULong = 0;
	if (cfgFile.readULong(cfgULong, "server", "keyRotationIntervalSeconds") == false)
	{
		LOG_WARN("Failed to get 'server.keyRotationIntervalSeconds' from config file ({})", configFile);
	}
	else
	{
		ntsSettings.server.keyRotationIntervalSeconds = static_cast<unsigned short>(cfgULong);
	}

	// [server] -> serverCertChainFile
	cfgString.clear();
	if (cfgFile.readString(cfgString, "server", "serverCertChainFile") == false)
	{
		LOG_WARN("Failed to get 'server.serverCertChainFile' from config file ({})", configFile);
	}
	else
	{
		// TODO: check --> exist file?
		ntsSettings.server.serverCertChainFile = cfgString;
	}

	// [server] -> serverPrivateKey
	cfgString.clear();
	if (cfgFile.readString(cfgString, "server", "serverPrivateKey") == false)
	{
		LOG_WARN("Failed to get 'server.serverPrivateKey' from config file ({})", configFile);
	}
	else
	{
		// TODO: check --> exist file?
		ntsSettings.server.serverPrivateKey = cfgString;
	}

	//// [server] -> supportedNextProtocols
	//cfgString.clear();
	//if (cfgFile.readString(cfgString, "server", "supportedNextProtocols") == false)
	//{
	//	LOG_WARN("Failed to get 'server.supportedNextProtocols' from config file ({})", configFile);
	//}
	//else
	//{
	//	// parse line
	//	std::vector<std::string> listTokens;
	//	parseString(cfgString, listTokens, ";, ");
	//	for (std::vector<std::string>::size_type i = 0; i < listTokens.size(); i++)
	//	{
	//		NextProtocol nextProtocol = getNextProtocolFromStr(listTokens.at(i));
	//		if (isSupported(nextProtocol) == true)
	//		{
	//			ntsSettings.server.supportedNextProtocols.push_back(nextProtocol);
	//		}
	//		else
	//		{
	//			LOG_WARN("protocol ({}) not supported: 'server.supportedNextProtocols' from config file ({})", listTokens.at(i), configFile);
	//		}
	//	}
	//}

	//// [server] -> supportedAeadAlgos
	//cfgString.clear();
	//if (cfgFile.readString(cfgString, "server", "supportedAeadAlgos") == false)
	//{
	//	LOG_WARN("Failed to get 'server.supportedAeadAlgos' from config file ({})", configFile);
	//}
	//else
	//{
	//	// parse line
	//	std::vector<std::string> listTokens;
	//	parseString(cfgString, listTokens, ";, ");
	//	for (std::vector<std::string>::size_type i = 0; i < listTokens.size(); i++)
	//	{
	//		AeadAlgorithm aead = getAeadAlgorithmFromStr(listTokens.at(i));
	//		if (isSupported(aead) == true)
	//		{
	//			ntsSettings.server.supportedAeadAlgos.push_back(aead);
	//		}
	//		else
	//		{
	//			LOG_WARN("algorithm ({}) not supported: 'server.supportedAeadAlgos' from config file ({})", listTokens.at(i), configFile);
	//		}
	//	}
	//}

	//// ============================== Section: client ===============================

	//cfgString.clear();
	//if (cfgFile.readString(cfgString, "client", "rootCaBundleFile") == false)
	//{
	//	LOG_WARN("Failed to get 'client.rootCaBundleFile' from config file ({})", configFile);
	//}
	//else
	//{
	//	// TODO: check --> exist file?
	//	ntsSettings.client.rootCaBundleFile = cfgString;
	//}

	//// [client] -> supportedNextProtocols
	//cfgString.clear();
	//if (cfgFile.readString(cfgString, "client", "supportedNextProtocols") == false)
	//{
	//	LOG_WARN("Failed to get 'client.supportedNextProtocols' from config file ({})", configFile);
	//}
	//else
	//{
	//	// parse line
	//	std::vector<std::string> listTokens;
	//	parseString(cfgString, listTokens, ";, ");
	//	for (std::vector<std::string>::size_type i = 0; i < listTokens.size(); i++)
	//	{
	//		NextProtocol nextProtocol = getNextProtocolFromStr(listTokens.at(i));
	//		if (isSupported(nextProtocol) == true)
	//		{
	//			ntsSettings.client.supportedNextProtocols.push_back(nextProtocol);
	//		}
	//		else
	//		{
	//			LOG_WARN("protocol ({}) not supported: 'client.supportedNextProtocols' from config file ({})", listTokens.at(i), configFile);
	//		}
	//	}
	//}

	//// [client] -> supportedAeadAlgos
	//cfgString.clear();
	//if (cfgFile.readString(cfgString, "client", "supportedAeadAlgos") == false)
	//{
	//	LOG_WARN("Failed to get 'client.supportedAeadAlgos' from config file ({})", configFile);
	//}
	//else
	//{
	//	// parse line
	//	std::vector<std::string> listTokens;
	//	parseString(cfgString, listTokens, ";, ");
	//	for (std::vector<std::string>::size_type i = 0; i < listTokens.size(); i++)
	//	{
	//		AeadAlgorithm aead = getAeadAlgorithmFromStr(listTokens.at(i));
	//		if (isSupported(aead) == true)
	//		{
	//			ntsSettings.client.supportedAeadAlgos.push_back(aead);
	//		}
	//		else
	//		{
	//			LOG_WARN("algorithm ({}) not supported: 'client.supportedAeadAlgos' from config file ({})", listTokens.at(i), configFile);
	//		}
	//	}
	//}

	
	return ntsSettings;
}


