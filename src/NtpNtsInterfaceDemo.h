/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#pragma once
#include "NtpNtsInterface.h"

class NtpNtsInterfaceDemo : public NtpNtsInterface
{
public:

	NtpNtsInterfaceDemo();
	~NtpNtsInterfaceDemo();

	//  ==================== von NTS benötigt ====================
	double getTransmitTimestamp() const override;
	double getOriginTimestamp() const override;
	std::string getIpAddress() const override;
	void updateTransmitTimestamp() override;

	unsigned int getExtensionFieldCount() const override;
	unsigned short getExtensionFieldType(unsigned int index) const override; 								// index beginnt bei 0
	std::vector<unsigned char> getExtensionFieldValue(unsigned int index) const override; 					// index beginnt bei 0
	void addExtensionField(unsigned short fieldType, std::vector<unsigned char> value) override;
	void delExtensionField(unsigned int index);

	std::vector<unsigned char> getSerializedPackage() const override;

	void setUseMessageForTimeSync(bool state) override;
	bool getUseMessageForTimeSync() const override;

	void setSendResponseMessage(bool state) override;
	bool getSendResponseMessage() const override;

	void setGenerateResponseMessage(bool state) override;
	bool getGenerateResponseMessage() const override;
	//  ==================== von NTS benötigt ====================



	//  ==================== von NTP benötigt ====================
	void setTransmitTimestamp(double timestamp);
	void setOriginTimestamp(double timestamp);
	void setIPaddress(std::string const &ip);

	bool isUseMessageForTimeSync() const;
	bool isSendResponseMessage() const;

	void createNewResponseMessage(std::string recipientIP);
	void createNewResponseMessage(NtpNtsInterface const &receivedPackage);
	//  ==================== von NTP benötigt ====================

private:

	double m_transmitTimestamp;
	double m_originTimestamp;
	bool m_useMessageForTimeSync;
	bool m_sendResponseMessage;
	bool m_generateResponseMessage;

	std::string m_IPaddress;

	std::vector<unsigned char> m_randomNtpHeaderData;
	std::vector<unsigned short> m_extensionFieldTypes;
	std::vector<std::vector<unsigned char>> m_extensionFieldValues;

};

// END OF FILE
