﻿/*
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#include <iostream>
#include <thread>
#include "generalFunctions.h"
#include "./Logger/Logger.h"
#include <csignal>
#include <boost/filesystem.hpp>
#include "NtsUnicastService.h"
#include "NtpNtsInterfaceDemo.h"
#include "NtsConfig.h"
#include <openssl/crypto.h> // openssl version


#ifdef _MSC_VER
//enable debug
#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>
#ifdef _DEBUG
#define DEBUG_NEW new(_NORMAL_BLOCK, __FILE__, __LINE__)
#define new DEBUG_NEW
#endif
#endif



void my_handler(int s)
{
	std::cout << "Caught signal: " << s << std::endl;
	std::this_thread::sleep_for(std::chrono::milliseconds(2000));
	exit(1);
}





int main(int argc, char* argv[])
{


#ifdef _MSC_VER
	// find memory leaks
	//_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	//_CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_DEBUG);
	//_CrtSetBreakAlloc(23609);
#endif

	SetWindow(160, 46, 160, 500);
	static_assert(sizeof(unsigned short) == 2, "unsigned short != 2");
	static_assert(CHAR_BIT == 8, "CHAR_BIT != 8");

	std::signal(SIGINT, my_handler);


	try
	{
		initLogger();

		int iter1 = 1;
		int iter2 = 1;
		int wait_ms = 0;

		std::string current_path = boost::filesystem::path(boost::filesystem::current_path()).string();
		boost::filesystem::path programPath(argv[0]);
		programPath = programPath.remove_filename();
		std::string program_path = programPath.string();


		LOG_DEBUG("Arbeitsverzeichnis:   {}", current_path);
		LOG_DEBUG("Programmverzeichnis:  {}", program_path);
		LOG_RAW("");
		LOG_INFO ("NTS version:          NTS (draft-ietf-ntp-using-nts-for-ntp-15) v{} ({})", NTS_VERSION, NTS_COMPILE_TIME);
		LOG_DEBUG("OpenSSL version:      {}", OpenSSL_version(OPENSSL_VERSION));
		LOG_DEBUG("BOOST version:        {}.{}.{}", (BOOST_VERSION/100000), (BOOST_VERSION / 100 % 1000), (BOOST_VERSION % 100));
		LOG_DEBUG("libaes_siv version:   1.0");
		LOG_DEBUG("rang version:         3.1.0");
		LOG_DEBUG("spdlog version:       {}.{}.{}", SPDLOG_VER_MAJOR, SPDLOG_VER_MINOR, SPDLOG_VER_PATCH);
		LOG_DEBUG("FMT version:          {}.{}.{}", (FMT_VERSION / 10000), (FMT_VERSION / 100 % 100), (FMT_VERSION % 100));
		LOG_RAW("");





		for (int i = 1; i <= iter1; i++)
		{

			NtsUnicastService nts_Client;
			nts_Client.initialization("configClient.ini");

			LOG_INFO("NTS: Initialisierung abgeschlossen");

			NtpNtsInterfaceDemo receivedPackage;
			NtpNtsInterfaceDemo responsePackage;
			responsePackage.createNewResponseMessage(IP_ADDRESS);
			LOG_TRACE("size of empty NTP packet: {}", responsePackage.getSerializedPackage().size());


			for (int i = 1; i <= iter2; i++)
			{
				std::this_thread::sleep_for(std::chrono::milliseconds(wait_ms));

				// NTPv4: Client: request erstellen
				nts_Client.createRequestMessage(responsePackage);

				size_t cReqSize = responsePackage.getSerializedPackage().size();
				receivedPackage = responsePackage;
				responsePackage.createNewResponseMessage(receivedPackage);

				size_t sResSize = responsePackage.getSerializedPackage().size();
				receivedPackage = responsePackage;
				responsePackage.createNewResponseMessage(receivedPackage);

				// NTPv4: Client: response prüfen
				nts_Client.processingReceivedNtpMessage(receivedPackage);

				LOG_DEBUG("client request size:    \t{} Bytes", cReqSize);
				LOG_DEBUG("server response size:   \t{} Bytes", sResSize);


				//std::cout << getItemID_Alternative(receivedPackage.getSerializedPackage()) << std::endl;

			}

		}

		std::this_thread::sleep_for(std::chrono::milliseconds(1000));
		//LOG_INFO("\n\n--------------------\n[Eingabe um TLS-Server zu beenden]\n\n");
		//std::cin.get();
	}
	catch (const spdlog::spdlog_ex &ex)
	{
		std::cout << "spdlog EXCEPTION: " << ex.what() << std::endl;
	}
	catch (std::exception& e)
	{
		LOG_FATAL("EXCEPTION: {}", e.what());
	}
	catch (...)
	{
		LOG_FATAL("unknown EXCEPTION");
	}



	LOG_INFO("Programmende erreicht\n\n----------------------------------\nProgramm beendet");
	std::cin.get();
	return 0;
}




/*
	Notes:

	1.2:	über den TLS-Kanal verhandeln die Parteien einige zusätzliche Protokollparameter und der Server sendet dem Client eine
			Lieferung von Cookies, zusammen mit einer Liste von einer oder mehreren IP-Adressen an NTP-Server, für die die Cookies gültig sind


	- client kann cookies speichern
- kommunikation zwischen KE-server und NTP-Server

•	Implementierungen die mehr als eine TLS-Version nutzen können, SOLLTEN NICHT auf Handshake-Fehler antworten,
	indem der Vorgang mit einer niedrigeren Protokollversion wiederholt wird
	o	Wird dies doch getan, so MUSS RFC7507 („TLS Fallback SCSV“) implementiert sein


•	Clients und Server KÖNNEN Längenbeschränkungen für Anfragen und Antworten erzwingen
	o	Server MÜSSEN jedoch Anfragen von mindestens 1024 Oktetts akzeptieren
	o	Clients SOLLTEN Antworten von mindestens 65536 Oktetts akzeptieren.

	-recstack verification



Unique-ID_EF:
•	Alle Serverpakete, die von NTS-implementierenden Servern als Reaktion auf Client-Pakete generiert werden,
	die dieses Erweiterungsfeld enthalten, MÜSSEN dieses Feld ebenfalls mit dem Inhalt der Anfrage des Clients enthalten


	- Off-Path-Angreifer?




	5.6 Link fehler


5.6 AaEncEF:
•	Für Mode 4 (Server)-Pakete wird nie ein Additional Padding-Feld benötigt
	(ggf warning? erzeugen?)
•	Für Mode 3 (Client)-Pakete wird die Länge des Additional Padding-Feldes wie folgt berechnet: ....
•	Server MÜSSEN diese Anforderung durchsetzen, indem sie jedes Paket verwerfen, das nicht diesem entspricht
•	Das NTS Authenticator and Encrypted Extension Fields-Erweiterungsfeld DARF NICHT in NTP-Pakete aufgenommen werden,
	deren Modus anders als 3 (Client) oder 4 (Server) ist.




5.7:

o	Der Client KANN Cookies möglicherweise wiederverwenden, um die Widerstandsfähigkeit (resilience) gegenüber der Unverknüpfbarkeit (unlinkability) zu priorisieren
o	Welche der beiden Prioritäten in einem bestimmten Fall priorisiert werden sollten, hängt von der Anwendung und den Vorlieben des Benutzers ab



•	Der Server MUSS trotzdem jedes nicht-authentifizierte Erweiterungsfeld verwerfen und mit der Verarbeitung fortfahren, als wäre diese nicht vorhanden
o		Der Server KANN Ausnahmen zu dieser Anforderung implementieren, wenn es die Spezifikation für bestimmte Erweiterungsfelder explizit vorsieht


•	Der Client MUSS trotzdem jedes nicht-authentifizierte Erweiterungsfeld verwerfen und mit der Verarbeitung fortfahren, als wäre diese nicht vorhanden
o		Der Client KANN Ausnahmen zu dieser Anforderung implementieren, wenn es die Spezifikation für bestimmte Erweiterungsfelder explizit vorsieht




•	Wenn der Server das Cookie nicht validieren oder die Anfrage nicht authentifizieren kann:
	o	dann SOLLTE er mit einem Kiss-o'-Death (KoD)-Paket antworten
			Kiss-Code: ***'NTSN'*** (bedeutet „NTS NAK“ (NAK: not acknowledged)
			siehe RFC 5905, Abschnitt 7.4

	o	Das Paket DARF folgende NTS-Erweiterungsfelder NICHT beinhalten:
			NTS Cookie
			NTS Authenticator and Encrypted Extension Fields




•	Wenn der NTP-Server zuvor mit authentischen NTS-geschützten NTP-Paketen geantwortet hat
	(d.h. Pakete, die das NTS Authenticator and Encrypted Extension-Erweiterungsfeld enthalten:
	o	Der Client MUSS alle vom Server empfangenen KoD-Pakete überprüfen
			Diese enthalten ein „Unique Identifier“-Erweiterungsfeld
			Das „Unique Identifier“-EF MUSS mit der ausstehenden Anfrage übereinstimmen

	o	Bei Fehlschlag MUSS das Paket ohne weitere Verarbeitung verworfen werden
	o	Bei Erfolg MUSS der Client RFC 5905, Abschnitt 7.4, wo es erforderlich ist, entsprechen




•	Ein Client KANN das NTS-KE-Protokoll bei erzwungener Trennung von einem NTP-Server automatisch erneut ausführen
	o	In diesem Fall MUSS es möglich sein, Schleifen zwischen den NTS-KE- und NTP-Servern zu erkennen und zu stoppen
			durch Limitierung der Versuche (z.B. exponentiell steigende Versuchsintervalle)




Bei Empfang einer NTS NAK
	•	Nach dem Empfang des NTS-NAK-KoD SOLLTE der Client bis zum nächsten Poll auf eine gültige NTS-geschützte Antwort warten
	•	wenn keine empfangen wird, SOLLTE der Client einen neuen NTS-KE-Handshake einleiten
		o	Damit versucht der Client neue Parameter (Cookies, AEAD-Schlüssel, …) auszuhandeln
		o	Wenn der NTS-KE-Handshake erfolgreich ist, MUSS der Client alle alten Cookies und Parameter verwerfen und stattdessen die neuen verwenden
		o	Solange der NTS-KE-Handshake nicht erfolgreich war, SOLLTE der Client weiterhin den NTP-Server unter Verwendung der vorhandenen Cookies und Parameter abfragen

	•	Um den NTP-Sitzungsneustart zu ermöglichen (falls der NTS-KE-Server nicht verfügbar ist) und um die NTS-KE-Serverlast zu reduzieren,
		SOLLTE der Client mindestens das letzte Cookie, den AEAD-Schlüssel, den ausgehandelten AEAD-Algorithmus und andere notwendige
		Parameter auf dem persistenten Speicher behalten
		o	Auf diese Weise kann der Client die NTP-Sitzung fortsetzen, ohne eine erneute NTS-KE-Verhandlung durchzuführen




•	Unmittelbar nach jeder solchen Schlüsselrotation sollten die Server alle Schlüssel, die vor zwei oder mehr Rotationsperioden generiert wurden, ***sicher*** löschen




•	Um ein vom Client bereitgestelltes Cookie zu überprüfen und zu entschlüsseln, ist es zuerst in seine Komponenten „I“, „N“ und „C“ zu parsen
	o	„I“ wird verwendet, um den Entschlüsselungsschlüssel „K“ nachzuschlagen
			Wenn der zugehörige Schlüssel gelöscht wurde oder nie existierte, scheitert die Entschlüsselung
		•		Geantwortet wird dann mit einer NTS NAK




- Records: Errors und Warnings senden



•	Aufgrund der Anforderung [RFC7822], dass Erweiterungen aufgefüllt (padding) und an Vier-Oktett-Grenzen ausgerichtet werden, kann die Antwortgröße in einigen Fällen die Anfragegröße um bis zu drei Oktette überschreiten
	o	Das ist ausreichend belanglos, sodass nicht weiter darauf eingegangen wird

	--> in-Place-Problem?





o	Sobald die Uhr synchronisiert wurde, schreiben Sie die aktuelle Systemzeit periodisch in den persistenten Speicher
		Nehmen Sie keine Zertifikate an, deren NotAfter früher als die letzte aufgezeichnete Zeit ist

o	Verarbeiten Sie keine Zeitpakete von Servern, wenn die von ihnen berechnete Zeit außerhalb des Gültigkeitszeitraums des Serverzertifikats liegt



*/


// (ಠ_ಠ)   ლ(ಠ益ಠ)ლ    (╯°□°）╯   (╥﹏╥)   \(°□°)/    (ง︡'-'︠)ง

// NTP: The reference implementation (NTP) discards any packet with an extension field length more than 1024 octets.    ?
// --> https://datatracker.ietf.org/doc/draft-stenn-ntp-extension-fields/?include_text=1
// TODO: exceptions an der NTS/NTP-Schnittstelle fangen und loggen (werden als .lib nicht gefangen)
// TODO: Lizenzdatum aktualisieren
// TODO: Header in den Sourcefiles verschieben
// TODO: führt TRACE code aus?
// TODO: Log raw (file/console) -> Text ohne präfix ausgeben (wie std::cout << ...)
// TODO: NTS NAK on error
// TODO: logrotation: datei ist nicht immer im richtigen ordner
// TODO: NTS in Namespace
// TODO: NTS als include --->    #include "nts/xxx.cpp"
// TODO: TLS 1.3 draft 28 --> gegen die andere impl.
// TODO: Ordnerstruktur anpassen
// TODO: includes anders lösen (Boost, OpenSSL)
// TODO: Cookie Placeholder KÖNNEN verschlüsselt sein!
// TODO: Berechnung Padding richtig? --> draft-12
// TODO: ner NTP interface
// TODO: max size TLS record?
// TDOD: prüfen ob NTS das letzte EF liefert --> dann min size: 28 bytes
// TODO: tls settings
// TODO: tls algos festlegen
// TODO: include guards ändern?  (#pragma once -->   in klassischen Guard #ifdef ...)
// TODO: error handling (records) / NTS NAK
// TODO: valgrind
// TODO: tests
// TODO: Boost Log zeigt UTC an anstatt lokale Zeit
// TODO: cmake
// TODO: clang format
// TODO: NTP: serialisieren bis AEAD (ohne EF delete)
// TODO: supported TLS versions
// TODO: gruppieren von Settings? --> TLS, Server, Client, MasterKey
// TODO: richtiges Parsen der EFs? --> NTP zero padding   könnte bei Cookies stören (AEAD operationen), wenn cookie z.b. 15 Byte groß ist (ggf auch UniqueID)
// TODO: arbeitspfad/exepfad ? ----> richtiges finden von config und certs
// TODO: NTP: Ordner sind case sensitive wtf!?
// TODO: klassenbeschreibungen in doxygen umwandeln
// TODO: Ersetzung: std::vector  .at() mit []?
// TODO: seeding (Master Key) in config einstellbar?  ---> seed sharing über mehrere server
// TODO: Cookies auf HDD speichern
// TODO: Test MasterKey und 0 rotationen
// TODO: DEBUG makro geht nicht (zeigt release an)
// TODO: TLS 1.3 bug in OpenSSL (label in KeyExporter darf max. 28 Zeichen lang sein)
// TODO: in Interface: print NTS-Info:  Version, OpenSSL version, pfade, Zertifikate, ....
// TODO: MAX_SIZE für Pakete, um IP fragmentierung zu verhindern
// TODO: showInfo() --> version von openssl spdlog und verzeichnisse
// https://github.com/dfoxfranke/nts/blob/master/fig_overview.txt
// TODO: namespace verwenden: 'RecStackVerification' -->  Records::StackVerification  ???
// TODO: IPvFuture support für 'Ntpv4ServerNeg' nachziehen
// TODO: doxygen sind teilweise noch in deutsch (z.B:: Ntpv4ServerNeg)
// TODO: prüfen: default ctor initialisiert nicht die build-in variablen!
// TODO: in 'ExtField::NtsAuthAndEncEF::isValid()'  -->   check Field length, min Nonce size, min cipher size, padding size
// TODO: add: lifetime control for client sessions?
// TODO: parse config file: in Pfaden "\" durch "/" ersetzen, damit es in linux funktioniert
// TODO: Logger: ggf Ordner erzeugen, wenn dieser fehlt (case sensitive)
// TODO: Logdatei Header(Überschriften)?


