#include "ConfigFile.h"
#include <fstream>
#include <algorithm>  // std::transform
#include <sstream>
#include <vector>


// =========================================================================================================================================
// ==========================================================  general functions  ==========================================================
// =========================================================================================================================================

/**
 * @brief Standard constructor.
 */
ConfigFile::ConfigFile():
	m_configFileName("")
{
}



/**
 * @brief Constructor with parameters.
 */
ConfigFile::ConfigFile(std::string const &filename):
	m_configFileName(filename)
{
}



/**
 * @brief Standard dstructor.
 */
ConfigFile::~ConfigFile()
{
}



// =========================================================================================================================================
// ========================================================  specific description  =========================================================
// =========================================================================================================================================

/**
 * @brief Checks that the configuration file exists and can be opened
 *
 * @return Returns 'true' if the configuration file exists
 *
 * @see getConfigFile()
 * @see setConfigFile()
 */
bool ConfigFile::existConfigFile()
{
	std::ifstream file;
	file.open(getConfigFile());
	if (file.is_open() == true)
	{
	    file.close();
	    return true;
	}
	else
	{
		return false;
	}
}



/**
 * @brief 	Checks the character encoding format of the configuration file. A valid file must
 * 			use the ANSI encoding format (UTF8 ist not supported).
 *
 * @return	Returns 'true' if the configuration file use the ANSI encoding format
 *
 * @retval	true	the configuration file exists, can be opened and use the ANSI encoding format
 * @retval	false	the configuration doesn't file exists, can not be opened or doesn't use the ANSI encoding format
 *
 */
bool ConfigFile::isConfigFileValid()
{
	std::ifstream file;
	unsigned char byte = 0;

    // open and check file
    file.open(getConfigFile());
    if (file.is_open() == true)
    {
    	// read one byte
    	file.read(reinterpret_cast<char *>(&byte), 1);
    	file.close();

    	// quick character encoding test (todo: test can fail -> make better tests)
    	if(byte < 0x80)
		{
    		return true; // ANSI encoding format (good)
		}
    	else
		{
    		return false; // UTF8 encoding format (bad)
		}
    }
    else
    {
    	return false;
    }
}



/**
 * @brief Returns the full path and filename of the configuration file
 *
 * @return Returns the full path and filename of the configuration file
 *
 * @see existConfigFile()
 * @see setConfigFile()
 */
std::string ConfigFile::getConfigFile() const
{
	return m_configFileName;
}



/**
 * @brief Sets the path and the name of the configuration file
 *
 * @param [in]  filename	The path and the name of the configuration file.
 *
 * @see existConfigFile()
 * @see getConfigFile()
 */
void ConfigFile::setConfigFile(std::string const &filename)
{
	this->m_configFileName = filename;
}



/**
 * @brief Reads a value (bool) from the configuration file
 *
 * @param [out] dst				Destination reference, where the value is to be copied.
 * @param [in]  section			The name of the section. Allowed characters: a-z, A-Z, 0-9, underscore (_), dot(.) and space ( ).
 * @param [in]  key				The name of the key. Allowed characters: a-z, A-Z, 0-9, underscore (_), dot(.) and space ( ).
 * @param [in]  defaultValue	The default value if the searched entry is not found or an error occurs
 *
 * @return Returns 'true' if the searched key was found.
 *
 * @see writeBool()
 */
bool ConfigFile::readBool(bool &dst, std::string const &section, std::string const &key, bool defaultValue) const
{
	std::string value = "";

	if (readConfigKey(value, section, key) == true)
	{
		// convert the string to lower case
		std::transform(value.begin(), value.end(), value.begin(),::tolower);
		if (value == "yes" || value == "true" || value == "1")
		{
			dst = true;
		}
		else
		{
			dst = false;
		}

		return true;
	}
	else
	{
		dst = defaultValue;
		return false;
	}
}



/**
 * @brief Writes a value (bool) to the configuration file
 *
 * @param [in] value		The value to be written to the configuration file.
 * @param [in] section		The name of the section. Allowed characters: a-z, A-Z, 0-9, underscore (_), dot(.) and space ( ).
 * @param [in] key			The name of the key. Allowed characters: a-z, A-Z, 0-9, underscore (_), dot(.) and space ( ).
 *
 * @return Returns 'true' on success.
 *
 * @see readBool()
 */
bool ConfigFile::writeBool(bool value, std::string const &section, std::string const &key) const
{
	std::string valueStr = "";

	if (value == true)
	{
		valueStr = "true";
	}
	else
	{
		valueStr = "false";
	}

	if (writeConfigKey(valueStr, section, key) == true)
	{
		return true;
	}
	else
	{
		return false;
	}
}



/**
 * @brief Reads a value (int) from the configuration file
 *
 * @param [out] dst				Destination reference, where the value is to be copied.
 * @param [in]  section			The name of the section. Allowed characters: a-z, A-Z, 0-9, underscore (_), dot(.) and space ( ).
 * @param [in]  key				The name of the key. Allowed characters: a-z, A-Z, 0-9, underscore (_), dot(.) and space ( ).
 * @param [in]  defaultValue	The default value if the searched entry is not found or an error occurs
 *
 * @return Returns 'true' if the searched key was found.
 *
 * @see writeInt()
 */
bool ConfigFile::readInt(int &dst, std::string const &section, std::string const &key, int defaultValue) const
{
	std::string value = "";

	if (readConfigKey(value, section, key) == true)
	{
		dst = std::stoi(value);
		return true;
	}
	else
	{
		dst = defaultValue;
		return false;
	}
}



/**
 * @brief Writes a value (int) to the configuration file
 *
 * @param [in] value		The value to be written to the configuration file.
 * @param [in] section		The name of the section. Allowed characters: a-z, A-Z, 0-9, underscore (_), dot(.) and space ( ).
 * @param [in] key			The name of the key. Allowed characters: a-z, A-Z, 0-9, underscore (_), dot(.) and space ( ).
 *
 * @return Returns 'true' on success.
 *
 * @see readInt()
 */
bool ConfigFile::writeInt(int value, std::string const &section, std::string const &key) const
{
	if (writeConfigKey(std::to_string(value), section, key) == true)
	{
		return true;
	}

	return false;
}



/**
 * @brief Reads a value (long) from the configuration file
 *
 * @param [out] dst				Destination reference, where the value is to be copied.
 * @param [in]  section			The name of the section. Allowed characters: a-z, A-Z, 0-9, underscore (_), dot(.) and space ( ).
 * @param [in]  key				The name of the key. Allowed characters: a-z, A-Z, 0-9, underscore (_), dot(.) and space ( ).
 * @param [in]  defaultValue	The default value if the searched entry is not found or an error occurs
 *
 * @return Returns 'true' if the searched key was found.
 *
 * @see writeLong()
 */
bool ConfigFile::readLong(long &dst, std::string const &section, std::string const &key, long defaultValue) const
{
	std::string value = "";

	if (readConfigKey(value, section, key) == true)
	{
		dst = std::stol(value);
		return true;
	}
	else
	{
		dst = defaultValue;
		return false;
	}
}



/**
 * @brief Writes a value (long) to the configuration file
 *
 * @param [in] value		The value to be written to the configuration file.
 * @param [in] section		The name of the section. Allowed characters: a-z, A-Z, 0-9, underscore (_), dot(.) and space ( ).
 * @param [in] key			The name of the key. Allowed characters: a-z, A-Z, 0-9, underscore (_), dot(.) and space ( ).
 *
 * @return Returns 'true' on success.
 *
 * @see readLong()
 */
bool ConfigFile::writeLong(long value, std::string const &section, std::string const &key) const
{
	if (writeConfigKey(std::to_string(value), section, key) == true)
	{
		return true;
	}
	else
	{
		return false;
	}
}



/**
 * @brief Reads a value (unsigned long) from the configuration file
 *
 * @param [out] dst				Destination reference, where the value is to be copied.
 * @param [in]  section			The name of the section. Allowed characters: a-z, A-Z, 0-9, underscore (_), dot(.) and space ( ).
 * @param [in]  key				The name of the key. Allowed characters: a-z, A-Z, 0-9, underscore (_), dot(.) and space ( ).
 * @param [in]  defaultValue	The default value if the searched entry is not found or an error occurs
 *
 * @return Returns 'true' if the searched key was found.
 *
 * @see writeULong()
 */
bool ConfigFile::readULong(unsigned long &dst, std::string const &section, std::string const &key, unsigned long defaultValue) const
{
	std::string value = "";

	if (readConfigKey(value, section, key) == true)
	{
		dst = std::stoul(value);
		return true;
	}
	else
	{
		dst = defaultValue;
		return false;
	}
}



/**
 * @brief Writes a value (unsigned long) to the configuration file
 *
 * @param [in] value		The value to be written to the configuration file.
 * @param [in] section		The name of the section. Allowed characters: a-z, A-Z, 0-9, underscore (_), dot(.) and space ( ).
 * @param [in] key			The name of the key. Allowed characters: a-z, A-Z, 0-9, underscore (_), dot(.) and space ( ).
 *
 * @return Returns 'true' on success.
 *
 * @see readULong()
 */
bool ConfigFile::writeULong(unsigned long value, std::string const &section, std::string const &key) const
{
	if (writeConfigKey(std::to_string(value), section, key) == true)
	{
		return true;
	}
	else
	{
		return false;
	}
}



/**
 * @brief Reads a value (long long) from the configuration file
 *
 * @param [out] dst				Destination reference, where the value is to be copied.
 * @param [in]  section			The name of the section. Allowed characters: a-z, A-Z, 0-9, underscore (_), dot(.) and space ( ).
 * @param [in]  key				The name of the key. Allowed characters: a-z, A-Z, 0-9, underscore (_), dot(.) and space ( ).
 * @param [in]  defaultValue	The default value if the searched entry is not found or an error occurs
 *
 * @return Returns 'true' if the searched key was found.
 *
 * @see writeLongLong()
 */
bool ConfigFile::readLongLong(long long &dst, std::string const &section, std::string const &key, long long defaultValue) const
{
	std::string value = "";

	if (readConfigKey(value, section, key) == true)
	{
		dst = std::stoll(value);
		return true;
	}
	else
	{
		dst = defaultValue;
		return false;
	}
}



/**
 * @brief Writes a value (long long) to the configuration file
 *
 * @param [in] value		The value to be written to the configuration file.
 * @param [in] section		The name of the section. Allowed characters: a-z, A-Z, 0-9, underscore (_), dot(.) and space ( ).
 * @param [in] key			The name of the key. Allowed characters: a-z, A-Z, 0-9, underscore (_), dot(.) and space ( ).
 *
 * @return Returns 'true' on success.
 *
 * @see readLongLong()
 */
bool ConfigFile::writeLongLong(long long value, std::string const &section, std::string const &key) const
{
	if (writeConfigKey(std::to_string(value), section, key) == true)
	{
		return true;
	}
	else
	{
		return false;
	}
}



/**
 * @brief Reads a value (unsigned long long) from the configuration file
 *
 * @param [out] dst				Destination reference, where the value is to be copied.
 * @param [in]  section			The name of the section. Allowed characters: a-z, A-Z, 0-9, underscore (_), dot(.) and space ( ).
 * @param [in]  key				The name of the key. Allowed characters: a-z, A-Z, 0-9, underscore (_), dot(.) and space ( ).
 * @param [in]  defaultValue	The default value if the searched entry is not found or an error occurs
 *
 * @return Returns 'true' if the searched key was found.
 *
 * @see writeULongLong()
 */
bool ConfigFile::readULongLong(unsigned long long &dst, std::string const &section, std::string const &key, unsigned long long defaultValue) const
{
	std::string value = "";

	if (readConfigKey(value, section, key) == true)
	{
		dst = std::stoull(value);
		return true;
	}
	else
	{
		dst = defaultValue;
		return false;
	}
}



/**
 * @brief Writes a value (unsigned long long) to the configuration file
 *
 * @param [in] value		The value to be written to the configuration file.
 * @param [in] section		The name of the section. Allowed characters: a-z, A-Z, 0-9, underscore (_), dot(.) and space ( ).
 * @param [in] key			The name of the key. Allowed characters: a-z, A-Z, 0-9, underscore (_), dot(.) and space ( ).
 *
 * @return Returns 'true' on success.
 *
 * @see readULongLong()
 */
bool ConfigFile::writeULongLong(unsigned long long value, std::string const &section, std::string const &key) const
{
	if (writeConfigKey(std::to_string(value), section, key) == true)
	{
		return true;
	}
	else
	{
		return false;
	}
}



/**
 * @brief Reads a value (float) from the configuration file
 *
 * @param [out] dst				Destination reference, where the value is to be copied.
 * @param [in]  section			The name of the section. Allowed characters: a-z, A-Z, 0-9, underscore (_), dot(.) and space ( ).
 * @param [in]  key				The name of the key. Allowed characters: a-z, A-Z, 0-9, underscore (_), dot(.) and space ( ).
 * @param [in]  defaultValue	The default value if the searched entry is not found or an error occurs
 *
 * @return Returns 'true' if the searched key was found.
 *
 * @see writeFloat()
 */
bool ConfigFile::readFloat(float &dst, std::string const &section, std::string const &key, float defaultValue) const
{
	std::string value = "";

	if (readConfigKey(value, section, key) == true)
	{
		dst = std::stof(value);
		return true;
	}
	else
	{
		dst = defaultValue;
		return false;
	}
}



/**
 * @brief Writes a value (float) to the configuration file
 *
 * @param [in] value		The value to be written to the configuration file.
 * @param [in] section		The name of the section. Allowed characters: a-z, A-Z, 0-9, underscore (_), dot(.) and space ( ).
 * @param [in] key			The name of the key. Allowed characters: a-z, A-Z, 0-9, underscore (_), dot(.) and space ( ).
 *
 * @return Returns 'true' on success.
 *
 * @see readFloat()
 */
bool ConfigFile::writeFloat(float value, std::string const &section, std::string const &key) const
{
	std::stringstream valueStr;
	valueStr.precision(25);
	valueStr << value;

	if (writeConfigKey(valueStr.str(), section, key) == true)
	{
		return true;
	}
	else
	{
		return false;
	}
}



/**
 * @brief Reads a value (double) from the configuration file
 *
 * @param [out] dst				Destination reference, where the value is to be copied.
 * @param [in]  section			The name of the section. Allowed characters: a-z, A-Z, 0-9, underscore (_), dot(.) and space ( ).
 * @param [in]  key				The name of the key. Allowed characters: a-z, A-Z, 0-9, underscore (_), dot(.) and space ( ).
 * @param [in]  defaultValue	The default value if the searched entry is not found or an error occurs
 *
 * @return Returns 'true' if the searched key was found.
 *
 * @see writeDouble()
 */
bool ConfigFile::readDouble(double &dst, std::string const &section, std::string const &key, double defaultValue) const
{
	std::string value = "";

	if (readConfigKey(value, section, key) == true)
	{
		dst = std::stod(value);
		return true;
	}
	else
	{
		dst = defaultValue;
		return false;
	}
}



/**
 * @brief Writes a value (double) to the configuration file
 *
 * @param [in] value		The value to be written to the configuration file.
 * @param [in] section		The name of the section. Allowed characters: a-z, A-Z, 0-9, underscore (_), dot(.) and space ( ).
 * @param [in] key			The name of the key. Allowed characters: a-z, A-Z, 0-9, underscore (_), dot(.) and space ( ).
 *
 * @return Returns 'true' on success.
 *
 * @see readDouble()
 */
bool ConfigFile::writeDouble(double value, std::string const &section, std::string const &key) const
{
	std::stringstream valueStr;
	valueStr.precision(25);
	valueStr << value;

	if (writeConfigKey(valueStr.str(), section, key) == true)
	{
		return true;
	}
	else
	{
		return false;
	}
}



/**
 * @brief Reads a value (long double) from the configuration file
 *
 * @param [out] dst				Destination reference, where the value is to be copied.
 * @param [in]  section			The name of the section. Allowed characters: a-z, A-Z, 0-9, underscore (_), dot(.) and space ( ).
 * @param [in]  key				The name of the key. Allowed characters: a-z, A-Z, 0-9, underscore (_), dot(.) and space ( ).
 * @param [in]  defaultValue	The default value if the searched entry is not found or an error occurs
 *
 * @return Returns 'true' if the searched key was found.
 *
 * @see writeLongDouble()
 */
bool ConfigFile::readLongDouble(long double &dst, std::string const &section, std::string const &key, long double defaultValue) const
{
	std::string value = "";

	if (readConfigKey(value, section, key) == true)
	{
		dst = std::stold(value);
		return true;
	}
	else
	{
		dst = defaultValue;
		return false;
	}
}



/**
 * @brief Writes a value (long double) to the configuration file
 *
 * @param [in] value		The value to be written to the configuration file.
 * @param [in] section		The name of the section. Allowed characters: a-z, A-Z, 0-9, underscore (_), dot(.) and space ( ).
 * @param [in] key			The name of the key. Allowed characters: a-z, A-Z, 0-9, underscore (_), dot(.) and space ( ).
 *
 * @return Returns 'true' on success.
 *
 * @see readLongDouble()
 */
bool ConfigFile::writeLongDouble(long double value, std::string const &section, std::string const &key) const
{
	std::stringstream valueStr;
	valueStr.precision(25);
	valueStr << value;

	if (writeConfigKey(valueStr.str(), section, key) == true)
	{
		return true;
	}
	else
	{
		return false;
	}
}



/**
 * @brief Reads a value (string) from the configuration file
 *
 * @param [out] dst				Destination reference, where the value is to be copied.
 * @param [in]  section			The name of the section. Allowed characters: a-z, A-Z, 0-9, underscore (_), dot(.) and space ( ).
 * @param [in]  key				The name of the key. Allowed characters: a-z, A-Z, 0-9, underscore (_), dot(.) and space ( ).
 * @param [in]  defaultValue	The default value if the searched entry is not found or an error occurs
 *
 * @return Returns 'true' if the searched key was found.
 *
 * @see writeString()
 */
bool ConfigFile::readString(std::string &dst, std::string const &section, std::string const &key, std::string const &defaultValue) const
{
	if (readConfigKey(dst, section, key) == true)
	{
		return true;
	}
	else
	{
		dst = defaultValue;
		return false;
	}
}



/**
 * @brief Writes a value (string) to the configuration file
 *
 * @param [in] value		The value to be written to the configuration file.
 * @param [in] section		The name of the section. Allowed characters: a-z, A-Z, 0-9, underscore (_), dot(.) and space ( ).
 * @param [in] key			The name of the key. Allowed characters: a-z, A-Z, 0-9, underscore (_), dot(.) and space ( ).
 *
 * @return Returns 'true' on success.
 *
 * @see readString()
 */
bool ConfigFile::writeString(std::string const &value, std::string const &section, std::string const &key) const
{
	if (writeConfigKey(value, section, key) == true)
	{
		return true;
	}
	else
	{
		return false;
	}
}



// =========================================================================================================================================
// ==========================================================  private functions  ==========================================================
// =========================================================================================================================================

/**
 * @brief Checks the name of the section. Allowed characters: a-z, A-Z, 0-9, underscore (_), dot(.) and space ( ).
 *
 * @return Returns 'true' if the section name is valid.
 *
 * @see isKeyNameValid()
 */
bool ConfigFile::isSectionNameValid(std::string const &section) const
{
	std::string allowedChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_. ";

	if ((section != "") && (section.find_first_not_of(allowedChars) == std::string::npos))
	{
		return true;
	}
	else
	{
		return false;
	}
}



/**
 * @brief Checks the name of the key. Allowed characters: a-z, A-Z, 0-9, underscore (_), dot(.) and space ( ).
 *
 * @return Returns 'true' if the key name is valid.
 *
 * @see isSectionNameValid()
 */
bool ConfigFile::isKeyNameValid(std::string const &key) const
{
	std::string allowedChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_. ";

	if ((key != "") && (key.find_first_not_of(allowedChars) == std::string::npos))
	{
		return true;
	}
	else
	{
		return false;
	}
}




/**
 * @brief Reads a value as string from the configuration file
 *
 * @param [out] dst				Destination string, where the value is to be copied.
 * @param [in]  section			The name of the section. Allowed characters: a-z, A-Z, 0-9, underscore (_), dot(.) and space ( ).
 * @param [in]  key				The name of the key. Allowed characters: a-z, A-Z, 0-9, underscore (_), dot(.) and space ( ).
 *
 * @return Returns 'true' if the searched key was found.
 *
 * @see writeConfigKey()
 */
bool ConfigFile::readConfigKey(std::string &dst, std::string section, std::string key) const
{
	std::ifstream		file;
    std::string			fileLine;
    std::string 		tmpString;
    std::string			keyValue;
	std::stringstream 	tmpSection;
	std::vector<std::string> configEntries;
    bool keyFound	= false;
    bool breakLoop	= false;
    int  entries	= 0;

    // check parameters
    if ((getConfigFile() == "") || (isSectionNameValid(section) == false) || (isKeyNameValid(key) == false))
    {
    	return false;
    }

    // convert 'section' and 'key' to lower case
    tmpSection << "[" << section << "]";
    section = tmpSection.str();
    transform(section.begin(), section.end(), section.begin(),::tolower);
    transform(key.begin(), key.end(), key.begin(),::tolower);

    // open and check file
    file.open(getConfigFile());
    if (file.is_open() == true)
    {

    	// FIXME Problem mit GetLine (exception). Mit folgenden code ergänzen!!!!!
    	/*
		// peek is used to determine whether there is another character to be read by getline()
    	while(!inStream.eof() && inStream.peek() != std::char_traits<char>::eof())
		*/


    	// read whole configuration file
        while (std::getline(file, fileLine))
        {
        	configEntries.push_back(fileLine);
        }
        entries = static_cast<int>(configEntries.size());
        file.close();
    }
    else
    {
    	return false;
    }


	// processing of the configuration file
	for(int i=0; i<entries; i++)
	{
		tmpString = configEntries.at(i);

		// remove leading spaces and tabs and convert the string to lower case
		tmpString.erase(0, tmpString.find_first_not_of(" \t"));
		tmpString.erase(tmpString.find_last_not_of(" \n\f\v\r")+1);
		transform(tmpString.begin(), tmpString.end(), tmpString.begin(),::tolower);

		// next processing level, if the section is found
		if (tmpString.find(section) == 0)
		{
			i++;
			while (i<entries && breakLoop == false)
			{
				tmpString = configEntries.at(i);

				// remove leading spaces and tabs and convert the string to lower case
				tmpString.erase(0, tmpString.find_first_not_of(" \t"));
				transform(tmpString.begin(), tmpString.end(), tmpString.begin(),::tolower);

				// the line is a comment
				if (tmpString.find(";") == 0)
				{
					keyFound = false;
					breakLoop = false;
				}

				// the line is a new section
				else if (tmpString.find("[") == 0)
				{
					keyFound = false;
					breakLoop = true;
				}

				// the line is a key
				else if (tmpString.find(key) == 0)
				{
					keyValue = configEntries.at(i);

					// erase the key name, spaces, and tabs from the string;
					keyValue.erase(0, keyValue.find_first_not_of(" \t")); // leading spaces and tabs
					keyValue.erase(0, key.length());
					keyValue.erase(0, keyValue.find_first_not_of(" \t"));

					// the next character must be '=' (otherwise we have the wrong key)
					if (keyValue.find("=") == 0)
					{
						// remove the spaces, 'new lines' and tabs at the beginning and at the end of the value
						keyValue.erase(0, keyValue.find_first_not_of("= \t"));
						keyValue.erase(keyValue.find_last_not_of(" \n\f\v\r")+1);
						keyFound = true;

						breakLoop = true;
					}
					else
					{
						keyValue.clear();
						keyFound = false;
						breakLoop = false;
					}
				}
				i++;
			} // while
			break;
		} // if
	}
	if (keyFound == true)
	{
		dst = keyValue;
		return true;
	}
	else
	{
		return false;
	}
}



/**
 * @brief Writes a value as string to the configuration file
 *
 * @param [in] value		The value to be written to the configuration file.
 * @param [in] section		The name of the section. Allowed characters: a-z, A-Z, 0-9, underscore (_), dot(.) and space ( ).
 * @param [in] key			The name of the key. Allowed characters: a-z, A-Z, 0-9, underscore (_), dot(.) and space ( ).
 *
 * @return Returns 'true' on success.
 *
 * @see readConfigKey()
 */
bool ConfigFile::writeConfigKey(std::string const &value, std::string section, std::string key) const
{
	std::fstream		file;

    std::string			fileLine;
    std::string 		tmpString;
    std::string			keyValue;
    std::string			sectionLC; 	// lower case version of 'section'
    std::string			keyLC;		// lower case version of 'key'

    std::stringstream 	tmpStream;
    std::vector<std::string> configEntries;

	bool sectionFound	= false;
	bool keyFound		= false;
	bool breakLoop		= false;

	std::string::size_type sectionStart	= 0;
	std::string::size_type sectionEnd	= 0;
	std::string::size_type keyLine		= 0;
    std::string::size_type index		= 0;


    // check parameters
    if ((getConfigFile() == "") || (isSectionNameValid(section) == false) || (isKeyNameValid(key) == false))
    {
    	return false;
    }

    // convert 'section' and 'key' to lower case
    tmpStream << "[" << section << "]";
    sectionLC = tmpStream.str();
    keyLC = key;
    transform(sectionLC.begin(), sectionLC.end(), sectionLC.begin(),::tolower);
    transform(keyLC.begin(), keyLC.end(), keyLC.begin(),::tolower);

    // open and check file
    file.open(getConfigFile(), std::ios_base::in);
    if (file.is_open() == true)
    {

    	// FIXME Problem mit GetLine (exception). Mit folgenden code ergänzen!!!!!
    	/*
		// peek is used to determine whether there is another character to be read by getline()
    	while(!inStream.eof() && inStream.peek() != std::char_traits<char>::eof())
		*/

    	// read whole configuration file
        while (std::getline(file, fileLine))
        {
        	configEntries.push_back(fileLine);
        }
        file.close();
    }

	// searching for the 'section' and the 'key' entry
    while (index<configEntries.size() && breakLoop == false)
	{
		tmpString = configEntries.at(index);

		// remove leading spaces and tabs and convert the string to lower case
		tmpString.erase(0, tmpString.find_first_not_of(" \t"));
		transform(tmpString.begin(), tmpString.end(), tmpString.begin(),::tolower);

		// next processing level, if the section is found
		if (tmpString.find(sectionLC) == 0)
		{
			keyFound = false;
			sectionFound = true;
			sectionStart = index;
			sectionEnd = index;
			index++;

			while (index<configEntries.size() && breakLoop == false)
			{
				tmpString = configEntries.at(index);

				// remove leading spaces and tabs and convert the string to lower case
				tmpString.erase(0, tmpString.find_first_not_of(" \t"));
				transform(tmpString.begin(), tmpString.end(), tmpString.begin(),::tolower);

				// the line is a comment
				if (tmpString.find(";") == 0)
				{
					breakLoop = false;
				}

				// the line is a new section
				else if (tmpString.find("[") == 0)
				{
					sectionEnd = index-1;
					breakLoop = true;
				}

				// the line is a key
				else if (tmpString.find(keyLC) == 0)
				{
					keyValue = configEntries.at(index);

					// erase the key name, spaces and tabs from the string;
					keyValue.erase(0, keyValue.find_first_not_of(" \t")); // leading spaces and tabs
					keyValue.erase(0, keyLC.length());
					keyValue.erase(0, keyValue.find_first_not_of(" \t"));

					// the next character must be '=' (otherwise we have the wrong key)
					if (keyValue.find("=") == 0)
					{
						keyLine = index;
						keyValue.clear();
						keyFound = true;
						breakLoop = true;
					}
					else
					{
						keyValue.clear();
						keyFound = false;
						breakLoop = true;
					}
				}

				// the end of the file has been reached
				else if (index == configEntries.size()-1)
				{
					sectionEnd = index;
					breakLoop = true;
				}
				index++;
			} // while
		} // if
		index++;
	} //while


    // change the value of the found key
	if (keyFound == true)
	{
		tmpStream.str("");
		tmpStream << key << " = " << value;
		configEntries.at(keyLine) = tmpStream.str();
	}

	// the key doesn't exist
	else
	{
		// the section exist
		if (sectionFound == true)
		{
			tmpStream.str("");
			tmpStream << key << " = " << value;

			// search a useful line and add the new key
			for (std::string::size_type i = sectionEnd; (i >= sectionStart) && (i < configEntries.size()); i--)
			{
				// end of the file
				if (i == configEntries.size()-1)
				{
					configEntries.push_back(tmpStream.str());
					break;
				}

				// the found section is empty
				else if (sectionEnd == sectionStart)
				{
					configEntries.insert(configEntries.begin() + i + 1, "");
					configEntries.insert(configEntries.begin() + i + 1, tmpStream.str());
					break;
				}

				// add the key between the last section entry and the following blank line
				else if ((configEntries.at(i) != "") && (configEntries.at(i+1) == ""))
				{
					configEntries.insert(configEntries.begin() + i + 1, tmpStream.str());
					break;
				}

				// add the key between the last section entry and the next section (...and add blank line between both)
				else if ((configEntries.at(i) != "") && (i == sectionEnd))
				{
					configEntries.insert(configEntries.begin() + i + 1, "");
					configEntries.insert(configEntries.begin() + i + 1, tmpStream.str());
					break;
				}
			}
		}

		// the section doesn't exist
		else
		{
			// don't add a blank line, if the file is empty
			if (configEntries.size() != 0)
			{
				configEntries.push_back("");
			}

			// add the section
			tmpStream.str("");
			tmpStream << "[" << section << "]";
			configEntries.push_back(tmpStream.str());

			// add the key
			tmpStream.str("");
			tmpStream << key << " = " << value;
			configEntries.push_back(tmpStream.str());
		}
	}

	// write the configuration file
    file.open(getConfigFile(), std::ios_base::out);
    if (file.is_open() == true)
    {
    	for (std::string::size_type i=0; i<configEntries.size(); i++)
    	{
    		file <<configEntries.at(i) << std::endl;
    	}
        file.close();
    }
    else
    {
    	return false;
    }

	return true;
}
// END OF FILE
