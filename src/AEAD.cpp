#include "AEAD.h"
#include "aes_siv.h"
#include "./Logger/Logger.h"
#include "generalFunctions.h"

// RAII-compliant deleter for low-level functions
struct AEAD_CTX_deleter
{
	void operator()(AES_SIV_CTX *ptr)
	{
		if (ptr != nullptr)
		{
			AES_SIV_CTX_cleanup(ptr);
			AES_SIV_CTX_free(ptr);
		}
	}
};
typedef std::unique_ptr<AES_SIV_CTX, AEAD_CTX_deleter> AEAD_CTX_Guard;



void AEAD::setAeadAlgorithm(AeadAlgorithm algorithm)
{
	this->m_AeadAlgorithm = algorithm;
}


void AEAD::setAeadAlgorithm(unsigned short algorithm)
{
	this->m_AeadAlgorithm = static_cast<AeadAlgorithm>(algorithm);
}


AeadAlgorithm AEAD::getAeadAlgorithm() const
{
	return m_AeadAlgorithm;
}


unsigned short AEAD::getAeadAlgorithmID() const
{
	return static_cast<unsigned short>(m_AeadAlgorithm);
}


/**
* @brief	Sets the Key as a copy of the passed parameter.
*/
void AEAD::setKey(std::vector<unsigned char> const & key)
{
	this->m_key = key;
}


const std::vector<unsigned char>& AEAD::getKey() const
{
	return m_key;
}


unsigned int AEAD::getKeySize()
{
	return static_cast<unsigned int>(m_key.size());
}


/**
* @brief	Sets the Nonce as a copy of the passed parameter.
*/
void AEAD::setNonce(std::vector<unsigned char> const & nonce)
{
	this->m_nonce = nonce;
}


const std::vector<unsigned char>& AEAD::getNonce() const
{
	return m_nonce;
}


unsigned int AEAD::getNonceSize()
{
	return static_cast<unsigned int>(m_nonce.size());
}


/**
* @brief	Sets the Plaintext as a copy of the passed parameter.
*/
void AEAD::setPlaintext(std::vector<unsigned char> const & plaintext)
{
	this->m_plaintext = plaintext;
}


const std::vector<unsigned char>& AEAD::getPlaintext() const
{
	return m_plaintext;
}


unsigned int AEAD::getPlaintextSize()
{
	return static_cast<unsigned int>(m_plaintext.size());
}


/**
* @brief	Sets the Associated Data as a copy of the passed parameter.
*/
void AEAD::setAssociatedData(std::vector<unsigned char> const & associatedData)
{
	this->m_associatedData = associatedData;
}


const std::vector<unsigned char>& AEAD::getAssociatedData() const
{
	return m_associatedData;
}


unsigned int AEAD::getAssociatedDataSize()
{
	return static_cast<unsigned int>(m_associatedData.size());
}


/**
* @brief	Sets the Ciphertext as a copy of the passed parameter.
*/
void AEAD::setCiphertext(std::vector<unsigned char> const & ciphertext)
{
	this->m_ciphertext = ciphertext;
}


const std::vector<unsigned char>& AEAD::getCiphertext() const
{
	return m_ciphertext;
}


unsigned int AEAD::getCiphertextSize()
{
	return static_cast<unsigned int>(m_ciphertext.size());
}


bool AEAD::encrypt()
{
	// check key and algorithm
	if (isAlgorithmAndKeyValid(m_AeadAlgorithm, m_key) == false)
	{
		LOG_ERROR("AEAD algorithm and key do not match");
		return false;
	}

	// check nonce (MAY be zero)
	unsigned char const *nonce = nullptr;
	size_t nonceSize = 0;
	if (m_nonce.size() == 0)
	{
		LOG_WARN("no nonce was defined for the AEAD operation");
		nonce = nullptr;
		nonceSize = 0;
	}
	else
	{
		nonce = m_nonce.data();
		nonceSize = m_nonce.size();
	}

	// check associated data (MAY be zero)
	unsigned char const *associatedData = nullptr;
	size_t associatedDataSize = 0;
	if (m_associatedData.size() == 0)
	{
		associatedData = nullptr;
		associatedDataSize = 0;
	}
	else
	{
		associatedData = m_associatedData.data();
		associatedDataSize = m_associatedData.size();
	}

	// check plaintext (MAY be zero)
	unsigned char const *plaintext = nullptr;
	size_t plaintextSize = 0;
	if (m_plaintext.size() == 0)
	{
		plaintext = nullptr;
		plaintextSize = 0;
	}
	else
	{
		plaintext = m_plaintext.data();
		plaintextSize = m_plaintext.size();
	}

	// AEAD encryption
	size_t aeadOutSize = plaintextSize + c_authTagSize;
	std::vector<unsigned char> aeadOut(aeadOutSize);
	AEAD_CTX_Guard ctx(AES_SIV_CTX_new());

	int status = AES_SIV_Encrypt(ctx.get(),
								 aeadOut.data(), &aeadOutSize,
								 m_key.data(), m_key.size(),
								 nonce, nonceSize,
								 plaintext, plaintextSize,
								 associatedData, associatedDataSize);
	
	if (status == 0)
	{
		LOG_ERROR("AEAD encryption failed");
		return false;
	}
	else
	{
		m_ciphertext = std::move(aeadOut);
		return true;
	}
}


bool AEAD::decrypt()
{
	// check key and algorithm
	if (isAlgorithmAndKeyValid(m_AeadAlgorithm, m_key) == false)
	{
		LOG_ERROR("AEAD algorithm and key do not match");
		return false;
	}

	// check ciphertext
	if (m_ciphertext.size() < c_authTagSize)
	{
		LOG_ERROR("invalid size of 'ciphertext'"); 
		return false;
	}

	// check nonce (MAY be zero)
	unsigned char const *nonce = nullptr;
	size_t nonceSize = 0;
	if (m_nonce.size() == 0)
	{
		LOG_WARN("no nonce was defined for the AEAD operation");
		nonce = nullptr;
		nonceSize = 0;
	}
	else
	{
		nonce = m_nonce.data();
		nonceSize = m_nonce.size();
	}

	// check associated data (MAY be zero)
	unsigned char const *associatedData = nullptr;
	size_t associatedDataSize = 0;
	if (m_associatedData.size() == 0)
	{
		associatedData = nullptr;
		associatedDataSize = 0;
	}
	else
	{
		associatedData = m_associatedData.data();
		associatedDataSize = m_associatedData.size();
	}

	// AEAD decryption
	size_t aeadOutSize = m_ciphertext.size() - c_authTagSize;
	std::vector<unsigned char> aeadOut(aeadOutSize);
	AEAD_CTX_Guard ctx(AES_SIV_CTX_new());

	printf("key      %u %s\n", m_key.size(), binToHexStr(m_key, "", 1024, true).c_str());
	printf("nonce    %u %s\n", m_nonce.size(), binToHexStr(m_nonce, "", 1024, true).c_str());
	printf("enc      %u %s\n", m_ciphertext.size(), binToHexStr(m_ciphertext, "", 1024, true).c_str());
	printf("add      %u %s\n", m_associatedData.size(), binToHexStr(m_associatedData, "", 1024, true).c_str());

	int status = AES_SIV_Decrypt(ctx.get(),
								 aeadOut.data(), &aeadOutSize,
								 m_key.data(), m_key.size(),
								 nonce, nonceSize,
								 m_ciphertext.data(), m_ciphertext.size(),
								 associatedData, associatedDataSize);

	if (status == 0)
	{
		LOG_ERROR("AEAD decryption failed");
		return false;
	}
	else
	{
		m_plaintext = std::move(aeadOut);
		return true;
	}
}


/**
* @brief	Performs an AEAD encryption without the use of member variables
*/
bool AEAD::directEncrypt(AeadAlgorithm aeadAlgorithm, 
						 std::vector<unsigned char> const & key, 
						 std::vector<unsigned char> const & plaintext, 
						 std::vector<unsigned char>& ciphertext, 
						 std::vector<unsigned char> const & nonce, 
						 std::vector<unsigned char> const & associatedData)
{
	// check key and algorithm
	if (isAlgorithmAndKeyValid(aeadAlgorithm, key) == false)
	{
		LOG_ERROR("AEAD algorithm and key do not match");
		return false;
	}

	// check nonce (MAY be zero)
	unsigned char const *noncePtr = nullptr;
	size_t nonceSize = 0;
	if (nonce.size() == 0)
	{
		LOG_WARN("no nonce was defined for the AEAD operation");
		noncePtr = nullptr;
		nonceSize = 0;
	}
	else
	{
		noncePtr = nonce.data();
		nonceSize = nonce.size();
	}

	// check associated data (MAY be zero)
	unsigned char const *associatedDataPtr = nullptr;
	size_t associatedDataSize = 0;
	if (associatedData.size() == 0)
	{
		associatedDataPtr = nullptr;
		associatedDataSize = 0;
	}
	else
	{
		associatedDataPtr = associatedData.data();
		associatedDataSize = associatedData.size();
	}

	// check plaintext (MAY be zero)
	unsigned char const *plaintextPtr = nullptr;
	size_t plaintextSize = 0;
	if (plaintext.size() == 0)
	{
		plaintextPtr = nullptr;
		plaintextSize = 0;
	}
	else
	{
		plaintextPtr = plaintext.data();
		plaintextSize = plaintext.size();
	}

	// AEAD encryption
	size_t aeadOutSize = plaintextSize + c_authTagSize;
	std::vector<unsigned char> aeadOut(aeadOutSize);
	AEAD_CTX_Guard ctx(AES_SIV_CTX_new());
	
	int status = AES_SIV_Encrypt(ctx.get(),
								 aeadOut.data(), &aeadOutSize,
								 key.data(), key.size(),
								 noncePtr, nonceSize,
								 plaintextPtr, plaintextSize,
								 associatedDataPtr, associatedDataSize);

	if (status == 0)
	{
		LOG_ERROR("AEAD encryption failed");
		return false;
	}
	else
	{
		ciphertext = std::move(aeadOut);
		return true;
	}
}


/**
* @brief	Performs an AEAD decryption without the use of member variables
*/
bool AEAD::directDecrypt(AeadAlgorithm aeadAlgorithm, 
						 std::vector<unsigned char> const & key, 
						 std::vector<unsigned char>& plaintext, 
						 std::vector<unsigned char> const & ciphertext, 
						 std::vector<unsigned char> const & nonce, 
						 std::vector<unsigned char> const & associatedData)
{
	// check key and algorithm
	if (isAlgorithmAndKeyValid(aeadAlgorithm, key) == false)
	{
		LOG_ERROR("AEAD algorithm and key do not match");
		return false;
	}

	// check ciphertext
	if (ciphertext.size() < c_authTagSize)
	{
		LOG_ERROR("invalid size of 'ciphertext'");
		return false;
	}

	// check nonce (MAY be zero)
	unsigned char const *noncePtr = nullptr;
	size_t nonceSize = 0;
	if (nonce.size() == 0)
	{
		LOG_WARN("no nonce was defined for the AEAD operation");
		noncePtr = nullptr;
		nonceSize = 0;
	}
	else
	{
		noncePtr = nonce.data();
		nonceSize = nonce.size();
	}

	// check associated data (MAY be zero)
	unsigned char const *associatedDataPtr = nullptr;

	size_t associatedDataSize = 0;
	if (associatedData.size() == 0)
	{
		associatedDataPtr = nullptr;
		associatedDataSize = 0;
	}
	else
	{
		associatedDataPtr = associatedData.data();
		associatedDataSize = associatedData.size();
	}

	// AEAD decryption
	size_t aeadOutSize = ciphertext.size() - c_authTagSize;
	std::vector<unsigned char> aeadOut(aeadOutSize);
	AEAD_CTX_Guard ctx(AES_SIV_CTX_new());

	int status = AES_SIV_Decrypt(ctx.get(),
								 aeadOut.data(), &aeadOutSize,
								 key.data(), key.size(),
								 noncePtr, nonceSize,
								 ciphertext.data(), ciphertext.size(),
								 associatedDataPtr, associatedDataSize);

	if (status == 0)
	{
		LOG_ERROR("AEAD decryption failed");
		return false;
	}
	else
	{
		plaintext = std::move(aeadOut);
		return true;
	}
}


/**
* @brief	Returns 'true' if the key length matches the algorithm
*/
bool AEAD::isAlgorithmAndKeyValid(AeadAlgorithm algorithm, std::vector<unsigned char> const & key)
{
	switch (algorithm)
	{
	case AeadAlgorithm::NOT_DEFINED:
		LOG_WARN("the AEAD algorithm not set");
		return false;

	case AeadAlgorithm::AEAD_AES_SIV_CMAC_256:
		if (key.size() != 32)
		{
			return false;
		}
		break;

	case AeadAlgorithm::AEAD_AES_SIV_CMAC_384:
		if (key.size() != 48)
		{
			return false;
		}
		break;

	case AeadAlgorithm::AEAD_AES_SIV_CMAC_512:
		if (key.size() != 64)
		{
			return false;
		}
		break;

	default:
		LOG_WARN("AEAD: AEAD algorithm not supported");
		return false;
	}
	return true;
}


/**
* @brief	Deletes the content of the AEAD object.
*/
void AEAD::clear()
{
	m_AeadAlgorithm = static_cast<AeadAlgorithm>(0);
	m_key.clear();
	m_nonce.clear();
	m_plaintext.clear();
	m_associatedData.clear();
	m_ciphertext.clear();
}


bool AEAD::isSupported(AeadAlgorithm aeadAlgorithm)
{
	switch (aeadAlgorithm)
	{
	case AeadAlgorithm::AEAD_AES_SIV_CMAC_256:
		return true;

	case AeadAlgorithm::AEAD_AES_SIV_CMAC_384:
		return true;

	case AeadAlgorithm::AEAD_AES_SIV_CMAC_512:
		return true;

	default:
		return false;
	}
}


bool AEAD::isSupported(unsigned short aeadAlgorithm)
{
	return AEAD::isSupported(static_cast<AeadAlgorithm>(aeadAlgorithm));
}


unsigned int AEAD::getMaxNonceLength(AeadAlgorithm aeadAlgorithm)
{
	switch (aeadAlgorithm)
	{
	case AeadAlgorithm::AEAD_AES_SIV_CMAC_256:
		return c_unlimitedNoneLength;

	case AeadAlgorithm::AEAD_AES_SIV_CMAC_384:
		return c_unlimitedNoneLength;

	case AeadAlgorithm::AEAD_AES_SIV_CMAC_512:
		return c_unlimitedNoneLength;

	default:
		LOG_WARN("AeadAlgorithm is not supported");
		return 0;
	}
}


unsigned int AEAD::getMaxNonceLength(unsigned short aeadAlgorithm)
{
	return AEAD::getMaxNonceLength(static_cast<AeadAlgorithm>(aeadAlgorithm));
}
