/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#pragma once
#include "NtsConfig.h"
#include <string>
#include <vector>

typedef struct NtsSettings
{
	NtsSettings() :
		general(),
		//console_logging(),
		//file_logging(),
		//NTPv4(),
		server()
		//client()
	{
	}





	struct general
	{
		NtsMode ntsServiceMode = NtsMode::not_set;
	} general;

	//struct console_logging
	//{
	//	bool enableLogToConsole = false;
	//	std::string logLevel = "";
	//} console_logging;

	//struct file_logging
	//{
	//	bool enableLogToFile = false;
	//	std::string logLevel = "";
	//	std::string fileName = "";
	//	std::string targetLogFolder = "";
	//	bool enableLogRotate = false;
	//	unsigned int maxLogFiles = 0;
	//	unsigned int maxFileSize = 0;
	//	unsigned int minFreeSpace = 0;
	//	unsigned int maxFolderSize = 0;
	//} file_logging;

	//struct NTPv4
	//{
	//	unsigned short ntpEF_uniqueId = 0;
	//	unsigned short ntpEF_ntsCookie = 0;
	//	unsigned short ntpEF_ntsCookiePlaceholder = 0;
	//	unsigned short ntpEF_ntsAuthAndEncEFs = 0;
	//} NTPv4;

	struct server
	{
		unsigned long tlsPort = 0;
		AeadAlgorithm aeadAlgoForMasterKey = AeadAlgorithm::NOT_DEFINED;
		//MessageDigestAlgo mdAlgoForMasterKeyHkdf = MessageDigestAlgo::Not_Defined;
		//unsigned long amountOfCookies = 0;
		unsigned long keyRotationHistory = 0;
		unsigned long keyRotationIntervalSeconds = 0;
		std::string serverCertChainFile = "";
		std::string serverPrivateKey = "";
		//std::vector<NextProtocol> supportedNextProtocols;
		//std::vector<AeadAlgorithm> supportedAeadAlgos;
	} server;

	//struct client
	//{
	//	std::string rootCaBundleFile = "";
	//	std::vector<NextProtocol> supportedNextProtocols;
	//	std::vector<AeadAlgorithm> supportedAeadAlgos;
	//} client;



	
} NtsSettings;

NtsSettings readConfigFile(std::string const &configFile);

