/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#pragma once

#include <stdlib.h>

#include <openssl/obj_mac.h>

 //todo: macros mit constexpr ersetzen

//#define RECORD_HEADER_SIZE						4	// size of the header in byte
#define STR_EXPAND(token) #token
#define STR(token) STR_EXPAND(token)

// version info 
// todo: auslagern in version.h
#define NTS_MAJOR_VERSION						0
#define NTS_MINOR_VERSION						5
#define NTS_PATCH_VERSION						7
extern const char NTS_COMPILE_TIME[];
constexpr char NTS_VERSION[]					= STR(NTS_MAJOR_VERSION) "." STR(NTS_MINOR_VERSION) "." STR(NTS_PATCH_VERSION);

// NTS specifications --> nts4ntp-12
#define UNIQUE_IDENTIFIER_SIZE					32 //bytes
#define TLS_PORT								4443
#define TLS_BUFFER_SIZE							4096
#define TLS_Exporter_Label						"EXPORTER-nts/1" // TODO: bug in OpenSSL 1.1.1 --> "EXPORTER-network-time-security/1"
#define TLS_ALPN_LIST							"ntske/1"
#define TLS_C2S_KEY_ID							0x00
#define TLS_S2C_KEY_ID							0x01

// NTS settings
#define IP_ADDRESS								"127.0.0.1" // 71.184.230.225
#define NONCE_SIZE								16 // bytes
#define SERVER_AEAD_ALGO						AeadAlgorithm::AEAD_AES_SIV_CMAC_512
//#define SERVER_DIGEST_ALGO						MessageDigestAlgo::SHA512
#define MAX_COOKIES								8 // max amount of cookies

#define CERT_ROOT_CA							"rootCaBundle.pem"



// NTP specifications --> rfc 5905 (NTPv4)
enum class NtpMode
{
	Client_mode = 3,
	Server_mode = 4
};


enum class NtsState
{
	not_init,
	init
};


enum class NtsMode
{
	not_set,
	client,
	server
};

enum class RecordType : unsigned short
{
	End_of_Message = 0,
	Next_Protocol_Negotiation = 1,
	Error = 2,
	Warning = 3,
	AEAD_Algorithm_Negotiation = 4,
	New_Cookie_for_NTPv4 = 5,
	NTPv4_Server_Negotiation = 6,
	NTPv4_Port_Negotiation = 7
};


enum class RecordErr : unsigned short
{
	Undefined = 0,
	Unrecognized_Critical_Record = 1,
	Bad_Request = 2
};


enum class RecordWarn : unsigned short
{
	Undefined = 0 // not defined yet
};


enum class NextProtocol : unsigned short
{
	NTPv4 = 0x0000,
	Undefined = 0xFFFF
};

enum class MessageDigestAlgo // openSSL
{
	Not_Defined = NID_undef,
	MD4 = NID_md4,
	MD5 = NID_md5,
	SHA1 = NID_sha1,
	SHA224 = NID_sha224,
	SHA256 = NID_sha256,
	SHA384 = NID_sha384,
	SHA512 = NID_sha512,
	RIPEMD_160 = NID_ripemd160
};


enum class NtpExtFieldType : unsigned short // NTS-Draft: TBD  
{
	Unique_Identifier = 0x0104,
	NTS_Cookie = 0x0204,
	NTS_Cookie_Placeholder = 0x0304,
	NTS_Authenticator_and_Encrypted_Extension_Fields = 0x0404
};

enum class AeadAlgorithm : unsigned short
{
	NOT_DEFINED = 0,

	//https://www.iana.org/assignments/aead-parameters/aead-parameters.xhtml
	// RFC 5116
	AEAD_AES_128_GCM = 1,
	AEAD_AES_256_GCM = 2,
	AEAD_AES_128_CCM = 3,
	AEAD_AES_256_CCM = 4,

	// RFC 5282
	AEAD_AES_128_GCM_8 = 5,
	AEAD_AES_256_GCM_8 = 6,
	AEAD_AES_128_GCM_12 = 7,
	AEAD_AES_256_GCM_12 = 8,
	AEAD_AES_128_CCM_SHORT = 9,
	AEAD_AES_256_CCM_SHORT = 10,
	AEAD_AES_128_CCM_SHORT_8 = 11,
	AEAD_AES_256_CCM_SHORT_8 = 12,
	AEAD_AES_128_CCM_SHORT_12 = 13,
	AEAD_AES_256_CCM_SHORT_12 = 14,

	// RFC 5297
	AEAD_AES_SIV_CMAC_256 = 15,		// <----- für NTS erforderlich
	AEAD_AES_SIV_CMAC_384 = 16,
	AEAD_AES_SIV_CMAC_512 = 17,

	// RFC 6655
	AEAD_AES_128_CCM_8 = 18,
	AEAD_AES_256_CCM_8 = 19,

	// RFC 7253, Section 3.1
	AEAD_AES_128_OCB_TAGLEN128 = 20,
	AEAD_AES_128_OCB_TAGLEN96 = 21,
	AEAD_AES_128_OCB_TAGLEN64 = 22,
	AEAD_AES_192_OCB_TAGLEN128 = 23,
	AEAD_AES_192_OCB_TAGLEN96 = 24,
	AEAD_AES_192_OCB_TAGLEN64 = 25,
	AEAD_AES_256_OCB_TAGLEN128 = 26,
	AEAD_AES_256_OCB_TAGLEN96 = 27,
	AEAD_AES_256_OCB_TAGLEN64 = 28,

	// RFC 7539
	AEAD_CHACHA20_POLY1305 = 29

	// 30 - 32767 		Unassigned
	// 32768 - 65535 	Reserved for Private Use [RFC5116]
};
