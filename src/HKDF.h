/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#pragma once

#include <vector>
#include "NtsConfig.h"

/*
   This is a HKDF (HMAC-based Extract-and-Expand Key Derivation Function) wrapper for the OpenSSL library.
   
   Short information:
   ----------------------------
   derivedKey = HKDF(key, salt, ctxInfo, md)

   key:                Is the the Input Keying Material to be derived. It must be a high-entropy secret.
   salt (optional):    A non-secret, reusable random value that strengthens the randomness extraction step.
   ctxInfo (optional): An optional context and application specific information. Max. length of this parameter is 1024 bytes.
   md:				   A message digest algorithm required by the internal HMAC function.
   derivedKey:         The derived and possibly extended output key. The max. length of the Output Keying Material is (255 * HashLength).

   more detail:  https://www.openssl.org/docs/man1.1.0/crypto/EVP_PKEY_CTX_set1_hkdf_key.html
                 https://tools.ietf.org/html/rfc5869
*/

class HKDF
{
public:
	HKDF();
	~HKDF();

	// Input Key Material
	void setInputKeyMaterial(std::vector<unsigned char> const &inputKeyMaterial);
	const std::vector<unsigned char> &getInputKeyMaterial() const;

	// Salt
	void setSalt(std::vector<unsigned char> const &salt);
	const std::vector<unsigned char> &getSalt() const;

	// Context Info
	void setContextInfo(std::vector<unsigned char> const &contextInfo);
	const std::vector<unsigned char> &getContextInfo() const;

	// Message Digest Algorithm
	void setMessageDigestAlgorithm(MessageDigestAlgo msgDigestAlgo);
	MessageDigestAlgo getMessageDigestAlgorithm() const;

	// Key length and the derived key
	void setKeyOutputLength(size_t keyOutputLength);
	size_t getKeyOutputLength() const;
	const std::vector<unsigned char> &getDerivedKey() const;

	// Cryptographic operations
	bool performHKDF();

	// Misc
	void clear(bool keysOnly, bool secureErase = true);


private:
	std::vector<unsigned char> m_inputKeyMaterial;
	std::vector<unsigned char> m_salt;
	std::vector<unsigned char> m_contextInfo;
	std::vector<unsigned char> m_outputKey;

	MessageDigestAlgo m_msgDigestAlgo;
	size_t m_keyOutputLength;
};

