#pragma once
#include <iostream>
#include <vector>
#include <assert.h>

#include "..\generalFunctions.h"
#include "..\NtpExtensionField.h"
#include "..\ExtF_UniqueIdentifier.h"
#include "..\ExtF_NtsCookie.h"
#include "..\ExtF_NtsCookiePlaceholder.h"
#include "..\ExtF_NtsAuthAndEncEF.h"

// TODO: Review

void Test_ExF_1()
{
	std::vector<unsigned char> AEAD_EF_Value
	{
		0x00, 0x10,											// EF Value: nonce length ("16")		
		0x20, 0x21,	0x22, 0x23, 0x24, 0x25, 0x26, 0x27,		// EF Value: nonce
		0x28, 0x29,	0x2A, 0x2B, 0x2C, 0x2D, 0x2E, 0x2F,
		0xA0, 0xA1,	0xA2, 0xA3, 0xA4, 0xA5, 0xA6, 0xA7,		// EF Value: ciphertext
		0xA8, 0xA9,	0xAA, 0xAB, 0xAC, 0xAD, 0xAE, 0xAF,
		0x02, 0x02											// EF Value: padding ("2")
	};

	NtpExtensionField NTPEF(NtpExtFieldType::NTS_Authenticator_and_Encrypted_Extension_Fields, AEAD_EF_Value);

	// NTP Ext Field infos
	std::cout << "NTP EF: getFieldTypeVal: " << NTPEF.getFieldTypeVal() << std::endl;
	std::cout << "NTP EF: getLength: " << NTPEF.getValueLength() << std::endl;
	std::cout << "NTP EF: Value:\n" << binToHexStr(NTPEF.getValue(), " ", 8) << std::endl << std::endl;

	// NTP Ext Field: 'value' field
	std::cout << "isValid: " << ExtField::NtsAuthAndEncEF::isValid(NTPEF) << std::endl;
	std::cout << "getNonceLength: " << ExtField::NtsAuthAndEncEF::getNonceLength(NTPEF) << std::endl;
	std::cout << "getCiphertextLength: " << ExtField::NtsAuthAndEncEF::getCiphertextLength(NTPEF) << std::endl;
	std::cout << "getPaddingLength: " << ExtField::NtsAuthAndEncEF::getPaddingLength(NTPEF) << std::endl << std::endl;
	std::cout << "getNonce:\n" << binToHexStr(ExtField::NtsAuthAndEncEF::getNonce(NTPEF), " ", 8) << std::endl << std::endl;
	std::cout << "getCiphertext:\n" << binToHexStr(ExtField::NtsAuthAndEncEF::getCiphertext(NTPEF), " ", 8) << std::endl << std::endl;
}





void Test_ExF_2()
{

	std::vector<unsigned char> const key =
	{
		0xff, 0xfe, 0xfd, 0xfc, 0xfb, 0xfa, 0xf9, 0xf8,
		0xf7, 0xf6, 0xf5, 0xf4, 0xf3, 0xf2, 0xf1, 0xf0,
		0xf0, 0xf1, 0xf2, 0xf3, 0xf4, 0xf5, 0xf6, 0xf7,
		0xf8, 0xf9, 0xfa, 0xfb, 0xfc, 0xfd, 0xfe, 0xff
	};

	std::vector<unsigned char> const assocData =
	{
		0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17,
		0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f,
		0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27
	};

	std::vector<unsigned char> const nonce =
	{
		0xA0, 0xA1, 0xA2, 0xA3, 0xA4, 0xA5, 0xA6, 0xA7,
		0xA8, 0xA9, 0xAa, 0xAb, 0xAc, 0xAd, 0xAe, 0xAf
	};

	std::vector<unsigned char> const plaintext =
	{
		0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88,
		0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee
	};



	NtpExtensionField newEF;
	ExtField::NtsAuthAndEncEF::format(newEF);
	//ExtField::NtsAuthAndEncEF::performAead(newEF, AeadAlgorithm::AEAD_AES_SIV_CMAC_256, key, plaintext, nonce, assocData);
	ExtField::NtsAuthAndEncEF::performAead(newEF, AeadAlgorithm::AEAD_AES_SIV_CMAC_256, key, nonce, assocData);


	// NTP Ext Field infos
	std::cout << "NTP EF: getFieldTypeVal: " << newEF.getFieldTypeVal() << std::endl;
	std::cout << "NTP EF: getLength: " << newEF.getValueLength() << std::endl;
	std::cout << "NTP EF: Value:\n" << binToHexStr(newEF.getValue(), " ", 8) << std::endl << std::endl;

	// NTP Ext Field: 'value' field
	std::cout << "isValid: " << ExtField::NtsAuthAndEncEF::isValid(newEF) << std::endl;
	std::cout << "getNonceLength: " << ExtField::NtsAuthAndEncEF::getNonceLength(newEF) << std::endl;
	std::cout << "getCiphertextLength: " << ExtField::NtsAuthAndEncEF::getCiphertextLength(newEF) << std::endl;
	std::cout << "getPaddingLength: " << ExtField::NtsAuthAndEncEF::getPaddingLength(newEF) << std::endl << std::endl;
	std::cout << "getNonce:\n" << binToHexStr(ExtField::NtsAuthAndEncEF::getNonce(newEF), " ", 8) << std::endl << std::endl;
	std::cout << "getCiphertext:\n" << binToHexStr(ExtField::NtsAuthAndEncEF::getCiphertext(newEF), " ", 8) << std::endl << std::endl;

}