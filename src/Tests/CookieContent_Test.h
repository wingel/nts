#pragma once
#include <iostream>
#include <vector>
#include <assert.h>

#include "..\generalFunctions.h"
#include "..\CookieContent.h"

// TODO: Review

void Test_CookieContent_1()
{


	std::vector<unsigned char> const s2c =
	{
		0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17,
		0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f
	};

	std::vector<unsigned char> const c2s =
	{
		0xA0, 0xA1, 0xA2, 0xA3, 0xA4, 0xA5, 0xA6, 0xA7,
		0xA8, 0xA9, 0xAa, 0xAb, 0xAc, 0xAd, 0xAe, 0xAf
	};

	AeadAlgorithm algo = AeadAlgorithm::AEAD_AES_SIV_CMAC_256;

	CookieContent CC(algo, s2c, c2s);

	std::cout << "isValid: " << CC.isValid() << std::endl;
	std::cout << "getSerializedData:\n" << binToHexStr(CC.getSerializedData(), " ", 8) << std::endl << std::endl;

	CookieContent CC2(CC.getSerializedData());

	std::cout << "isValid: " << CC2.isValid() << std::endl;
	std::cout << "getSerializedData:\n" << binToHexStr(CC2.getSerializedData(), " ", 8) << std::endl << std::endl;

	std::cout << "s2c len: " << CC2.getS2CKeyLength() << std::endl;
	std::cout << "s2c:\n" << binToHexStr(CC2.getS2CKey(), " ", 8) << std::endl << std::endl;

	std::cout << "c2s len: " << CC2.getC2SKeyLength() << std::endl;
	std::cout << "c2s:\n" << binToHexStr(CC2.getC2SKey(), " ", 8) << std::endl << std::endl;

	std::cout << "algo ID: " << CC2.getAeadAlgorithmVal() << std::endl;
}