#pragma once
#include <iostream>
#include <vector>
#include <assert.h>

#include "..\generalFunctions.h"
#include "..\AEAD.h"

// TODO: Review


void TEST_AEAD_1() // without nonce
{
	// test vectors: rfc5297 --> Appendix A.1
	std::vector<unsigned char> const key =
	{
		0xff, 0xfe, 0xfd, 0xfc, 0xfb, 0xfa, 0xf9, 0xf8,
		0xf7, 0xf6, 0xf5, 0xf4, 0xf3, 0xf2, 0xf1, 0xf0,
		0xf0, 0xf1, 0xf2, 0xf3, 0xf4, 0xf5, 0xf6, 0xf7,
		0xf8, 0xf9, 0xfa, 0xfb, 0xfc, 0xfd, 0xfe, 0xff
	};

	std::vector<unsigned char> const assocData =
	{
		0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17,
		0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f,
		0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27
	};

	std::vector<unsigned char> const plaintext =
	{
		0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88,
		0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee};

	std::vector<unsigned char> const chipertext =
	{
		0x85, 0x63, 0x2d, 0x07, 0xc6, 0xe8, 0xf3, 0x7f,
		0x95, 0x0a, 0xcd, 0x32, 0x0a, 0x2e, 0xcc, 0x93,
		0x40, 0xc0, 0x2b, 0x96, 0x90, 0xc4, 0xdc, 0x04,
		0xda, 0xef, 0x7f, 0x6a, 0xfe, 0x5c
	};


	std::cout << "ENCRYPTION" << std::endl;
	AEAD aeadEnc;
	aeadEnc.setAeadAlgorithm(AeadAlgorithm::AEAD_AES_SIV_CMAC_256);
	aeadEnc.setKey(key);
	aeadEnc.setAssociatedData(assocData);
	aeadEnc.setPlaintext(plaintext);
	aeadEnc.encrypt();

	std::cout << "cipertext size: " << aeadEnc.getCiphertextSize() << std::endl;
	std::cout << "cipertext content:\n" << binToHexStr(aeadEnc.getCiphertext(), " ", 8) << std::endl;
	std::cout << "\n\n";
	std::cout << "expected cipertext content:\n" << binToHexStr(chipertext, " ", 8) << std::endl;
	std::cout << "\n\n";
	std::cout << "cipertext equal: " << (aeadEnc.getCiphertext() == chipertext ? "yes" : "no") << std::endl;

	std::cout << "\n\n";
	std::cout << "\n\n";


	std::cout << "DECRYPTION" << std::endl;
	AEAD aeadDec;
	aeadDec.setAeadAlgorithm(AeadAlgorithm::AEAD_AES_SIV_CMAC_256);
	aeadDec.setKey(key);
	aeadDec.setAssociatedData(assocData);
	aeadDec.setCiphertext(aeadEnc.getCiphertext());
	aeadDec.decrypt();

	std::cout << "plaintext size: " << aeadDec.getPlaintextSize() << std::endl;
	std::cout << "plaintext content:\n" << binToHexStr(aeadDec.getPlaintext(), " ", 8) << std::endl;
	std::cout << "\n\n";
	std::cout << "expected plaintext content:\n" << binToHexStr(plaintext, " ", 8) << std::endl;
	std::cout << "\n\n";
	std::cout << "plaintext equal: " << (aeadEnc.getPlaintext() == plaintext ? "yes" : "no") << std::endl;

}

