#include "RecType_EndOfMessage.h"
#include "../Logger/Logger.h"

/**
* @brief	Clears the content and sets the Record header according the rules of this type.
*/
void RecType::EndOfMessage::format(Record & record)
{
	record.clear();
	record.setCriticalBit(true);
	record.setType(RecordType::End_of_Message);
}


/**
* @brief	Checks if the record is valid and compliant with the NTS specification.
*/
bool RecType::EndOfMessage::isValid(Record const & record)
{
	bool status = true;

	if (record.getCriticalBit() == false)
	{
		LOG_ERROR("Critical Bit is not set");
		status = false;
	}

	if (record.getType() != RecordType::End_of_Message)
	{
		LOG_ERROR("wrong Record Type: '{}'", record.getTypeVal());
		status = false;
	}

	if (record.getBodyLength() != 0)
	{
		LOG_ERROR("invalid Body Length: {} bytes", record.getBodyLength());
		status = false;
	}

	return status;
}
