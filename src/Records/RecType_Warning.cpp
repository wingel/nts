#include "RecType_Warning.h"
#include "../Logger/Logger.h"

/**
* @brief	Clears the content and sets the Record header according the rules of this type.
*/
void RecType::Warning::format(Record & record)
{
	record.clear();
	record.setCriticalBit(true);
	record.setType(RecordType::Warning);
	setWarning(record, RecordWarn::Undefined);
}


/**
* @brief	Checks if the record is valid and compliant with the NTS specification.
*/
bool RecType::Warning::isValid(Record const & record)
{
	bool status = true;

	if (record.getCriticalBit() == false)
	{
		LOG_ERROR("Critical Bit is not set");
		status = false;
	}

	if (record.getType() != RecordType::Warning)
	{
		LOG_ERROR("wrong Record Type: '{}'", record.getTypeVal());
		status = false;
	}

	if (record.getBodyLength() != 2)
	{
		LOG_ERROR("invalid Body Length: {} bytes", record.getBodyLength());
		status = false;
	}

	return status;
}


unsigned short RecType::Warning::getWarningVal(Record const & record)
{
	std::vector<unsigned char> const &rawWarn = record.getBody();
	return ((rawWarn.at(0) & 0xFF) << 8) | rawWarn.at(1);
}


RecordWarn RecType::Warning::getWarning(Record const & record)
{
	return static_cast<RecordWarn>(getWarningVal(record));
}


void RecType::Warning::setWarning(Record & record, unsigned short warning)
{
	std::vector<unsigned char> entry(2);
	entry.at(0) |= (warning >> 8) & 0xFF;
	entry.at(1) |= warning & 0xFF;
	record.setBody(entry);
}


void RecType::Warning::setWarning(Record & record, RecordWarn warning)
{
	setWarning(record, static_cast<unsigned short>(warning));
}
