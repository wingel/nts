#include "RecType_NextProtNeg.h"
#include "../Logger/Logger.h"

/**
* @brief	Clears the content and sets the Record header according the rules of this type.
*/
void RecType::NextProtNeg::format(Record & record)
{
	record.clear();
	record.setCriticalBit(true);
	record.setType(RecordType::Next_Protocol_Negotiation);
}


/**
* @brief	Checks if the record is valid and compliant with the NTS specification.
*/
bool RecType::NextProtNeg::isValid(Record const & record)
{
	bool status = true;

	if (record.getCriticalBit() == false)
	{
		LOG_ERROR("Critical Bit is not set");
		status = false;
	}

	if (record.getType() != RecordType::Next_Protocol_Negotiation)
	{
		LOG_ERROR("wrong Record Type: '{}'", record.getTypeVal());
		status = false;
	}

	if ((record.getBodyLength() % c_protocolSize) != 0)
	{
		LOG_ERROR("invalid Body Length: {} bytes", record.getBodyLength());
		status = false;
	}

	return status;
}


unsigned int RecType::NextProtNeg::countProtocols(Record const & record)
{
	return record.getBodyLength() / c_protocolSize;
}


/**
* @brief	Sets the algorithm list of the Record (as a copy of the delivered list).
*/
void RecType::NextProtNeg::setProtocolList(Record & record, std::vector<unsigned short> const &protocolList)
{
	std::vector<unsigned char> ListVector;
	for (auto const& item : protocolList)
	{
		std::vector<unsigned char> entry(2);
		entry.at(0) |= (item >> 8) & 0xFF;
		entry.at(1) |= item & 0xFF;

		ListVector.insert(ListVector.end(), entry.begin(), entry.end());
	}
	record.setBody(ListVector);
}


/**
* @brief	Deletes all algorithms of the Record
*/
void RecType::NextProtNeg::clearProtocols(Record & record)
{
	record.setBody(std::vector<unsigned char>());
}


std::vector<unsigned short> RecType::NextProtNeg::getProtocolList(Record const & record)
{
	std::vector<unsigned char> const &rawList = record.getBody();
	std::vector<unsigned short> finalList;
	
	for (size_t i = 0; i < rawList.size(); i+= c_protocolSize)
	{
		if ((rawList.size() - i) < c_protocolSize) 
		{
			LOG_WARN("invalid Body Length: {} bytes", rawList.size());
			break;
		}

		unsigned short protocol = ((rawList.at(i) & 0xFF) << 8) | rawList.at(i + 1);
		finalList.push_back(protocol);
	}
	return finalList;
}


void RecType::NextProtNeg::addProtocol(Record & record, unsigned short protocol)
{
	std::vector<unsigned char> item(2);
	item.at(0) |= (protocol >> 8) & 0xFF;
	item.at(1) |= protocol & 0xFF;

	std::vector<unsigned char> ListVector = record.getBody();
	ListVector.insert(ListVector.end(), item.begin(), item.end());
	record.setBody(ListVector);
}


void RecType::NextProtNeg::addProtocol(Record & record, NextProtocol protocol)
{
	addProtocol(record, static_cast<unsigned short>(protocol));
}


/**
* @param [in]	index	Index (position) of the algorithm (starts at 1).
*/
unsigned short RecType::NextProtNeg::getProtocolVal(Record const & record, unsigned int index)
{
	std::vector<unsigned short> protList = getProtocolList(record);

	if ((index < 1) || (index > protList.size()))
	{
		LOG_ERROR("index is out of range");
		return 0;
	}
	return protList.at(index - 1);
}


/**
* @param [in]	index	Index (position) of the algorithm (starts at 1).
*/
NextProtocol RecType::NextProtNeg::getProtocol(Record const & record, unsigned int index)
{
	return static_cast<NextProtocol>(getProtocolVal(record, index));
}
