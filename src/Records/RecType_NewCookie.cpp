#include "RecType_NewCookie.h"
#include "../Logger/Logger.h"

/**
* @brief	Clears the content and sets the Record header according the rules of this type.
*/
void RecType::NewCookie::format(Record & record)
{
	record.clear();
	record.setCriticalBit(false);
	record.setType(RecordType::New_Cookie_for_NTPv4);
}

/**
* @brief	Checks if the record is valid and compliant with the NTS specification.
*/
bool RecType::NewCookie::isValid(Record const & record)
{
	bool status = true;

	if (record.getCriticalBit() == true)
	{
		LOG_WARN("Critical Bit is set (should not be set)");
	}

	if (record.getType() != RecordType::New_Cookie_for_NTPv4)
	{
		LOG_ERROR("wrong Record Type: '{}'", record.getTypeVal());
		status = false;
	}

	if (record.getBodyLength() == 0)
	{
		LOG_ERROR("invalid Body Length: 0 bytes");
		status = false;
	}

	return status;
}


unsigned short RecType::NewCookie::getCookieSize(Record const & record)
{
	return record.getBodyLength();
}


const std::vector<unsigned char> & RecType::NewCookie::getCookie(Record const & record)
{
	return record.getBody();
}


void RecType::NewCookie::setCookie(Record & record, std::vector<unsigned char> const & cookie)
{
	record.setBody(cookie);
}
