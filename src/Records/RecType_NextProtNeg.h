/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#pragma once

#include "Record.h"

namespace RecType
{
	namespace NextProtNeg
	{
		void format(Record &record);
		bool isValid(Record const &record);

		unsigned int countProtocols(Record const &record);
		void clearProtocols(Record &record);

		std::vector<unsigned short> getProtocolList(Record const &record);
		void setProtocolList(Record &record, std::vector<unsigned short> const &protocolList);

		unsigned short getProtocolVal(Record const &record, unsigned int index);
		NextProtocol getProtocol(Record const &record, unsigned int index);
		void addProtocol(Record &record, unsigned short protocol);
		void addProtocol(Record &record, NextProtocol protocol);

		namespace
		{
			const unsigned short c_protocolSize = 2; // 16-bit value
		}
	}
}
