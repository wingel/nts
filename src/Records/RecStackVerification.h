/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#pragma once
#include "RecordStack.h"

class RecStackVerification
{
public:
	RecStackVerification() = default;
	~RecStackVerification() = default;

	bool clientRequestStackIsValid(RecordStack const & stack, bool verifyItems) const;
	bool serverResponseStackIsValid(RecordStack const & stack, bool verifyItems) const;

private:

	enum class RecordStackType
	{
		ClientRequest,
		ServerResponse
	};

	bool verifyEndOfMsg(RecordStack const & stack) const;
	bool verifyNtsNextProtNeg(RecordStack const & stack) const;
	bool verifyError(RecordStack const & stack, RecordStackType stackType) const;
	bool verifyWarning(RecordStack const & stack, RecordStackType stackType) const;
	bool verifyAeadAlgoNeg(RecordStack const & stack, bool ntpIsNextProtocol) const;
	bool verifyNewCookie(RecordStack const & stack, RecordStackType stackType) const;
	bool verifyNtpv4ServerNeg(RecordStack const & stack) const;
	bool verifyNtpv4PortNeg(RecordStack const & stack) const;

	bool verifyStackItems(RecordStack const & stack) const;
	bool isNextProtocolPresent(RecordStack const & stack, NextProtocol nextProt) const;
};