/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#pragma once

#include <vector>
#include "../NtsConfig.h"

/*
   Construction of a Record:
   
    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |C|         Record Type         |          Body Length          |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                                                               |
   |                           Record Body                         |
   |                                                               |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   
   C: 'Critical Bit'
   more details: https://tools.ietf.org/pdf/draft-ietf-ntp-using-nts-for-ntp-15.pdf
*/

class Record
{
public:
	Record();
	Record(bool critBit, unsigned short type, std::vector<unsigned char> const &body);
	Record(bool critBit, RecordType type, std::vector<unsigned char> const &body);
	Record(std::vector<unsigned char> const &serializedData);
	~Record() = default;

	// Critical Bit
	bool getCriticalBit() const;
	void setCriticalBit(bool critBit);

	// Record Type
	unsigned short getTypeVal() const;
	RecordType getType() const;
	void setType(unsigned short type);
	void setType(RecordType type);

	// Body and Body Length
	const std::vector<unsigned char> & getBody() const;
	void setBody(std::vector<unsigned char> const &recordBody);
	unsigned short getBodyLength() const;

	// Misc
	std::vector<unsigned char> getSerializedData() const;
	void fromSerializedData(std::vector<unsigned char> const &serializedData);
	void clear();

private:
	const unsigned int c_recordHeaderSize = 4;

	bool m_criticalBit;
	unsigned short m_recordType;
	std::vector<unsigned char> m_recordBody;
};