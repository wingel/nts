#include "RecType_Ntpv4PortNeg.h"
#include "../Logger/Logger.h"

/**
* @brief	Clears the content and sets the Record header according the rules of this type.
*/
void RecType::Ntpv4PortNeg::format(Record & record)
{
	record.clear();
	record.setCriticalBit(false);
	record.setType(RecordType::NTPv4_Port_Negotiation);
	setPort(record, 0);
}


/**
* @brief	Checks if the record is valid and compliant with the NTS specification.
*/
bool RecType::Ntpv4PortNeg::isValid(Record const & record)
{
	bool status = true;

	if (record.getType() != RecordType::NTPv4_Port_Negotiation)
	{
		LOG_ERROR("wrong Record Type: '{}'", record.getTypeVal());
		status = false;
	}

	if (record.getBodyLength() != 2)
	{
		LOG_ERROR("invalid Body Length: {} bytes", record.getBodyLength());
		status = false;
	}

	return status;
}


unsigned short RecType::Ntpv4PortNeg::getPort(Record const & record)
{
	if (record.getBodyLength() != 2)
	{
		LOG_WARN("invalid Body Length: {} bytes", record.getBodyLength());
		return 0;
	}

	std::vector<unsigned char> const &rawPort = record.getBody();
	return ((rawPort.at(0) & 0xFF) << 8) | rawPort.at(1);
}


void RecType::Ntpv4PortNeg::setPort(Record & record, unsigned short port)
{
	std::vector<unsigned char> raw;
	raw.resize(2);
	raw.at(0) |= (port >> 8) & 0xFF;
	raw.at(1) |= port & 0xFF;
	record.setBody(raw);
}