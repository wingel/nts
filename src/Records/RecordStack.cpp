#include "RecordStack.h"
#include "../Logger/Logger.h"

RecordStack::RecordStack(std::vector<unsigned char> const & serializedData)
{
	fromSerializedData(serializedData);
}


unsigned int RecordStack::getStackSize() const
{
	return static_cast<unsigned int>(m_RecordStack.size());
}


/**
* @param [in]	index	Index of the Record in the stack (starts at 1). 
*/
const Record & RecordStack::getRecord(unsigned int index) const
{
	if ((index < 1) || (index > m_RecordStack.size()))
	{
		LOG_ERROR("index is out of range");
		return m_RecordStack.at(0);
	}
	return m_RecordStack.at(index - 1);
}


/**
* @param [in]	type		The Record Type to search for.
* @param [in]	startIndex	The first index be considered in the search (starts at 1). 
*
* @return	The index of the first match or zero if the Record Type wasn't found.
*/
unsigned int RecordStack::findRecord(RecordType type, unsigned int startIndex) const
{
	for (unsigned int i = startIndex; i <= getStackSize(); i++)
	{
		if (type == getRecord(i).getType())
		{
			return i;
		}
	}
	return 0;
}


/**
* @brief	Adds a Record as a copy at the end of the stack.
*/
void RecordStack::addRecord(Record const & record)
{
	m_RecordStack.push_back(record);
}


/**
* @brief	Deletes all Record entries in the stack.
*/
void RecordStack::clear()
{
	m_RecordStack.clear();
}


/**
* @brief	Serializes the entire stack into a binary data stream.
*/
std::vector<unsigned char> RecordStack::getSerializedData() const
{
	std::vector<unsigned char> stackStream;
	for (auto const& record : m_RecordStack)
	{
		std::vector<unsigned char> rawRecord;
		rawRecord = record.getSerializedData();
		stackStream.insert(stackStream.end(), rawRecord.begin(), rawRecord.end());
	}

	return stackStream;
}


/**
* @brief	Parses the given binary stream into a stack of Records.
*/
void RecordStack::fromSerializedData(std::vector<unsigned char> const & serializedData)
{
	std::vector<Record> recordStack;
	size_t index = 0;

	while (true)
	{
		if ((serializedData.size() - index) < c_recordHeaderSize)
		{
			LOG_ERROR("the binary data stream (raw Record stack) is incomplete or damaged");
			return;
		}
		
		// parse the next NTS Record from stream
		bool criticalBit = (serializedData.at(index) >> 7) & 1;
		unsigned short recordType = ((serializedData.at(index) & 0x7f) << 8) | serializedData.at(index + 1);
		unsigned short bodyLength = (serializedData.at(index + 2) << 8) | serializedData.at(index + 3);
		size_t payloadStart = index + 4;
		size_t payloadEnd = payloadStart + bodyLength;
		if (payloadEnd > serializedData.size())
		{
			LOG_ERROR("corrupted or incomplete Record found (size mismatch)");
			return;
		}
		std::vector<unsigned char> recordBody(serializedData.begin() + payloadStart, serializedData.begin() + payloadEnd);

		// add the extracted Record into the stack
		Record extractedRecord(criticalBit, recordType, recordBody);
		recordStack.push_back(extractedRecord);
		index += c_recordHeaderSize + bodyLength;
		
		// save all Records on success
		if ((serializedData.size() - index) == 0) // all bytes processed
		{
			m_RecordStack = std::move(recordStack);
			return;
		}
	}
}