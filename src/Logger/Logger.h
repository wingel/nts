/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#pragma once

#define SPDLOG_ACTIVE_LEVEL SPDLOG_LEVEL_TRACE


#include <spdlog/spdlog.h>





#define LOG_TRACE(...) SPDLOG_TRACE(__VA_ARGS__);
#define LOG_DEBUG(...) SPDLOG_DEBUG(__VA_ARGS__);
#define LOG_INFO(...) SPDLOG_INFO(__VA_ARGS__);
#define LOG_WARN(...) SPDLOG_WARN(__VA_ARGS__);
#define LOG_ERROR(...) SPDLOG_ERROR(__VA_ARGS__);
#define LOG_FATAL(...) SPDLOG_CRITICAL(__VA_ARGS__);
#define LOG_RAW(...) NtsLogger::getInstance().log(__VA_ARGS__);




//https://github.com/gabime/spdlog/wiki
//http://fmtlib.net/latest/syntax.html
//https://github.com/agauniyal/rang


void initLogger(); // quick and dirty

class NtsLogger
{ 
public:

	NtsLogger();
	~NtsLogger();
	static NtsLogger& getInstance();

	template<typename... Args> 
	inline void log(const char *fmt, const Args &... args)
	{
		logger->critical(fmt, args...);
	}



private:
	NtsLogger(NtsLogger const&) = delete;
	void operator=(NtsLogger const&) = delete;

	std::shared_ptr<spdlog::logger> logger;
};







/*
#include <string>
#include <sstream>
#include "BaseConfig.h"
#include "boost/log/trivial.hpp"
#include "boost/log/utility/setup.hpp"
#include <boost/log/sinks/async_frontend.hpp>

#include <boost/log/utility/manipulators/add_value.hpp>
#include "boost/date_time/posix_time/posix_time.hpp"

#include <iomanip>

namespace logging = boost::log;
namespace sinks = boost::log::sinks;
namespace expr = boost::log::expressions;
namespace keywords = boost::log::keywords;




#define LOG_LOCATION \
		<< boost::log::add_value("Line", static_cast<int>(__LINE__)) \
		<< boost::log::add_value("File", __FILENAME__) \
		<< boost::log::add_value("Func", __func__)


#define LOG_TRACE	BOOST_LOG_TRIVIAL(trace) LOG_LOCATION
#define LOG_DEBUG	BOOST_LOG_TRIVIAL(debug) LOG_LOCATION
#define LOG_INFO	BOOST_LOG_TRIVIAL(info) LOG_LOCATION
#define LOG_WARN	BOOST_LOG_TRIVIAL(warning) LOG_LOCATION
#define LOG_ERROR	BOOST_LOG_TRIVIAL(error) LOG_LOCATION
#define LOG_FATAL	BOOST_LOG_TRIVIAL(fatal) LOG_LOCATION

// forward declaration
struct NtsSettings; // TODO design überdenken

class Logger
{
public:
	void init();
	void init(NtsSettings const& settings);
	void shutdown();
	static Logger& getInstance();


private:
	Logger() = default;
	~Logger() = default;

	Logger(const Logger&) = delete;
	Logger& operator=(const Logger&) = delete;
	Logger(Logger&&) = delete;
	Logger& operator=(Logger&&) = delete;

	void initConsoleLogger();
	void initFileLogger();

	static void consoleLineFormatter(boost::log::record_view const& record, boost::log::formatting_ostream& stream);
	static void fileLineFormatter(boost::log::record_view const& record, boost::log::formatting_ostream& stream);

	static std::string getLocalTime(std::string const & format);
	static std::string severityStrFormatter(std::string const & severityStr);

	logging::trivial::severity_level getSeverityLevel(std::string const & severityLevel);

	// members
	logging::trivial::severity_level m_consoleLogLevel;
	logging::trivial::severity_level m_fileLogLevel;

	std::string m_logFileName;
	std::string m_targetDir;
	std::string m_logFileNameWithPath;

	bool m_enableLogRotate;
	int m_maxLogFiles;
	int m_maxFileSize;
	int m_minFreeSpace;
	int m_maxDirSize;
};


*/
