#include "./Logger.h"
#include <spdlog/tweakme.h>
#include "./rang/rang.hpp"
#include <spdlog/async.h>
#include "./color_console.hpp"
#include "./combined_rotating_logger.hpp"
#include <spdlog/sinks/base_sink.h>

#include "spdlog/sinks/stdout_sinks.h"

void initLogger()
{
	auto logger = spdlog::nts_console_mt<spdlog::async_factory>("nts");
	logger->set_level(spdlog::level::debug);
	logger->set_pattern("%H:%M:%S;%n;%s;%#;%!;%v");
	spdlog::set_default_logger(logger);
	logger->flush_on(spdlog::level::warn);
	spdlog::flush_every(std::chrono::seconds(1));
}





NtsLogger::NtsLogger()
{
	//	std::cout << "Logger init" << std::endl;
	try
	{
		logger = spdlog::stdout_logger_mt<spdlog::async_factory>("raw");
		logger->set_level(spdlog::level::critical);
		logger->set_pattern("%v");
		logger->flush_on(spdlog::level::critical);
	}
	catch (const spdlog::spdlog_ex& ex)
	{
		std::cout << "Log initialization failed: " << ex.what() << std::endl;
	}


	//	auto file_logger = spdlog::combined_rotating_logger_mt("file_logger", "log/log.txt", 0, 0, false, false);
	//file_logger->set_pattern("%d.%m.%Y  %H:%M:%S.%f  [%l]  [%n]  %v");
	//file_logger->flush_on(spdlog::level::trace);

	//spdlog::flush_every(std::chrono::seconds(1));
}

NtsLogger::~NtsLogger()
{
	logger->flush();
	spdlog::drop_all();
}



NtsLogger& NtsLogger::getInstance()
{
	static NtsLogger instance; // Guaranteed to be destroyed.
	return instance;
}





/*
Logger& Logger::getInstance()
{
	static Logger instance; // is thread safe
	return instance;
}

void Logger::init()
{
	logging::core::get()->remove_all_sinks(); // reset logger

	// default settings
	m_consoleLogLevel = logging::trivial::severity_level::debug;
	m_fileLogLevel = logging::trivial::severity_level::warning;

	m_logFileName = "NTS_%Y.%m.%d - %H.%M.%S.log";
	m_targetDir = "Logs";

	std::stringstream fullPath;
	fullPath << m_targetDir << "/" << m_logFileName;
	m_logFileNameWithPath = fullPath.str();

	// todo: in header verschieben
	m_enableLogRotate = true;
	m_maxLogFiles = 10;
	m_maxFileSize = 1 * 1024 * 1024; // 1 MB per file
	m_minFreeSpace = 20 * 1024 * 1024;
	m_maxDirSize = 15 * 1024 * 1024;

	// create logger
	initConsoleLogger();
	//initFileLogger(); // todo: filelogger nutzen?
}

void Logger::init(NtsSettings const & settings)
{
	logging::core::get()->remove_all_sinks(); // reset logger

	if ((settings.console_logging.enableLogToConsole == false) && (settings.file_logging.enableLogToFile == false))
	{
		boost::log::core::get()->set_logging_enabled(false);
	}
	else
	{
		boost::log::core::get()->set_logging_enabled(true);
	}

	// import settings
	m_consoleLogLevel = getSeverityLevel(settings.console_logging.logLevel);
	m_fileLogLevel = getSeverityLevel(settings.file_logging.logLevel);
	m_logFileName = settings.file_logging.fileName;
	m_targetDir = settings.file_logging.targetLogFolder;
	m_enableLogRotate = settings.file_logging.enableLogRotate;
	m_maxLogFiles = settings.file_logging.maxLogFiles;
	m_maxFileSize = settings.file_logging.maxFileSize * 1024; // size in Bytes
	m_minFreeSpace = settings.file_logging.minFreeSpace * 1024;
	m_maxDirSize = settings.file_logging.maxFolderSize * 1024;
	std::stringstream fullPath;
	fullPath << m_targetDir << "/" << m_logFileName;
	m_logFileNameWithPath = fullPath.str();

	// create logger
	if (settings.console_logging.enableLogToConsole == true)
	{
		initConsoleLogger();
	}

	if (settings.file_logging.enableLogToFile == true)
	{
		initFileLogger();
	}
}

void Logger::shutdown()
{
	logging::core::get()->remove_all_sinks();
}

void Logger::initConsoleLogger()
{
	// construct the sink
	typedef sinks::asynchronous_sink< sinks::text_ostream_backend > text_sink;
	boost::shared_ptr< text_sink > sink = boost::make_shared< text_sink >();

	// add stream (console)
	sink->locked_backend()->add_stream(boost::shared_ptr< std::ostream >(&std::clog, boost::null_deleter()));

	// set filter and formatter
	sink->set_filter(logging::trivial::severity >= m_consoleLogLevel);
	sink->set_formatter(&Logger::consoleLineFormatter);

	// register the sink in the logging core
	logging::core::get()->add_sink(sink);
}

void Logger::initFileLogger()
{
	using textFileSink = sinks::asynchronous_sink<sinks::text_file_backend>;

	if (m_enableLogRotate == true)
	{
		// construct the sink
		boost::shared_ptr<textFileSink> sink = boost::make_shared<textFileSink>
			(
				keywords::file_name = m_logFileNameWithPath.c_str(),
				keywords::rotation_size = m_maxFileSize,
				keywords::time_based_rotation = sinks::file::rotation_at_time_point(0, 0, 0) //todo:   rotation -> ini file
				);

		sink->locked_backend()->set_file_collector
		(
			sinks::file::make_collector
			(
				keywords::target = m_targetDir.c_str(),
				keywords::max_size = m_maxDirSize,
				keywords::min_free_space = m_minFreeSpace,
				keywords::max_files = m_maxLogFiles
			)
		);

		sink->locked_backend()->scan_for_files();
		sink->locked_backend()->enable_final_rotation(true);

		// set filter and formatter
		sink->set_filter(logging::trivial::severity >= m_fileLogLevel);
		sink->set_formatter(&Logger::fileLineFormatter);
		sink->locked_backend()->auto_flush(true);

		// register the sink in the logging core
		logging::core::get()->add_sink(sink);
	}
	else
	{
		// construct the sink
		boost::shared_ptr<textFileSink> sink = boost::make_shared<textFileSink>
		(
			keywords::file_name = m_logFileNameWithPath.c_str()
		);

		// set filter and formatter
		sink->set_filter(logging::trivial::severity >= m_fileLogLevel);
		sink->set_formatter(&Logger::fileLineFormatter);
		sink->locked_backend()->auto_flush(true);

		// register the sink in the logging core
		logging::core::get()->add_sink(sink);
	}
}

void Logger::consoleLineFormatter(boost::log::record_view const & record, boost::log::formatting_ostream & stream)
{
	std::stringstream severity;
	severity << record[logging::trivial::severity];

	stream << getLocalTime("%H:%M:%S");
	stream << " [" << severityStrFormatter(severity.str()) << "] ";
	stream << "[nts]  ";
	stream << record[expr::smessage];
}

void Logger::fileLineFormatter(boost::log::record_view const & record, boost::log::formatting_ostream & stream)
{
	std::stringstream severity;
	severity << record[logging::trivial::severity];

	stream << "" << getLocalTime("%d.%m.%Y  %H:%M:%S.%f") << "  ";
	stream << "[" << severityStrFormatter(severity.str()) << "]  ";
	stream << "[" << std::setw(32) << std::left << logging::extract<std::string>("File", record) << "] ";
	stream << "[" << std::setw(30) << std::left << logging::extract<std::string>("Func", record) << "] ";
	stream << "[" << std::setw(4) << std::right << logging::extract<int>("Line", record) << "]  ";
	stream << record[expr::smessage];
}

std::string Logger::getLocalTime(std::string const & format)
{
	// http://www.boost.org/doc/libs/1_66_0/doc/html/date_time/date_time_io.html
	boost::posix_time::time_facet* facet = new boost::posix_time::time_facet(format.c_str()); // info: no 'delete' needed
	std::stringstream timeDateStream;
	timeDateStream.imbue(std::locale(timeDateStream.getloc(), facet));
	timeDateStream << boost::posix_time::microsec_clock::universal_time();
	return timeDateStream.str();
}

std::string Logger::severityStrFormatter(std::string const & severityStr)
{
	if (severityStr == "trace")   return "trace";
	if (severityStr == "debug")   return "debug";
	if (severityStr == "info")    return "info ";
	if (severityStr == "warning") return "warn ";
	if (severityStr == "error")   return "error";
	if (severityStr == "fatal")   return "fatal";
	return "unknown";
}

logging::trivial::severity_level Logger::getSeverityLevel(std::string const & severityLevel)
{
	std::string loglevel = severityLevel;
	std::transform(loglevel.begin(), loglevel.end(), loglevel.begin(), ::tolower);

	if (loglevel == "trace")   return logging::trivial::severity_level::trace;
	if (loglevel == "debug")   return logging::trivial::severity_level::debug;
	if (loglevel == "info")    return logging::trivial::severity_level::info;
	if (loglevel == "warning") return logging::trivial::severity_level::warning;
	if (loglevel == "error")   return logging::trivial::severity_level::error;
	if (loglevel == "fatal")   return logging::trivial::severity_level::fatal;

	LOG_WARN("loglevel ungueltig";
	return logging::trivial::severity_level::info;
}

*/