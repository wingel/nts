/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#pragma once

#define WIN32_LEAN_AND_MEAN // damit Boost.ASIO zusammen mit "windows.h" funktioniert      socket problem

#ifdef _WIN32
#include "windows.h"
#endif


#include <string>
#include <vector>



void SetWindow(int Width, int Height, int WidthBuffer, int HeightBuffer);
std::string binToHexStr(unsigned char const *buf, int num, std::string pairDelimiter, int newLineNum, bool uppercase = true);
std::string binToHexStr(std::vector<unsigned char> const &data, std::string pairDelimiter, int newLineNum, bool uppercase = true);
void parseString(const std::string &string, std::vector<std::string> &tokens, const std::string &delimiter);

