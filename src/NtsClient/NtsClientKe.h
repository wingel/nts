/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#pragma once
#include <vector>
#include "../NtsConfig.h"
#include <mutex>
#include <openssl/ssl.h>
#include "../Records/RecordStack.h"



class NtsClientKe
{
public:
	NtsClientKe();
	~NtsClientKe() = default;

	std::vector<unsigned char> generateTlsRequest();
	bool processTlsResponse(std::vector<unsigned char> const& tlsAppDataPayload, SSL &ssl);


	bool isTlsKeSuccessful() const;

	const std::vector<NextProtocol> &getNegotiatedNextProtocols() const;
	const AeadAlgorithm &getNegotiatedAeadAlgo() const;
	const std::vector<std::vector<unsigned char>> &getCookieStack() const;
	const std::vector<unsigned char> &getC2SKey() const;
	const std::vector<unsigned char> &getS2CKey() const;
	void clearCookies();
	void clearAll(); // au�er supported Algos


private:



	// process warning / errors
	bool isWarningRecPresent(RecordStack const & recStack) const;
	bool isErrorRecPresent(RecordStack const & recStack) const;

	// process Next Protocol
	std::vector<NextProtocol> getNegotiatedNextProtList(RecordStack const & recStack) const;
	bool checkNegotiatedNextProtList(std::vector<NextProtocol> const &negProtList) const; // ret true, wenn NTS konform

	// process AEAD Algo
	bool checkNegotiatedAeadAlgo(RecordStack const & recStack) const; // ret true, wenn NTS konform
	AeadAlgorithm extractNegAeadAlgo(RecordStack const & recStack) const;

	// process cookies
	std::vector<std::vector<unsigned char>> extractCookies(RecordStack const & recStack) const;


	bool isSupported(NextProtocol nextProt) const;
	bool isSupported(AeadAlgorithm AeadAlgo) const;

	std::vector<unsigned char> extractTLSKey(SSL &ssl, std::string tlsExporterLabel, NextProtocol nextProt, AeadAlgorithm aeadAlgo, unsigned short mode) const;

	static std::mutex m_mutex;



	std::vector<NextProtocol> m_supportedNextProt;
	std::vector<AeadAlgorithm> m_supportedAeadAlgos;

	std::vector<NextProtocol> m_negNextProt;
	AeadAlgorithm m_negotiatedAeadAlgo;
	std::vector<std::vector<unsigned char>> m_cookieStack;
	std::vector<unsigned char> m_C2SKey;
	std::vector<unsigned char> m_S2CKey;

	bool m_ntsKeSuccessful;
};

