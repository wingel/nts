/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#pragma once
 // TODO: Review


#include <assert.h>
#include <iostream>
#include <vector>
#include "../Logger/Logger.h"
#include "../generalFunctions.h"
#include "../cryptoFunctions.h"
#include "../NtsConfig.h"
#include "../NtpExtensionFields/NtpExtensionField.h"
#include "../NtpExtensionFields/ExtF_UniqueIdentifier.h"
#include "../NtpExtensionFields/ExtF_NtsAuthAndEncEF.h"
#include "../NtpExtensionFields/ExtF_NtsCookie.h"
#include "../NtpExtensionFields/ExtF_NtsCookiePlaceholder.h"
#include <queue>
#include <chrono>
#include "NtpNtsInterface.h"
#include "../NtpExtensionFields/NtpExtFieldStack.h"
#include "../NtpExtensionFields/NtpExtFieldStackVerification.h"

class NtsClientNtp
{
public:
	NtsClientNtp() = default;
	~NtsClientNtp() = default;

	void generateNtpRequest(NtpNtsInterface &newNtpRequest);
	void processNtpResponse(NtpNtsInterface &receivedNtpResponse);


	void addCookie(std::vector<unsigned char> cookie);
	void setAeadAlgorithm(AeadAlgorithm aeadAlgo);
	void setC2SKey(std::vector<unsigned char> C2SKey);
	void setS2CKey(std::vector<unsigned char> S2CKey);
	void setPlaceholderSize(unsigned short size);

	bool verifyNtpMessage(NtpNtsInterface & receivedNtpRequest, NtpExtensionField const &aeadExtField); // als separate funktion?

	size_t getCookieStackSize() const;


private:

	std::vector<unsigned char> m_uniqueIdentifier;
	unsigned short m_placeholderSize;

	std::queue<std::vector<unsigned char>> m_cookieStack;
	AeadAlgorithm m_aeadAlgo;
	std::vector<unsigned char> m_C2SKey;
	std::vector<unsigned char> m_S2CKey;

	std::vector<unsigned char> m_decryptedExtFields;
};

