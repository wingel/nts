#include "NtsClientSession.h"


NtsClientSession::NtsClientSession(NtsSettings const& settings, std::string const& keServerIp)
	:
	m_settings(settings),
	m_keServerIp(keServerIp)
{
}


void NtsClientSession::generateNtpRequest(NtpNtsInterface &newNpRequest)
{
	LOG_TRACE("NtsClientSession: genNtpReq: Session ID (time server): {}", newNpRequest.getIpAddress());
	LOG_TRACE("NtsClientSession: genNtpReq: cookies available (amount): {}", m_ntpProtocol.getCookieStackSize());

	if (m_ntpProtocol.getCookieStackSize() == 0)
	{
		m_ntsKeSuccessful = false;
	}

	if (m_ntsKeSuccessful == false)
	{
		LOG_TRACE("NtsClientSession: genNtpReq: starte NTS KE (TLS)");
		startTlsKe(newNpRequest.getIpAddress(), TLS_PORT);
		processTlsResponsePostProcess();
	}
	
	if (m_ntsKeSuccessful == false)
	{
		LOG_ERROR("NtsClientSession::generateNtpRequest(): TLS KE nicht erfolgt bzw. fehlgeschlagen");
		return;
	}
	m_ntpProtocol.generateNtpRequest(newNpRequest);
}

void NtsClientSession::processNtpResponse(NtpNtsInterface & receivedNtpResponse)
{
	LOG_TRACE("NtsClientSession: procNtpRes: Session ID (time server): {}", receivedNtpResponse.getIpAddress());
	LOG_TRACE("NtsClientSession: procNtpRes: cookies available (amount): {}", m_ntpProtocol.getCookieStackSize());

	if (m_ntsKeSuccessful == false)
	{
		LOG_ERROR("NtsClientSession::processNtpResponse(): TLS KE nicht erfolgt bzw. fehlgeschlagen");
		return;
	}

	m_ntpProtocol.processNtpResponse(receivedNtpResponse);
}

std::string const & NtsClientSession::getKeServerIp() const
{
	return m_keServerIp;
}


void NtsClientSession::startTlsKe(std::string const& serverIp, int port)
{
	if (serverIp == "")
	{
		LOG_ERROR("NtsClientSession::startTlsKe(): serverIP is empty");
	}

	try
	{
		boost::asio::io_service io_service;
		boost::asio::ip::tcp::resolver resolver(io_service);
		boost::asio::ip::tcp::resolver::iterator iterator = resolver.resolve(serverIp, std::to_string(port));
		boost::asio::ssl::context ctx(boost::asio::ssl::context::tls_client);

		ctx.load_verify_file(CERT_ROOT_CA); // TODO: read from .ini

		ctx.set_options(boost::asio::ssl::context::default_workarounds
							  | boost::asio::ssl::context::no_sslv2
							  | boost::asio::ssl::context::no_sslv3
							  | boost::asio::ssl::context::no_tlsv1
							  | boost::asio::ssl::context::no_tlsv1_1
							//  | boost::asio::ssl::context::no_tlsv1_2
							  | boost::asio::ssl::context::single_dh_use);

		TlsClient client(io_service, ctx, iterator, m_ntsTlsKe);
		io_service.run();
	}
	catch (std::exception &err)
	{
		LOG_FATAL("NtsClientSession::startTlsKe(): Exception: Can not resolve server address ({}:{}) --> MSG: {}", serverIp, port, err.what());
	}
	return;
}


void NtsClientSession::processTlsResponsePostProcess()
{
	if (m_ntsTlsKe.isTlsKeSuccessful() == false)
	{
		LOG_ERROR("NtsClientSession::processTlsResponse: TLS KE failed");
		m_ntsTlsKe.clearAll();
		m_ntsKeSuccessful = false;
		return;
	}

	// Next Protocol: search for "NTPv4"
	const std::vector<NextProtocol> &negNextProt = m_ntsTlsKe.getNegotiatedNextProtocols();
	bool foundProt = false;
	for (auto prot : negNextProt)
	{
		if (prot == NextProtocol::NTPv4)
		{
			foundProt = true;
			break;
		}
	}
	if (foundProt == false)
	{
		LOG_ERROR("NtsClientSession::processTlsResponse: Next Protocol 'NTPv4' not found");
		m_ntsTlsKe.clearAll();
		m_ntsKeSuccessful = false;
		return;
	}

	// Protokoll gefunden --> Daten extrahieren (TLS keys, Cookies, algos)
	m_ntpProtocol.setAeadAlgorithm(m_ntsTlsKe.getNegotiatedAeadAlgo());
	m_ntpProtocol.setC2SKey(m_ntsTlsKe.getC2SKey());
	m_ntpProtocol.setS2CKey(m_ntsTlsKe.getS2CKey());
	if (m_ntsTlsKe.getCookieStack().size() != 0)
	{
		m_ntpProtocol.setPlaceholderSize(static_cast<unsigned short>(m_ntsTlsKe.getCookieStack().at(0).size()));
	}

	const std::vector<std::vector<unsigned char>> &cookieStack = m_ntsTlsKe.getCookieStack();
	for (auto cookie : cookieStack)
	{
		m_ntpProtocol.addCookie(cookie);
	}
	m_ntsTlsKe.clearCookies();
	m_ntsKeSuccessful = true;
}



