#include "NtsClient.h"
#include "../Logger/Logger.h"


NtsClient::NtsClient(NtsSettings const& settings)
	:
	m_settings(settings)
{
}

void NtsClient::generateNtpRequest(NtpNtsInterface & newNtpRequest)
{
	std::string const & keServerIP = newNtpRequest.getIpAddress();
	if (keServerIP == "")
	{
		LOG_ERROR("serverIP is empty");
		return;
	}
	LOG_TRACE("IP address (KE server): {}", keServerIP);
	NtsClientSession & clientSession = getSession(keServerIP);
	clientSession.generateNtpRequest(newNtpRequest);
}

void NtsClient::processNtpResponse(NtpNtsInterface & receivedNtpResponse)
{
	std::string const & keServerIP = receivedNtpResponse.getIpAddress();
	if (keServerIP == "")
	{
		LOG_ERROR("NtsClient::receivedNtpResponse(): keServerIP is empty");
		return;
	}
	LOG_TRACE("NtsClient: IP address (KE server): {}", keServerIP);
	NtsClientSession & clientSession = getSession(keServerIP);
	clientSession.processNtpResponse(receivedNtpResponse);
}


NtsClientSession & NtsClient::getSession(std::string const & keServerIp)
{
	// search for existing sessions
	for (auto& it : m_clientSessionList)
	{
		if (it.getKeServerIp() == keServerIp)
		{
			LOG_TRACE("client session found");
			return it;
		}
	}
	
	// session not found; create a new one
	LOG_TRACE("client session not found; create a new one");
	m_clientSessionList.emplace_back(NtsClientSession(m_settings, keServerIp));
	if (m_clientSessionList.empty() == false)
	{
		return m_clientSessionList.back();
	}
	else
	{
		LOG_FATAL("can not create a new session");
		throw std::runtime_error("can not create a new session");
	}
}
