#include "NtsClientKe.h"
#include <sstream>
#include "../Logger/Logger.h"
#include "../Records/RecType_NextProtNeg.h"
#include "../Records/RecType_AeadAlgoNeg.h"
#include "../Records/RecType_EndOfMessage.h"
#include "../Records/RecStackVerification.h"
#include "../cryptoFunctions.h"
#include "../generalFunctions.h"


/*
	#include "../Records/Record.h"
	#include "../Records/RecType_Error.h"
	#include "../Records/RecType_Warning.h"
	#include "../Records/RecType_NewCookie.h"
*/



std::mutex NtsClientKe::m_mutex;


NtsClientKe::NtsClientKe()
{
	m_supportedNextProt.push_back(NextProtocol::NTPv4);

	// todo: get from ini file
	m_supportedAeadAlgos.push_back(AeadAlgorithm::AEAD_AES_SIV_CMAC_256);
	m_supportedAeadAlgos.push_back(AeadAlgorithm::AEAD_AES_SIV_CMAC_384);
	m_supportedAeadAlgos.push_back(AeadAlgorithm::AEAD_AES_SIV_CMAC_512);

	m_negotiatedAeadAlgo = AeadAlgorithm::NOT_DEFINED;
	m_ntsKeSuccessful = false;
}




std::vector<unsigned char> NtsClientKe::generateTlsRequest()
{
	std::lock_guard<std::mutex> guard(m_mutex);

	m_ntsKeSuccessful=false;

	// create Record: NTS Next Protocol Negotiation
	Record NtsNextProtNegRec;
	RecType::NextProtNeg::format(NtsNextProtNegRec);
	for (auto nextProtocol : m_supportedNextProt)
	{
		RecType::NextProtNeg::addProtocol(NtsNextProtNegRec, nextProtocol);
	}
	

	// create Record: AEAD Algorithm Negotiation
	Record AeadAlgoNegRec;
	RecType::AeadAlgoNeg::format(AeadAlgoNegRec);
	for (auto aeadAlgo : m_supportedAeadAlgos)
	{
		RecType::AeadAlgoNeg::addAlgorithm(AeadAlgoNegRec, aeadAlgo);
	}


	// create Record: End of Message
	Record EndOfMsgRec;
	RecType::EndOfMessage::format(EndOfMsgRec);


	// generate TLS ApplicationData payload
	RecordStack recStack;
	recStack.addRecord(NtsNextProtNegRec);
	recStack.addRecord(AeadAlgoNegRec);
	recStack.addRecord(EndOfMsgRec);
	std::vector<unsigned char> TlsAppDataPayload = recStack.getSerializedData();

	return TlsAppDataPayload;
}




bool NtsClientKe::processTlsResponse(std::vector<unsigned char> const & tlsAppDataPayload, SSL &ssl)
{

	std::lock_guard<std::mutex> guard(m_mutex);

	m_ntsKeSuccessful = false;

	// seperate records from stream
	RecordStack recStack(tlsAppDataPayload);


	// check record stack (is it NTS conform?)
	if (RecStackVerification().serverResponseStackIsValid(recStack, true) == false)
	{
		LOG_ERROR("NtsClientKe: invalid TLS data");
	}


	//  fehler vorhanden?
	if (isErrorRecPresent(recStack) == true)
	{
		LOG_ERROR("NtsClientKe: Error records found");
	}


	//  warnungen vorhanden?
	if (isWarningRecPresent(recStack) == true)
	{
		LOG_ERROR("NtsClientKe: Warning records found");
	}


	// Next Protocols extrahieren und pr�fen
	std::vector<NextProtocol> negotiatedProtocols = getNegotiatedNextProtList(recStack);
	if (checkNegotiatedNextProtList(negotiatedProtocols) == false)
	{
		LOG_ERROR("NtsClientKe: Next_Protocol_Negotiation ung�ltig");
	}
	else
	{
		m_negNextProt = negotiatedProtocols;

		// Debug output
		std::stringstream ss;
		for (auto iter : negotiatedProtocols)
		{
			ss << getNextProtocolStr(iter) << "  ";
		}
		LOG_TRACE("NtsClientKe: supported Next Protocols: {}", ss.str());
	}

	
	// AEAD-Algo pr�fen und extrahieren
	if (checkNegotiatedAeadAlgo(recStack) == false)
	{
		LOG_ERROR("NtsClientKe: AEAD negotiation failed");
	}
	else
	{
		m_negotiatedAeadAlgo = extractNegAeadAlgo(recStack);
		LOG_TRACE("NtsClientKe: selected AEAD algorithm: {}", getAeadAlgorithmStr(m_negotiatedAeadAlgo));
	}
	
	

	// cookies extrahieren
	std::vector<std::vector<unsigned char>> cookieList = extractCookies(recStack);
	if (cookieList.size() == 0)
	{
		LOG_ERROR("NtsClientKe: keine cookies vorhanden");
	}
	else
	{
		for (auto cookie : cookieList)
		{
			m_cookieStack.push_back(cookie);
		}

		// FOR TRACE: list cookies
		LOG_TRACE("NtsClientKe: received cookies: {}", m_cookieStack.size());
		std::stringstream CookieStackStr;
		for (auto cookie : m_cookieStack)
		{
			CookieStackStr << getItemID(cookie) << "  ";
		}
		LOG_TRACE("NtsClientKe: received cookies: {}", CookieStackStr.str());
	}


	// TLS-Schl�ssel exportieren
	m_C2SKey = extractTLSKey(ssl, TLS_Exporter_Label, NextProtocol::NTPv4, m_negotiatedAeadAlgo, TLS_C2S_KEY_ID);
	m_S2CKey = extractTLSKey(ssl, TLS_Exporter_Label, NextProtocol::NTPv4, m_negotiatedAeadAlgo, TLS_S2C_KEY_ID);

	LOG_TRACE("NtsClientKe: C2SKey: {}", binToHexStr(m_C2SKey, " ", 64, true));
	LOG_TRACE("NtsClientKe: S2CKey: {}", binToHexStr(m_S2CKey, " ", 64, true));
	

	m_ntsKeSuccessful = true;

	// TLS KE beendet (tls closed?)
	return true; // KE war erfolgreich
}








bool NtsClientKe::isTlsKeSuccessful() const
{
	std::lock_guard<std::mutex> guard(m_mutex);
	return m_ntsKeSuccessful;
}

const std::vector<NextProtocol>& NtsClientKe::getNegotiatedNextProtocols() const
{
	return m_negNextProt;
}

const AeadAlgorithm & NtsClientKe::getNegotiatedAeadAlgo() const
{
	return m_negotiatedAeadAlgo;
}

const std::vector<std::vector<unsigned char>>& NtsClientKe::getCookieStack() const
{
	return m_cookieStack;
}

const std::vector<unsigned char>& NtsClientKe::getC2SKey() const
{
	std::lock_guard<std::mutex> guard(m_mutex);
	return m_C2SKey;
}

const std::vector<unsigned char>& NtsClientKe::getS2CKey() const
{
	std::lock_guard<std::mutex> guard(m_mutex);
	return m_S2CKey;
}

void NtsClientKe::clearCookies()
{
	m_cookieStack.clear();
}

void NtsClientKe::clearAll()
{
	m_negNextProt.clear();
	m_negotiatedAeadAlgo = AeadAlgorithm::NOT_DEFINED;
	m_cookieStack.clear();
	m_C2SKey.clear();
	m_S2CKey.clear();
}

bool NtsClientKe::isWarningRecPresent(RecordStack const & recStack) const
{
	if (recStack.findRecord(RecordType::Warning) != 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool NtsClientKe::isErrorRecPresent(RecordStack const & recStack) const
{
	if (recStack.findRecord(RecordType::Error) != 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}



// ==============================================================================================================================



bool NtsClientKe::checkNegotiatedNextProtList(std::vector<NextProtocol> const &negProtList) const
{
	if (negProtList.size() == 0)
	{
		LOG_ERROR("NtsClientKe: 'Next_Protocol_Negotiation': No supported protocols found (list empty)");
		return false;
	}
	else
	{
		for (auto protocol : negProtList)
		{
			if (isSupported(protocol) == false)
			{
				LOG_ERROR("NtsClientKe: 'Next_Protocol_Negotiation' contains unsupported protocols");
				return false;
			}
		}
	}

	return true;
}

bool NtsClientKe::checkNegotiatedAeadAlgo(RecordStack const & recStack) const
{
	// search record
	unsigned index = recStack.findRecord(RecordType::AEAD_Algorithm_Negotiation);
	if (index == 0)
	{
		LOG_ERROR("NtsClientKe: record not found: 'AEAD_Algorithm_Negotiation'");
		return false;
	}

	// extract and verify aead algo
	const Record &rec = recStack.getRecord(index);
	unsigned int numAeadAlgos = RecType::AeadAlgoNeg::countAlgorithms(rec);
	if (numAeadAlgos == 0)
	{
		LOG_ERROR("NtsClientKe: 'AEAD_Algorithm_Negotiation': no AEAD algos found");
		return false;
	}
	else if (numAeadAlgos > 1)
	{
		LOG_ERROR("NtsClientKe: 'AEAD_Algorithm_Negotiation': wrong numbers of AEAD algos (>1)");
		return false;
	}
	else
	{
		if (isSupported(RecType::AeadAlgoNeg::getAlgorithm(rec, 1)) == false)
		{
			LOG_ERROR("NtsClientKe: 'AEAD_Algorithm_Negotiation': AEAD algo not supported");
			return false;
		}
		else
		{
			return true;
		}
	}
}

AeadAlgorithm NtsClientKe::extractNegAeadAlgo(RecordStack const & recStack) const
{
	// search record
	unsigned index = recStack.findRecord(RecordType::AEAD_Algorithm_Negotiation);
	if (index == 0)
	{
		LOG_ERROR("NtsClientKe: record not found: 'AEAD_Algorithm_Negotiation'");
		return AeadAlgorithm::NOT_DEFINED;
	}
	
	const Record &rec = recStack.getRecord(index);
	return RecType::AeadAlgoNeg::getAlgorithm(rec, 1);
}



std::vector<std::vector<unsigned char>> NtsClientKe::extractCookies(RecordStack const & recStack) const
{
	std::vector<std::vector<unsigned char>> cookieList;
	unsigned int stackSize = recStack.getStackSize();

	for (unsigned int i = 1; i <= stackSize; i++)
	{
		const Record &rec = recStack.getRecord(i);
		if (rec.getType() == RecordType::New_Cookie_for_NTPv4)
		{
			cookieList.push_back(rec.getBody());
		}
	}
	return cookieList;
}



std::vector<NextProtocol> NtsClientKe::getNegotiatedNextProtList(RecordStack const & recStack) const
{
	std::vector<NextProtocol> list;

	// search record
	unsigned index = recStack.findRecord(RecordType::Next_Protocol_Negotiation);
	if (index == 0)
	{
		LOG_ERROR("NtsClientKe: record not found: 'Next_Protocol_Negotiation'");
		return list;
	}

	// extract list
	const Record &rec = recStack.getRecord(index);
	std::vector<unsigned short> nextProtNegList = RecType::NextProtNeg::getProtocolList(rec);
	for (auto prot : nextProtNegList)
	{
		list.push_back(static_cast<NextProtocol>(prot));
	}
	return list;
}



bool NtsClientKe::isSupported(NextProtocol nextProt) const
{
	for (auto prot : m_supportedNextProt)
	{
		if (prot == nextProt)
		{
			return true;
		}
	}
	return false;
}

bool NtsClientKe::isSupported(AeadAlgorithm AeadAlgo) const
{
	for (auto algo : m_supportedAeadAlgos)
	{
		if (algo == AeadAlgo)
		{
			return true;
		}
	}
	return false;
}

std::vector<unsigned char> NtsClientKe::extractTLSKey(SSL & ssl, std::string tlsExporterLabel, NextProtocol nextProt, AeadAlgorithm aeadAlgo, unsigned short mode) const
{
	unsigned short nextProtocol = static_cast<unsigned short>(nextProt);
	unsigned short aeadAlgorithm = static_cast<unsigned short>(aeadAlgo);
	unsigned int tlsKeySize = determineKeySize(aeadAlgo);
	if (tlsKeySize == 0)
	{
		LOG_ERROR("NtsServerKe::extractTLSKey(): invalid key size");
	}

	std::vector<unsigned char> context(5); // 5 bytes

										   // set NextProtocol
	context.at(0) = (nextProtocol >> 8) & 0xFF;
	context.at(1) = nextProtocol & 0xFF;

	// set AeadAlgorithm
	context.at(2) = (aeadAlgorithm >> 8) & 0xFF;
	context.at(3) = aeadAlgorithm & 0xFF;

	// set Key mode
	context.at(4) = mode & 0xFF;

	// export key
	std::vector<unsigned char> tlsKey(tlsKeySize);
	int useContext = 1;
	int ret = SSL_export_keying_material(&ssl, tlsKey.data(), tlsKeySize, tlsExporterLabel.c_str(), tlsExporterLabel.length(), context.data(), context.size(), useContext); // RFC 5705

	if (ret != 1)
	{
		LOG_ERROR("NtsServerKe::extractTLSKey(): SSL_export_keying_material failed");
	}

	return tlsKey;
}
