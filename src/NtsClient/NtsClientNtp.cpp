#include "NtsClientNtp.h"
#include "../AEAD.h"
#include <sstream>

void NtsClientNtp::generateNtpRequest(NtpNtsInterface &newNtpRequest)
{
	LOG_TRACE("");
	LOG_TRACE("Client: genNtpReq: STARTED");
	LOG_TRACE("Client: genNtpReq: number of NTP EFs: {}", newNtpRequest.getExtensionFieldCount());


	// create EF: Unique Identifier
	NtpExtensionField uniqIdentEF;
	ExtField::UniqueIdentifier::format(uniqIdentEF);
	m_uniqueIdentifier = getRandomNumber(UNIQUE_IDENTIFIER_SIZE);
	ExtField::UniqueIdentifier::setUniqueId(uniqIdentEF, m_uniqueIdentifier);
	LOG_TRACE("Client: genNtpReq: uniqueID extension created (ID: {}, size: {})",
		getItemID(ExtField::UniqueIdentifier::getUniqueId(uniqIdentEF)),
		ExtField::UniqueIdentifier::getUniqueIdSize(uniqIdentEF));


	// FOR TRACE: list cookies
	std::queue<std::vector<unsigned char>> tempCookieStack(m_cookieStack);
	LOG_TRACE("Client: genNtpReq: amount of cookies: {}", tempCookieStack.size());
	std::stringstream CookieStackStr;
	while (tempCookieStack.empty() == false)
	{
		CookieStackStr << getItemID(tempCookieStack.front()) << "  ";
		tempCookieStack.pop();
	}
	LOG_TRACE("Client: genNtpReq: cookie stack: {}", CookieStackStr.str());


	// create EF: NTS Cookie
	NtpExtensionField ntsCookie;
	ExtField::NtsCookie::format(ntsCookie);
	if (m_cookieStack.size() == 0)
	{
		// TODO: neuer TLS handshake
		LOG_ERROR("NtsClientNtp::generateNtpRequest(): out of cookies");
		return;
	}
	ExtField::NtsCookie::setCookie(ntsCookie, m_cookieStack.front());
	m_cookieStack.pop();
	LOG_TRACE("Client: genNtpReq: ntsCookie extension created (cookie ID: {}, size: {})",
		getItemID(ExtField::NtsCookie::getCookie(ntsCookie)),
		ExtField::NtsCookie::getCookieSize(ntsCookie));


	// create EF: NTS Cookieplaceholder (on demand)
	std::vector<NtpExtensionField> placeholderStack;
	size_t countPlaceholder = MAX_COOKIES - m_cookieStack.size() - 1;
	if (countPlaceholder < 0)
	{
		// TODO: assert?
		assert("num placeholder < 0");
	}
	else if (countPlaceholder > 0)
	{
		for (size_t i = 1; i <= countPlaceholder; i++)
		{
			NtpExtensionField cookiePlaceholder;
			ExtField::NtsCookiePlaceholder::format(cookiePlaceholder);
			ExtField::NtsCookiePlaceholder::setPlaceholderSize(cookiePlaceholder, m_placeholderSize);
			placeholderStack.push_back(cookiePlaceholder);
		}
		LOG_TRACE("Client: genNtpReq: cookiePlaceholder extension(s) created (amount: {}, size per placeholder: {})",
			placeholderStack.size(), 
			m_placeholderSize);
	}



	// Erweiterungsfelder in NTP einbetten
	newNtpRequest.addExtensionField(uniqIdentEF.getFieldTypeVal(), uniqIdentEF.getValue());
	LOG_TRACE("Client: genNtpReq: NTP EF (uniqueID) added (type: {}, value size: {}, value ID: {}, raw EF size: {})",
		uniqIdentEF.getFieldTypeVal(),
		uniqIdentEF.getValueLength(),
		getItemID(uniqIdentEF.getValue()),
		uniqIdentEF.getSerializedData().size());

	newNtpRequest.addExtensionField(ntsCookie.getFieldTypeVal(), ntsCookie.getValue());
	LOG_TRACE("Client: genNtpReq: NTP EF (ntsCookie) added (type: {}, value size: {}, value ID: {}, raw EF size: {})",
		ntsCookie.getFieldTypeVal(),
		ntsCookie.getValueLength(),
		getItemID(ntsCookie.getValue()),
		ntsCookie.getSerializedData().size());

	for (auto placeholder : placeholderStack)
	{
		newNtpRequest.addExtensionField(placeholder.getFieldTypeVal(), placeholder.getValue());
		LOG_TRACE("Client: genNtpReq: NTP EF (placeholder) added (type: {}, value size: {}, value ID: {}, raw EF size: {})",
			placeholder.getFieldTypeVal(),
			placeholder.getValueLength(),
			getItemID(placeholder.getValue()),
			placeholder.getSerializedData().size());
	}



	// create EF: NTS Authenticator and Encrypted Extension Fields
	NtpExtensionField AuthAndEncExF;
	ExtField::NtsAuthAndEncEF::format(AuthAndEncExF);
	std::vector<unsigned char> nonce = getRandomNumber(NONCE_SIZE);
	std::vector<unsigned char> associatedData;


	// NTP Zeitstempel aktualisieren
	newNtpRequest.setSendResponseMessage(true);
	newNtpRequest.updateTransmitTimestamp();


	// ==================================== TIME CRIT ====================================
	auto start = std::chrono::steady_clock::now();
	// -----------------------------------------------------------------------------------

	associatedData = newNtpRequest.getSerializedPackage();
	ExtField::NtsAuthAndEncEF::performAead(AuthAndEncExF, m_aeadAlgo, m_C2SKey, nonce, associatedData);

	// AuthAndEncExF in NTP einbetten
	newNtpRequest.addExtensionField(AuthAndEncExF.getFieldTypeVal(), AuthAndEncExF.getValue());

	// -----------------------------------------------------------------------------------
	auto end = std::chrono::steady_clock::now();
	auto time = end - start;
	std::chrono::duration<double, std::micro> proccess_time_us = time;
	std::cout.precision(10);
	LOG_DEBUG("Client: genNtpReq: TimeRequest crit time: {} us", proccess_time_us.count());
	// ==================================== TIME CRIT ====================================

	LOG_TRACE("Client: genNtpReq: perform AEAD (serialized NTP ID: {}, serialized NTP size: {})",
		getItemID(associatedData),
		associatedData.size());

	LOG_TRACE("Client: genNtpReq: NTP EF (AEAD ExtF) added (type: {}, value size: {}, raw EF size: {})",
		AuthAndEncExF.getFieldTypeVal(),
		AuthAndEncExF.getValueLength(),
		AuthAndEncExF.getSerializedData().size());

	
	// ADVANCED TRACE OUT
	int numEFs = newNtpRequest.getExtensionFieldCount();
	std::vector<unsigned char> rawNtpPacket = newNtpRequest.getSerializedPackage();
	LOG_TRACE("Client: genNtpReq: NTP Packet --> packet size: {}, packet ID: {}, num EFs: {}",
		rawNtpPacket.size(),
		getItemID(rawNtpPacket),
		numEFs);

	for (int i = 0; i < numEFs; i++)
	{
		LOG_TRACE("Client: genNtpReq: NTP EF --> index: {}, type: {}, value size: {}, value (ID): {}",
			i,
			newNtpRequest.getExtensionFieldType(i),
			newNtpRequest.getExtensionFieldValue(i).size(),
			getItemID(newNtpRequest.getExtensionFieldValue(i)));
	}
	LOG_TRACE("Client: genNtpReq: FINISHED");
	LOG_TRACE("");
	// ADVANCED TRACE OUT


	// TODO: on error:
	//newNtpRequest.setSendResponseMessage(false);
	return;
}








void NtsClientNtp::processNtpResponse(NtpNtsInterface & receivedNtpResponse)
{
	LOG_TRACE("");
	LOG_TRACE("Client: procNtpRes: STARTED");
	LOG_TRACE("Client: procNtpRes: number of NTP EFs: {}", receivedNtpResponse.getExtensionFieldCount());


	// ADVANCED TRACE OUT
	int numEFs = receivedNtpResponse.getExtensionFieldCount();
	std::vector<unsigned char> rawNtpPacket = receivedNtpResponse.getSerializedPackage();
	LOG_TRACE("Client: procNtpRes: NTP Packet --> packet size: {}, packet ID: {}, num EFs: {}",
		rawNtpPacket.size(),
		getItemID(rawNtpPacket),
		numEFs);

	for (int i = 0; i < numEFs; i++)
	{
		LOG_TRACE("Client: procNtpRes: NTP EF --> index: {}, type: {}, value size: {}, value (ID): {}",
			i,
			receivedNtpResponse.getExtensionFieldType(i),
			receivedNtpResponse.getExtensionFieldValue(i).size(),
			getItemID(receivedNtpResponse.getExtensionFieldValue(i)));
	}
	// ADVANCED TRACE OUT


	// Erweiterungsfelder extrahieren    
	int numReceivedExtFields = receivedNtpResponse.getExtensionFieldCount();
	NtpExtFieldStack receivedExtFieldStack;
	if (numReceivedExtFields == 0)
	{
		LOG_ERROR("NtsClientNtp::processNtpResponse(): empfangenes NTP beinhaltet keine EFs");
		return;
	}
	for (int index = 0; index <= numReceivedExtFields - 1; index++)
	{
		NtpExtensionField ntpExtF;
		ntpExtF.setFieldType(receivedNtpResponse.getExtensionFieldType(index));
		ntpExtF.setValue(receivedNtpResponse.getExtensionFieldValue(index));
		receivedExtFieldStack.addExtField(ntpExtF); 
	}


	// ToDo: NTS cookies are WITHIN the AuthAndEncExt! 
	// ToDo: was tun wenn cookies nicht in aead ext sind
	// check record stack (is it NTS conform?)
	bool receivedNAK = false; // todo: anpassen
	if (NtpExtFieldStackVerification().isStackValid(receivedExtFieldStack, NtpMode::Server_mode, true, receivedNAK) == false)
	{
		LOG_ERROR("NtsClientNtp::processNtpResponse(): wrong NTP EF constellation");
	}
	


	// check unique ID (must match)
	unsigned int uniqIdIndex = receivedExtFieldStack.findExtField(NtpExtFieldType::Unique_Identifier);
	const NtpExtensionField &uniqIdExtField = receivedExtFieldStack.getExtField(uniqIdIndex);
	const std::vector<unsigned char> &receivedUID = ExtField::UniqueIdentifier::getUniqueId(uniqIdExtField);
	if (receivedUID != m_uniqueIdentifier)
	{
		LOG_ERROR("NtsClientNtp::processNtpResponse(): unique ID mismatch");
	}
	m_uniqueIdentifier.clear();
	LOG_TRACE("Client: procNtpRes: extracted uniqueID (ID: {}, size: {})",
		getItemID(receivedUID),
		receivedUID.size());



	// Integrit�tspr�fung der empfangenen Nachricht
	unsigned int aeadExtFieldIndex = receivedExtFieldStack.findExtField(NtpExtFieldType::NTS_Authenticator_and_Encrypted_Extension_Fields);
	const NtpExtensionField &extractedAeadEF = receivedExtFieldStack.getExtField(aeadExtFieldIndex);
	if (verifyNtpMessage(receivedNtpResponse, extractedAeadEF) == false)
	{
		LOG_ERROR("NtsClientNtp::processNtpResponse(): Integrit�tspr�fung fehlgeschlagen");
	}

	// FOR TRACE
	rawNtpPacket = receivedNtpResponse.getSerializedPackage();
	LOG_TRACE("Client: procNtpRes: NTP Packet (after AEAD --> packet size: {}, packet ID: {}, num EFs: {}",
		rawNtpPacket.size(),
		getItemID(rawNtpPacket),
		receivedNtpResponse.getExtensionFieldCount());



	// ggf. verschl�sselte EF verarbeiten
	if (m_decryptedExtFields.size() != 0)
	{
		LOG_TRACE("NtsClientNtp::processNtpResponse(): start parsing decrypted extension fields");

		NtpExtFieldStack decryptedExtFieldStack(m_decryptedExtFields);
		LOG_TRACE("NtsClientNtp::processNtpResponse(): decrypted EF (amount): {}", decryptedExtFieldStack.getStackSize());
		
		// Cookie(s) extrahieren und speichern
		int countCookies = 0;
		for (unsigned int index = 1; index <= decryptedExtFieldStack.getStackSize(); index++)
		{
			const NtpExtensionField &extField = decryptedExtFieldStack.getExtField(index);
			if (extField.getFieldType() == NtpExtFieldType::NTS_Cookie)
			{
				if (ExtField::NtsCookie::isValid(extField) == false)
				{
					LOG_ERROR("NtsClientNtp::processNtpResponse(): extracted raw Cookie is invalid");
				}
				else
				{
					m_cookieStack.push(ExtField::NtsCookie::getCookie(extField));
					countCookies++;
					LOG_TRACE("Client: procNtpRes: pushed received cookie into stack: ID: {}", getItemID(ExtField::NtsCookie::getCookie(extField)));
				}
			}
		}
		if (countCookies == 0)
		{
			LOG_INFO("NtsClientNtp::processNtpResponse(): Keine Cookies empfangen");
		}

		// FOR TRACE: list cookies
		std::queue<std::vector<unsigned char>> tempCookieStack(m_cookieStack);
		LOG_TRACE("Client: procNtpRes: amount of cookies (after extract): {}", tempCookieStack.size());
		std::stringstream CookieStackStr;
		while (tempCookieStack.empty() == false)
		{
			CookieStackStr << getItemID(tempCookieStack.front()) << "  ";
			tempCookieStack.pop();
		}
		LOG_TRACE("Client: procNtpRes: cookie stack: {}", CookieStackStr.str());


	}

	



	// set time :)
	receivedNtpResponse.setUseMessageForTimeSync(true);


	LOG_TRACE("Client: procNtpRes: FINISHED");
	LOG_TRACE("");
}





void NtsClientNtp::addCookie(std::vector<unsigned char> cookie)
{
	if (cookie.size() == 0)
	{
		LOG_ERROR("NtsClientNtp::addCookie: invalid size of the cookie (is zero)");
	}
	else
	{
		this->m_cookieStack.push(cookie);
	}
}

void NtsClientNtp::setAeadAlgorithm(AeadAlgorithm aeadAlgo)
{
	this->m_aeadAlgo = aeadAlgo;
}

void NtsClientNtp::setC2SKey(std::vector<unsigned char> C2SKey)
{
	if (C2SKey.size() == 0)
	{
		LOG_ERROR("NtsClientNtp::setC2SKey: invalid key size (is zero)");
	}
	else
	{
		this->m_C2SKey = C2SKey;
	}
}

void NtsClientNtp::setS2CKey(std::vector<unsigned char> S2CKey)
{
	if (S2CKey.size() == 0)
	{
		LOG_ERROR("NtsClientNtp::setS2CKey: invalid key size (is zero)");
	}
	else
	{
		this->m_S2CKey = S2CKey;
	}
}

void NtsClientNtp::setPlaceholderSize(unsigned short size)
{
	if (size == 0)
	{
		LOG_ERROR("NtsClientNtp::setPlaceholderSize: invalid placeholder size (is zero)");
	}
	else
	{
		this->m_placeholderSize = size;
	}
}




bool NtsClientNtp::verifyNtpMessage(NtpNtsInterface & receivedNtpRequest, NtpExtensionField const &aeadExtField)
{
	bool status = true;

	if (ExtField::NtsAuthAndEncEF::isValid(aeadExtField) == false)
	{
		LOG_ERROR("NtsClientNtp::verifyNtpMessage(): aeadExtField is invalid");
		status = false;
	}

	std::vector<unsigned char> nonce = ExtField::NtsAuthAndEncEF::getNonce(aeadExtField);
	std::vector<unsigned char> encryptedContent = ExtField::NtsAuthAndEncEF::getCiphertext(aeadExtField);

	// TODO das sollte ge�ndert werden
	int extFieldCount = receivedNtpRequest.getExtensionFieldCount();
	for (int index = 0; index <= extFieldCount - 1; index++)
	{
		unsigned short type = static_cast<unsigned short>(NtpExtFieldType::NTS_Authenticator_and_Encrypted_Extension_Fields);
		if (receivedNtpRequest.getExtensionFieldType(index) == type)
		{
			receivedNtpRequest.delExtensionField(index);
			break;
		}
	}

	// check integrity and encrypt
	AEAD aead;
	aead.setAeadAlgorithm(m_aeadAlgo);
	aead.setKey(m_S2CKey);
	aead.setCiphertext(encryptedContent);
	aead.setNonce(nonce);
	aead.setAssociatedData(receivedNtpRequest.getSerializedPackage());

	if (aead.decrypt() == false)
	{
		LOG_ERROR("NtsClientNtp::verifyNtpMessage: decryption failed");
		status = false;
	}

	// save decrypted ext fields (if available)
	m_decryptedExtFields = aead.getPlaintext();

	return status;
}

size_t  NtsClientNtp::getCookieStackSize() const
{
	return m_cookieStack.size();
}
