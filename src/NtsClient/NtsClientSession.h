/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#pragma once


#include <vector>
#include "../Logger/Logger.h"
#include "../generalFunctions.h"

#include "./NtsClientKe.h"
#include "NtsClientNtp.h"
#include "NtpNtsInterface.h"
#include "./TlsClient.h"
#include <string>

struct NtsSettings;

class NtsClientSession
{
public:
	NtsClientSession(NtsSettings const& settings, std::string const& keServerIp);
	~NtsClientSession() = default;

	void generateNtpRequest(NtpNtsInterface &newNtpRequest);
	void processNtpResponse(NtpNtsInterface &receivedNtpResponse);

	std::string const & getKeServerIp() const;

private:

	// TLS connection
	void startTlsKe(std::string const& serverIp, int port);
	void processTlsResponsePostProcess();

	NtsClientKe m_ntsTlsKe;
	NtsClientNtp m_ntpProtocol;

	bool m_ntsKeSuccessful;

	
	// int lastActivity -->  (lifetime)

	std::string m_keServerIp;
	NtsSettings const & m_settings;
};

