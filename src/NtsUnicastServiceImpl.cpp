#include "NtsUnicastService.h"
#include "NtsUnicastServiceImpl.h"


NtsUnicastServiceImpl::NtsUnicastServiceImpl()
	:
	m_ntsClient(m_settings),
	m_ntsServer(m_settings),
	m_ntsMode(NtsMode::not_set),
	m_ntsState(NtsState::not_init)
{
}

void NtsUnicastServiceImpl::initialization(std::string const & globalConfigFile)
{
	try
	{
#ifdef _DEBUG
		LOG_INFO("NTS (draft-ietf-ntp-using-nts-for-ntp-15) v{} ({}) - debug build", NTS_VERSION, NTS_COMPILE_TIME);
#else
		LOG_INFO("NTS (draft-ietf-ntp-using-nts-for-ntp-15) v{} ({}) - release build", NTS_VERSION, NTS_COMPILE_TIME);
#endif
		//LOG_INFO("working directory: {}", boost::filesystem::path(boost::filesystem::current_path()));
		LOG_INFO("config file (parameter): {}", globalConfigFile);

		m_settings = readConfigFile(globalConfigFile);
		m_ntsMode = m_settings.general.ntsServiceMode;
//		Logger::getInstance().init(m_settings.readSettings());

		if (m_ntsMode == NtsMode::client)
		{
			LOG_INFO("NTS mode: client");
			// keine init funktion n�tig
		}
		else if (m_ntsMode == NtsMode::server)
		{
			LOG_INFO("NTS mode: server");
			m_ntsServer.initialization();
		}
		else
		{
			LOG_DEBUG("NTS mode: not defined!");
		}

		m_ntsState = NtsState::init;
	}
	catch (std::exception const &e)
	{
		LOG_FATAL("NTS: a critical error has occurred. Message: {}", e.what());
		throw;
	}
	catch (...)
	{
		LOG_FATAL("NTS: a critical error has occurred. Reason unknown!");
		throw;
	}
}

void NtsUnicastServiceImpl::reset()
{
	try
	{
		shutdown();
		//m_ntsClient = NtsClient();
		//m_ntsServer = NtsServer();

		//(&m_ntsServer)->~NtsServer();
		//new (&m_ntsServer) NtsServer();

		m_ntsMode = NtsMode::not_set;
		m_ntsState = NtsState::not_init;
	}
	catch (std::exception const &e)
	{
		LOG_FATAL("NTS: a critical error has occurred. Message: {}", e.what());
		throw;
	}
	catch (...)
	{
		LOG_FATAL("NTS: a critical error has occurred. Reason unknown!");
		throw;
	}
}

void NtsUnicastServiceImpl::shutdown()
{
	if (m_ntsMode == NtsMode::server)
	{
		// spdlog shutdown?
		m_ntsServer.shutdown();
	}
}

void NtsUnicastServiceImpl::processingNtpMessage(NtpNtsInterface & receivedPackage, NtpNtsInterface & newResponsePackage)
{
	try
	{
		if (isInit() == false)
		{
			LOG_ERROR("NtsUnicastServiceImpl::processingNtpMessage: NTS is not init");
		}

		if (m_ntsMode == NtsMode::client)
		{
			m_ntsClient.processNtpResponse(receivedPackage);
			m_ntsClient.generateNtpRequest(newResponsePackage);
		}
		else if (m_ntsMode == NtsMode::server)
		{
			m_ntsServer.generateNtpResponse(receivedPackage, newResponsePackage);
		}
		else
		{
			LOG_ERROR("NtsUnicastServiceImpl::processingNtpMessage: NTS mode not set");
		}
	}
	catch (std::exception const &e)
	{
		LOG_FATAL("NTS: a critical error has occurred. Message: {}", e.what());
		throw;
	}
	catch (...)
	{
		LOG_FATAL("NTS: a critical error has occurred. Reason unknown!");
		throw;
	}
}

void NtsUnicastServiceImpl::processingReceivedNtpMessage(NtpNtsInterface & receivedPackage)
{
	try
	{
		if (isInit() == false)
		{
			LOG_ERROR("NtsUnicastServiceImpl::processingReceivedNtpMessage: NTS is not init");
		}
		if (m_ntsMode == NtsMode::client)
		{
			m_ntsClient.processNtpResponse(receivedPackage);
		}
		else
		{
			LOG_ERROR("NtsUnicastServiceImpl::processingReceivedNtpMessage: NTS is not in client mode");
		}
	}
	catch (std::exception const &e)
	{
		LOG_FATAL("NTS: a critical error has occurred. Message: {}", e.what());
		throw;
	}
	catch (...)
	{
		LOG_FATAL("NTS: a critical error has occurred. Reason unknown!");
		throw;
	}
}

void NtsUnicastServiceImpl::createRequestMessage(NtpNtsInterface & newResponsePackage)
{
	try
	{
		if (isInit() == false)
		{
			LOG_ERROR("NtsUnicastServiceImpl::createRequestMessage: NTS is not init");
		}
		if (m_ntsMode == NtsMode::client)
		{
			m_ntsClient.generateNtpRequest(newResponsePackage);
		}
		else
		{
			LOG_ERROR("NtsUnicastServiceImpl::createRequestMessage: NTS is not in client mode");
		}
	}
	catch (std::exception const &e)
	{
		LOG_FATAL("NTS: a critical error has occurred. Message: {}", e.what());
		throw;
	}
	catch (...)
	{
		LOG_FATAL("NTS: a critical error has occurred. Reason unknown!");
		throw;
	}
}

bool NtsUnicastServiceImpl::isInit()
{
	if (m_ntsState == NtsState::init)
	{ 
		return true;
	}
	else
	{
		return false;
	}
}

bool NtsUnicastServiceImpl::isClientMode()
{
	if (m_ntsMode == NtsMode::client)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool NtsUnicastServiceImpl::isServerMode()
{
	if (m_ntsMode == NtsMode::server)
	{
		return true;
	}
	else
	{
		return false;
	}
}
