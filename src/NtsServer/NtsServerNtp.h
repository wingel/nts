/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#pragma once

#include "NtpNtsInterface.h"
#include "../MasterKey.h"
#include "../NtpExtensionFields/NtpExtensionField.h"


class NtsServerNtp
{
public:
	NtsServerNtp();
	~NtsServerNtp() = default;

	void generateNtpResponse(NtpNtsInterface &receivedNtpRequest, NtpNtsInterface &newNtpResponse);
	void setMasterKey(MasterKey *m_masterKey);

private:

	bool decryptAndParseCookie(std::vector<unsigned char> const &rawCookie);
	bool verifyNtpMessage(NtpNtsInterface &receivedNtpRequest, NtpExtensionField const &aeadExtField);

	std::vector<unsigned char> createCookie();


	AeadAlgorithm getServerAeadAlgo() const;
	std::vector<unsigned char> getServerAeadKey(unsigned short keyId) const;
	unsigned short getServerAeadKeyId() const;


	AeadAlgorithm m_receivedAeadAlgorithm;
	std::vector<unsigned char> m_receivedC2SKey;
	std::vector<unsigned char> m_receivedS2CKey;
	std::vector<unsigned char> m_decryptedExtFields;


	MasterKey *m_masterKey; // TODO: pointer?  dann wenigsten allozieren oder was?   ...oder nullptr?
};

