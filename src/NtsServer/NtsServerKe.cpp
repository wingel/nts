#include "NtsServerKe.h"
#include "../Logger/Logger.h"
#include "../Records/RecStackVerification.h"

#include "../Records/RecType_EndOfMessage.h"
#include "../Records/RecType_NewCookie.h"
#include "../Records/RecType_AeadAlgoNeg.h"
#include "../Records/RecType_NextProtNeg.h"
#include "../AEAD.h"
#include "../Cookie/CookieContent.h"
#include "../Cookie/Cookie.h"
#include "../cryptoFunctions.h"
#include <openssl/err.h>
#include "../generalFunctions.h"

/*
	#include "../Records/RecType_Error.h"
	#include "../Records/RecType_Warning.h"
*/


std::mutex NtsServerKe::m_mutex;


NtsServerKe::NtsServerKe()
	:
	m_masterKey(nullptr)
{
	// ToDo: make it better
	m_supportedNextProt.push_back(NextProtocol::NTPv4);
	m_supportedAeadAlgos.push_back(AeadAlgorithm::AEAD_AES_SIV_CMAC_256);
	m_supportedAeadAlgos.push_back(AeadAlgorithm::AEAD_AES_SIV_CMAC_384);
	m_supportedAeadAlgos.push_back(AeadAlgorithm::AEAD_AES_SIV_CMAC_512);
}



std::vector<unsigned char> NtsServerKe::processTlsRequest(std::vector<unsigned char> const & tlsAppDataPayload, SSL &ssl) const
{
	std::lock_guard<std::mutex> guard(m_mutex);

	// seperate records from stream
	RecordStack recStack(tlsAppDataPayload);


	// check record stack (is it NTS conform?)
	if (RecStackVerification().clientRequestStackIsValid(recStack, true) == false)
	{
		LOG_ERROR("NtsServerKe: invalid TLS data");
	}


	// Next Protocols selektieren
	Record ntsNextProtNegRec;
	if (processNextProtNeg(recStack, ntsNextProtNegRec) == false)
	{
		LOG_ERROR("NtsServerKe: Next_Protocol_Negotiation failed");
	}


	// AEAD-Algorithmus selektieren
	Record aeadAlgoNeg;
	if (processAeadAlgoNeg(recStack, aeadAlgoNeg) == false)
	{
		LOG_ERROR("NtsServerKe: AEAD Algorithm Negotiation failed");
	}

	// TLS-schlüssel exportieren
	AeadAlgorithm selectedAeadAlgorithm = RecType::AeadAlgoNeg::getAlgorithm(aeadAlgoNeg, 1);
	std::vector<unsigned char> C2SKey = extractTLSKey(ssl, TLS_Exporter_Label, NextProtocol::NTPv4, selectedAeadAlgorithm, TLS_C2S_KEY_ID);
	std::vector<unsigned char> S2CKey = extractTLSKey(ssl, TLS_Exporter_Label, NextProtocol::NTPv4, selectedAeadAlgorithm, TLS_S2C_KEY_ID);

    printf("%s:\n", __PRETTY_FUNCTION__);
    printf("c2s %u %s\n", C2SKey.size(), binToHexStr(C2SKey, "", 1024, true).c_str());
    printf("s2c %u %s\n", S2CKey.size(), binToHexStr(S2CKey, "", 1024, true).c_str());


	// Cookies generieren
	std::vector<Record> cookieStack;
	for (int i = 1; i <= MAX_COOKIES; i++)
	{
		std::vector<unsigned char> cookie = createCookie(selectedAeadAlgorithm, S2CKey, C2SKey);
		Record cookieRec;
		RecType::NewCookie::format(cookieRec);
		RecType::NewCookie::setCookie(cookieRec, cookie);
		cookieStack.push_back(cookieRec);
	}


	// create Record: End of Message
	Record EndOfMsgRec;
	RecType::EndOfMessage::format(EndOfMsgRec);


	// generate TLS ApplicationData payload
	RecordStack responseStack;
	responseStack.addRecord(ntsNextProtNegRec);
	responseStack.addRecord(aeadAlgoNeg);
	for (auto item : cookieStack)
	{
		responseStack.addRecord(item);
	}
	responseStack.addRecord(EndOfMsgRec);


	return responseStack.getSerializedData();
}





// ========================================================================================================================

bool NtsServerKe::processNextProtNeg(RecordStack recStack, Record & recordOut) const
{
	recordOut.clear();

	// search record
	unsigned index = recStack.findRecord(RecordType::Next_Protocol_Negotiation);
	if (index == 0)
	{
		LOG_ERROR("NtsServerKe: record not found: 'Next_Protocol_Negotiation'");
		return false;
	}

	// select supported Next Protocols
	const Record &clientRecord = recStack.getRecord(index);
	std::vector<unsigned short> nextProtNegList = RecType::NextProtNeg::getProtocolList(clientRecord);
	if (selectNextProtNeg(nextProtNegList) == false)
	{
		LOG_ERROR("NtsServerKe: NextProt-Aushandlung fehlgeschlagen");
		return false;
	}

	// build response record
	RecType::NextProtNeg::format(recordOut);
	RecType::NextProtNeg::setProtocolList(recordOut, nextProtNegList);
	return true;
}



bool NtsServerKe::processAeadAlgoNeg(RecordStack recStack, Record & recordOut) const
{
	recordOut.clear();

	// search record
	unsigned index = recStack.findRecord(RecordType::AEAD_Algorithm_Negotiation);
	if (index == 0)
	{
		LOG_ERROR("NtsServerKe: record not found: 'AEAD_Algorithm_Negotiation'");
		return false;
	}

	// select supported AEAD Protocols
	const Record &clientRecord = recStack.getRecord(index);
	std::vector<unsigned short> aeadAlgoList = RecType::AeadAlgoNeg::getAlgorithmList(clientRecord);
	if (selectAeadAlgorithm(aeadAlgoList) == false)
	{
		LOG_ERROR("NtsServerKe: AEAD-Aushandlung fehlgeschlagen");
		return false;
	}

	// build response record
	RecType::AeadAlgoNeg::format(recordOut);
	RecType::AeadAlgoNeg::setAlgorithmList(recordOut, aeadAlgoList);
	return true;
}




bool NtsServerKe::selectNextProtNeg(std::vector<unsigned short>& nextProtNegList) const
{
	/*
		- Clientliste nehmen
		- alle löschen, die der server nicht sprechen kann
		- liste kann also auch leer sein
	*/

	if (nextProtNegList.size() == 0)
	{
		LOG_ERROR("Next_Protocol_Negotiation: client requst MUSS min. 1 Protokoll beinhalten");
		nextProtNegList.clear();
		return false;
	}

	std::vector<unsigned short> acceptedProtList;
	for (size_t i = 0; i < nextProtNegList.size(); i++)
	{
		if (isSupported(static_cast<NextProtocol>(nextProtNegList.at(i))) == true)
		{
			acceptedProtList.push_back(nextProtNegList.at(i));
		}
	}
	
	if (acceptedProtList.size() == 0)
	{
		LOG_WARN("NtsServerKe: keine unterstützten Next Protocols gefunden");
		nextProtNegList.clear();
		return false;
	}
	else
	{
		nextProtNegList = std::move(acceptedProtList);
		return true;
	}
}

bool NtsServerKe::selectAeadAlgorithm(std::vector<unsigned short>& aeadAlgoList) const
{
	/*
	- nur EINEN AEAD algo auswählen, den client und server verstehen
	- auswahl nach Prioität (absteigend)
	- darf nicht leer sein (da client/server AEAD_AES_SIV_CMAC_256 unterstützen MÜSSEN)
	*/
	if (aeadAlgoList.size() == 0)
	{
		LOG_ERROR("selectAeadAlgorithm: client requst MUSS min. 1 AEAD_Algo beinhalten");
		aeadAlgoList.clear();
		return false;
	}

	std::vector<unsigned short> acceptedAlgo;
	for (size_t i = 0; i < aeadAlgoList.size(); i++)
	{
		if (isSupported(static_cast<AeadAlgorithm>(aeadAlgoList.at(i))) == true)
		{
			acceptedAlgo.push_back(aeadAlgoList.at(i));
			break; // only 1 AEAD algo is allowed
		}
	}

	if (acceptedAlgo.size() == 0)
	{
		LOG_WARN("NtsServerKe: keine unterstützten AEAD Algos gefunden");
		aeadAlgoList.clear();
		return false;
	}
	else
	{
		aeadAlgoList = std::move(acceptedAlgo);
		return true;
	}
}

std::vector<unsigned char> NtsServerKe::createCookie(AeadAlgorithm negAeadAlgo, std::vector<unsigned char> S2CKey, std::vector<unsigned char> C2SKey) const
{
	// get master AEAD Algo, Key and ID
	AeadAlgorithm masterAeadAlgo = getServerAeadAlgo();
	unsigned int masterAeadKeyId = getServerAeadKeyId();
	std::vector<unsigned char> masterAeadKey = getServerAeadKey(masterAeadKeyId);

	// create plain cookie
	CookieContent plainCookie(negAeadAlgo, S2CKey, C2SKey);

	// create new nonce
	std::vector<unsigned char> nonce = getRandomNumber(NONCE_SIZE);
	if (nonce.size() == 0)
	{
		LOG_ERROR("createCookie: nonce is zero");
	}

	// encrypt plain cookie
	AEAD aead;
	std::vector<unsigned char> encCookieContent; //aead out
	if (aead.isSupported(masterAeadAlgo) == true)
	{
		aead.setAeadAlgorithm(masterAeadAlgo);
		aead.setKey(masterAeadKey);
		aead.setPlaintext(plainCookie.getSerializedData());
		aead.setNonce(nonce);
		if (aead.encrypt() == false)
		{
			LOG_ERROR("createCookie: ENCRYPTION failed!");
		}
		else
		{
			encCookieContent = aead.getCiphertext();
		}
	}
	else
	{
		LOG_ERROR("createCookie: ENCRYPTION failed: aead algo not supported");
	}

	// create final cookie
	Cookie cookie(masterAeadKeyId, nonce, encCookieContent);
	if (cookie.isValid() == false)
	{
		LOG_ERROR("createCookie: cookie is invald");
	}
	
	return cookie.getSerializedData();
}





bool NtsServerKe::isSupported(NextProtocol nextProt) const
{
	for (auto prot : m_supportedNextProt)
	{
		if (prot == nextProt)
		{
			return true;
		}
	}
	return false;
}

bool NtsServerKe::isSupported(AeadAlgorithm AeadAlgo) const
{
	for (auto algo : m_supportedAeadAlgos)
	{
		if (algo == AeadAlgo)
		{
			return true;
		}
	}
	return false;
}



std::vector<unsigned char> NtsServerKe::extractTLSKey(SSL & ssl, std::string tlsExporterLabel, NextProtocol nextProt, AeadAlgorithm aeadAlgo, unsigned short mode) const
{
	unsigned short nextProtocol = static_cast<unsigned short>(nextProt);
	unsigned short aeadAlgorithm = static_cast<unsigned short>(aeadAlgo);
	unsigned int tlsKeySize = determineKeySize(aeadAlgo);
	if (tlsKeySize == 0)
	{
		LOG_ERROR("NtsServerKe::extractTLSKey(): invalid key size");
	}

	std::vector<unsigned char> context(5); // 5 bytes

	 // set NextProtocol
	context.at(0) = (nextProtocol >> 8) & 0xFF;
	context.at(1) = nextProtocol & 0xFF;

	// set AeadAlgorithm
	context.at(2) = (aeadAlgorithm >> 8) & 0xFF;
	context.at(3) = aeadAlgorithm & 0xFF;

	// set Key mode
	context.at(4) = mode & 0xFF;

	// export key
	std::vector<unsigned char> tlsKey(tlsKeySize);
	int useContext = 1;
	int ret = SSL_export_keying_material(&ssl, tlsKey.data(), tlsKeySize, tlsExporterLabel.c_str(), tlsExporterLabel.length(), context.data(), context.size(), useContext); // RFC 5705

	if (ret != 1)
	{
		char errorBuf[255];
		LOG_ERROR("NtsServerKe::extractTLSKey(): SSL_export_keying_material failed --> {}", ERR_error_string(ERR_get_error(), errorBuf));
	}

	return tlsKey;
}





AeadAlgorithm NtsServerKe::getServerAeadAlgo() const
{
	return SERVER_AEAD_ALGO;
}

std::vector<unsigned char> NtsServerKe::getServerAeadKey(unsigned short keyId) const
{
	if (m_masterKey == nullptr)
	{
		LOG_ERROR("NtsServerKe::getServerAeadKey(): m_masterKey == nullptr");
		return std::vector<unsigned char>();
	}
	return m_masterKey->getKey(keyId);
}

unsigned short NtsServerKe::getServerAeadKeyId() const
{
	if (m_masterKey == nullptr)
	{
		LOG_ERROR("NtsServerKe::getServerAeadKeyId(): m_masterKey == nullptr");
		return 0;
	}
	return m_masterKey->getCurrentKeyId();;
}

void NtsServerKe::setMasterKey(MasterKey * masterKey)
{
	this->m_masterKey = masterKey;
}