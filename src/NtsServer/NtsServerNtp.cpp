#include "NtsServerNtp.h"
#include "../AEAD.h"
#include "../Logger/Logger.h"
#include "../cryptoFunctions.h"
#include "../NtpExtensionFields/NtpExtFieldStack.h"
#include "../NtpExtensionFields/NtpExtFieldStackVerification.h"
#include "../NtpExtensionFields/ExtF_NtsCookie.h"
#include "../NtpExtensionFields/ExtF_NtsCookiePlaceholder.h"
#include "../NtpExtensionFields/ExtF_UniqueIdentifier.h"
#include "../NtpExtensionFields/ExtF_NtsAuthAndEncEF.h"
#include "../Cookie/Cookie.h"
#include "../Cookie/CookieContent.h"


NtsServerNtp::NtsServerNtp()
	:
	m_receivedAeadAlgorithm(AeadAlgorithm::NOT_DEFINED)
{
}

void NtsServerNtp::generateNtpResponse(NtpNtsInterface & receivedNtpRequest, NtpNtsInterface & newNtpResponse)
{
	LOG_TRACE("");
	LOG_TRACE("Server: genNtpReq: STARTED");
	LOG_TRACE("Server: genNtpReq: client IP address: {}", newNtpResponse.getIpAddress());

	// ADVANCED TRACE OUT
	int numEFs = receivedNtpRequest.getExtensionFieldCount();
	std::vector<unsigned char> rawNtpPacket = receivedNtpRequest.getSerializedPackage();
	LOG_TRACE("Server: genNtpReq: NTP Packet --> packet size: {}, packet ID: {}, num EFs: {}", rawNtpPacket.size(), getItemID(rawNtpPacket), numEFs);

	for (int i = 0; i < numEFs; i++)
	{
		LOG_TRACE("Server: genNtpReq: NTP EF --> index: {}, type: {}, value size: {}, value (ID): {}", 
			i, 
			receivedNtpRequest.getExtensionFieldType(i), 
			receivedNtpRequest.getExtensionFieldValue(i).size(), 
			getItemID(receivedNtpRequest.getExtensionFieldValue(i))
		);
	}
	// ADVANCED TRACE OUT

	//==================================  Überprüfungen  ==================================

	LOG_TRACE("Server: procNtpRes: Phase 1 of 2 - verify request");

	// Erweiterungsfelder extrahieren    
	int numReceivedExtFields = receivedNtpRequest.getExtensionFieldCount();
	NtpExtFieldStack receivedExtFieldStack;
	if (numReceivedExtFields == 0)
	{
		LOG_ERROR("NtsServerNtp::generateNtpResponse(): empfangenes NTP beinhaltet keine EFs");
		newNtpResponse.setSendResponseMessage(false);
		return;
	}
	for (int index = 0; index <= numReceivedExtFields-1; index++)
	{
		NtpExtensionField ntpExtF;
		ntpExtF.setFieldType(receivedNtpRequest.getExtensionFieldType(index));
		ntpExtF.setValue(receivedNtpRequest.getExtensionFieldValue(index));
		receivedExtFieldStack.addExtField(ntpExtF);  //TODO: hier wäre const& sinnvoll --> NTP/NTS interface anpassen?
	}



	// check record stack (is it NTS conform?)
	if (NtpExtFieldStackVerification().isStackValid(receivedExtFieldStack, NtpMode::Client_mode, true) == false)
	{
		LOG_ERROR("NtsServerNtp::generateNtpResponse(): wrong NTP EF constellation");
		newNtpResponse.setSendResponseMessage(false);
		return;
	}
	


	// Cookie extrahieren, entschlüsseln und parsen
	unsigned int ntsCookieIndex = receivedExtFieldStack.findExtField(NtpExtFieldType::NTS_Cookie);
	const NtpExtensionField &extractedNtsCookie = receivedExtFieldStack.getExtField(ntsCookieIndex);
	if (ExtField::NtsCookie::isValid(extractedNtsCookie) == false)
	{
		LOG_ERROR("NtsServerNtp::generateNtpResponse(): extracted raw Cookie is invalid");
		newNtpResponse.setSendResponseMessage(false);
		return;
	}
	const std::vector<unsigned char> &rawCookie = ExtField::NtsCookie::getCookie(extractedNtsCookie);
	if (decryptAndParseCookie(rawCookie) == false)
	{
		LOG_ERROR("NtsServerNtp::generateNtpResponse(): Cookie-Verarbeitung fehlgeschlagen");
		newNtpResponse.setSendResponseMessage(false);
		return;
	}
	LOG_TRACE("Server: genNtpReq: ntsCookie extracted (cookie ID: {}, size: {})", 
			getItemID(ExtField::NtsCookie::getCookie(extractedNtsCookie)), 
			ExtField::NtsCookie::getCookieSize(extractedNtsCookie));


	// Cookie size == placeholder size?
	unsigned int cookieSize = ExtField::NtsCookie::getCookieSize(extractedNtsCookie);
	for (unsigned int index = 1; index <= receivedExtFieldStack.getStackSize(); index++)
	{
		const NtpExtensionField &extField = receivedExtFieldStack.getExtField(index);
		if (extField.getFieldType() == NtpExtFieldType::NTS_Cookie_Placeholder)
		{
			unsigned int placeholderSize = ExtField::NtsCookiePlaceholder::getPlaceholderSize(extField);

			LOG_TRACE("Server: genNtpReq: cookie placeholder found (EF index: {}, placeholder size: {}", index - 1, placeholderSize);

			if (placeholderSize != cookieSize)
			{
				LOG_ERROR("NtsServerNtp::generateNtpResponse(): placeholderSize mismatch");
				newNtpResponse.setSendResponseMessage(false);
				return;
			}

		}
	}




	// Integritätsprüfung der empfangenen Nachricht
	unsigned int aeadExtFieldIndex = receivedExtFieldStack.findExtField(NtpExtFieldType::NTS_Authenticator_and_Encrypted_Extension_Fields);
	const NtpExtensionField &extractedAeadEF = receivedExtFieldStack.getExtField(aeadExtFieldIndex);
	if (verifyNtpMessage(receivedNtpRequest, extractedAeadEF) == false)
	{
		LOG_ERROR("NtsServerNtp::generateNtpResponse(): Integritätsprüfung fehlgeschlagen");
		newNtpResponse.setSendResponseMessage(false);
		return;
	}
	
	// FOR TRACE
	rawNtpPacket = receivedNtpRequest.getSerializedPackage();
	LOG_TRACE("Server: procNtpRes: NTP Packet (after AEAD --> packet size: {}, packet ID: {}, num EFs: {}", 
		rawNtpPacket.size(),
		getItemID(rawNtpPacket),
		receivedNtpRequest.getExtensionFieldCount());


	// ggf. verschlüsselte EF verarbeiten
	if (m_decryptedExtFields.size() != 0)
	{
		// mach irgendwas
		// todo: what we do if cookies available
		LOG_INFO("NtsServerNtp::generateNtpResponse(): Erweiterungsfelder entschlüsselt!");
	}

	




	// =================================  Antwort erstellen  ==================================

	LOG_TRACE("Server: genNtpReq: Phase 2 of 2 - create response");


	// create EF: Unique Identifier
	int index = receivedExtFieldStack.findExtField(NtpExtFieldType::Unique_Identifier);
	const NtpExtensionField &uniqIdentEF = receivedExtFieldStack.getExtField(index);
	if (ExtField::UniqueIdentifier::isValid(uniqIdentEF) == false) // Todo: dies in der ExtFieldVerification machen
	{
		LOG_INFO("NtsServerNtp::generateNtpResponse(): uniqIdentEF invalid");
	}
	const std::vector<unsigned char> &uniqueIdVal = ExtField::UniqueIdentifier::getUniqueId(uniqIdentEF);

	NtpExtensionField newUniqIdentEF;
	ExtField::UniqueIdentifier::format(newUniqIdentEF);
	ExtField::UniqueIdentifier::setUniqueId(newUniqIdentEF, uniqueIdVal);


	// Erweiterungsfeld in NTP einbetten
	newNtpResponse.addExtensionField(uniqIdentEF.getFieldTypeVal(), uniqIdentEF.getValue());
	LOG_TRACE("Server: genNtpReq: NTP EF (uniqueID) added (type: {}, value size: {}, value ID: {})",
		uniqIdentEF.getFieldTypeVal(),
		uniqIdentEF.getValueLength(),
		getItemID(uniqIdentEF.getValue()));



	// create EF: NTS Cookie(s) 
	std::vector<NtpExtensionField> ntsCookieStack;
	int numPlaceholder = receivedExtFieldStack.countExtFieldType(NtpExtFieldType::NTS_Cookie_Placeholder);
	int numNewCookies = 1 + numPlaceholder;
	if (numNewCookies < 0)
	{
		//TODO: using assert?
		assert("numNewCookies < 0");
	}
	else if (numNewCookies > 0)
	{
		for (int i = 1; i <= numNewCookies; i++)
		{
			NtpExtensionField cookie;
			ExtField::NtsCookie::format(cookie);
			ExtField::NtsCookie::setCookie(cookie, createCookie());
			ntsCookieStack.push_back(cookie);
		}
	}

	
	// generate cookie extension field stream
	std::vector<unsigned char> plainExtensionStream;
	for (auto cookie : ntsCookieStack)
	{
		const std::vector<unsigned char> &stream = cookie.getSerializedData();
		plainExtensionStream.insert(plainExtensionStream.end(), stream.begin(), stream.end());
		LOG_TRACE("Server: genNtpReq: NTP EF --> AuthAndEncExt EF: (ntsCookie) added (type: {}, value size: {}, value ID: {})",
			cookie.getFieldTypeVal(),
			cookie.getValueLength(),
			getItemID(cookie.getValue()));
	}




	// create EF: NTS Authenticator and Encrypted Extension Fields
	NtpExtensionField AuthAndEncExF;
	ExtField::NtsAuthAndEncEF::format(AuthAndEncExF);
	std::vector<unsigned char> nonce = getRandomNumber(NONCE_SIZE);
	std::vector<unsigned char> associatedData;


	// NTP Zeitstempel aktualisieren
	newNtpResponse.setSendResponseMessage(true);
	newNtpResponse.updateTransmitTimestamp();


	// ==================================== TIME CRIT ====================================
	auto start = std::chrono::steady_clock::now();
	// -----------------------------------------------------------------------------------

	associatedData = newNtpResponse.getSerializedPackage();
	ExtField::NtsAuthAndEncEF::performAead(AuthAndEncExF, m_receivedAeadAlgorithm, m_receivedS2CKey, plainExtensionStream, nonce, associatedData);

	// AuthAndEncExF in NTP einbetten
	newNtpResponse.addExtensionField(AuthAndEncExF.getFieldTypeVal(), AuthAndEncExF.getValue());

	// -----------------------------------------------------------------------------------
	auto end = std::chrono::steady_clock::now();
	auto time = end - start;
	std::chrono::duration<double, std::micro> proccess_time_us = time;
	std::cout.precision(10);
	LOG_DEBUG("Server: genNtpReq: TimeResponse crit time: {} us", proccess_time_us.count());
	// ==================================== TIME CRIT ====================================

	LOG_TRACE("Server: genNtpReq: perform AEAD (serialized NTP ID: {}, serialized NTP size: {})",
		getItemID(associatedData),
		associatedData.size());

	LOG_TRACE("Server: genNtpReq: NTP EF (AEAD ExtF) added (type: {}, value size: {})",
		AuthAndEncExF.getFieldTypeVal(),
		AuthAndEncExF.getValueLength());


	// ADVANCED TRACE OUT
	numEFs = newNtpResponse.getExtensionFieldCount();
	rawNtpPacket = newNtpResponse.getSerializedPackage();
	LOG_TRACE("Server: genNtpReq: NTP Packet --> packet size: {}, packet ID: {}, num EFs: {}",
		rawNtpPacket.size(),
		getItemID(rawNtpPacket),
		numEFs);

	for (int i = 0; i < numEFs; i++)
	{
		LOG_TRACE("Server: genNtpReq: NTP EF --> index: {}, type: {}, value size: {}, value (ID): {}",
			i,
			newNtpResponse.getExtensionFieldType(i),
			newNtpResponse.getExtensionFieldValue(i).size(),
			getItemID(newNtpResponse.getExtensionFieldValue(i)));
	}
	LOG_TRACE("Server: genNtpReq: FINISHED");
	LOG_TRACE("");
	// ADVANCED TRACE OUT

	// on error:
	//newNtpResponse.setSendResponseMessage(false);
	AuthAndEncExF.clear();
	return;
}

void NtsServerNtp::setMasterKey(MasterKey * m_masterKey)
{
	this->m_masterKey = m_masterKey;
}

bool NtsServerNtp::decryptAndParseCookie(std::vector<unsigned char> const & rawCookie)
{
	bool status = true; 

	// check cookie
	Cookie cookie(rawCookie, NONCE_SIZE);
	if (cookie.isValid() == false)
	{
		LOG_ERROR("NtsServerNtp::decryptAndParseCookie(): cookie is invalid");
		status = false;
	}

	// parse cookie
	unsigned short keyID = cookie.getKeyId();
	const std::vector<unsigned char> &nonce = cookie.getNonce();
	const std::vector<unsigned char> &encContent = cookie.getEncCookieContent();

	// get Server AEAD key
	const std::vector<unsigned char> &serverAeadKey = getServerAeadKey(keyID);
	AeadAlgorithm serverAeadAlgo = getServerAeadAlgo();

	// decrypt cookie
	AEAD aead;
	aead.setAeadAlgorithm(serverAeadAlgo);
	aead.setKey(serverAeadKey);
	aead.setCiphertext(encContent);
	aead.setNonce(nonce);
	if (aead.decrypt() == false)
	{
		LOG_ERROR("NtsServerNtp::decryptAndParseCookie(): decryption failed");
		status = false;
	}

	// parse decrypted cookie
	CookieContent cookieContent(aead.getPlaintext());
	if (cookieContent.isValid() == false)
	{
		LOG_ERROR("NtsServerNtp::decryptAndParseCookie(): decrypted cookie is invalid");
		status = false;
	}

	// save extracted values
	m_receivedAeadAlgorithm = cookieContent.getAeadAlgorithm();
	m_receivedC2SKey = cookieContent.getC2SKey();
	m_receivedS2CKey = cookieContent.getS2CKey();

	return status;
}



bool NtsServerNtp::verifyNtpMessage(NtpNtsInterface & receivedNtpRequest, NtpExtensionField const &aeadExtField)
{
	bool status = true;

	if (ExtField::NtsAuthAndEncEF::isValid(aeadExtField) == false)
	{
		LOG_ERROR("NtsServerNtp::verifyNtpMessage(): aeadExtField is invalid");
		status = false;
	}

	std::vector<unsigned char> nonce = ExtField::NtsAuthAndEncEF::getNonce(aeadExtField);
	std::vector<unsigned char> encryptedContent = ExtField::NtsAuthAndEncEF::getCiphertext(aeadExtField);

	// TODO das sollte geändert werden
	int extFieldCount = receivedNtpRequest.getExtensionFieldCount();
	for (int index = 0; index <= extFieldCount - 1; index++)
	{
		unsigned short type = static_cast<unsigned short>(NtpExtFieldType::NTS_Authenticator_and_Encrypted_Extension_Fields);
		if (receivedNtpRequest.getExtensionFieldType(index) == type)
		{
			receivedNtpRequest.delExtensionField(index);
			break;
		}
	}

	// check integrity and encrypt
	AEAD aead;
	aead.setAeadAlgorithm(m_receivedAeadAlgorithm);
	aead.setKey(m_receivedC2SKey);
	aead.setCiphertext(encryptedContent);
	aead.setNonce(nonce);
	aead.setAssociatedData(receivedNtpRequest.getSerializedPackage());

	

	LOG_TRACE("Server: genNtpReq: NTP packet without AEAD EF  AEAD algo:  {}", getAeadAlgorithmStr(m_receivedAeadAlgorithm));

	LOG_TRACE("Server: genNtpReq: NTP packet without AEAD EF  m_receivedS2CKey ID:  {}", getItemID(m_receivedS2CKey));
	LOG_TRACE("Server: genNtpReq: NTP packet without AEAD EF  m_receivedS2CKey SIZE:  {}", m_receivedS2CKey.size());

	LOG_TRACE("Server: genNtpReq: NTP packet without AEAD EF  m_receivedC2SKey ID:  {}", getItemID(m_receivedC2SKey));
	LOG_TRACE("Server: genNtpReq: NTP packet without AEAD EF  m_receivedC2SKey SIZE:  {}", m_receivedC2SKey.size());

	LOG_TRACE("Server: genNtpReq: NTP packet without AEAD EF  nonce ID:  {}", getItemID(nonce));
	LOG_TRACE("Server: genNtpReq: NTP packet without AEAD EF  nonce SIZE:  {}", nonce.size());

	LOG_TRACE("Server: genNtpReq: NTP packet without AEAD EF  setCiphertext ID:  {}", getItemID(encryptedContent));
	LOG_TRACE("Server: genNtpReq: NTP packet without AEAD EF  setCiphertext SIZE:  {}", encryptedContent.size());

	LOG_TRACE("Server: genNtpReq: NTP packet without AEAD EF  AD ID:  {}", getItemID(receivedNtpRequest.getSerializedPackage()));
	LOG_TRACE("Server: genNtpReq: NTP packet without AEAD EF  AD SIZE:  {}", receivedNtpRequest.getSerializedPackage().size());


	if (aead.decrypt() == false)
	{
		LOG_ERROR("NtsServerNtp::verifyNtpMessage: decryption failed");
		status = false;
	}

	// save decrypted ext fields (if available)
	m_decryptedExtFields = aead.getPlaintext();


	return status;
}

std::vector<unsigned char> NtsServerNtp::createCookie()
{
	// get master AEAD Algo, Key and ID
	AeadAlgorithm masterAeadAlgo = getServerAeadAlgo();
	unsigned int masterAeadKeyId = getServerAeadKeyId();
	std::vector<unsigned char> masterAeadKey = getServerAeadKey(masterAeadKeyId);

	// create plain cookie
	CookieContent plainCookie(m_receivedAeadAlgorithm, m_receivedS2CKey, m_receivedC2SKey);

	// create new nonce
	std::vector<unsigned char> nonce = getRandomNumber(NONCE_SIZE);
	if (nonce.size() == 0)
	{
		LOG_ERROR("createCookie: nonce is zero)");
	}

	// encrypt plain cookie
	AEAD aead;
	std::vector<unsigned char> encCookieContent; //aead out
	if (aead.isSupported(masterAeadAlgo) == true)
	{
		aead.setAeadAlgorithm(masterAeadAlgo);
		aead.setKey(masterAeadKey);
		aead.setPlaintext(plainCookie.getSerializedData());
		aead.setNonce(nonce);
		if (aead.encrypt() == false)
		{
			LOG_ERROR("createCookie: ENCRYPTION failed!");
		}
		else
		{
			encCookieContent = aead.getCiphertext();
		}
	}
	else
	{
		LOG_ERROR("createCookie: ENCRYPTION failed: aead algo not supported");
	}

	// create final cookie
	Cookie cookie(masterAeadKeyId, nonce, encCookieContent);
	if (cookie.isValid() == false)
	{
		LOG_ERROR("createCookie: cookie is invald");
	}

	return cookie.getSerializedData();
}




AeadAlgorithm NtsServerNtp::getServerAeadAlgo() const
{
	return SERVER_AEAD_ALGO;
}

std::vector<unsigned char> NtsServerNtp::getServerAeadKey(unsigned short keyId) const
{
	if (m_masterKey == nullptr)
	{
		LOG_ERROR("NtsServerNtp::getServerAeadKey(): m_masterKey == nullptr");
		return std::vector<unsigned char>();
	}
	return m_masterKey->getKey(keyId);
}

unsigned short NtsServerNtp::getServerAeadKeyId() const
{
	if (m_masterKey == nullptr)
	{
		LOG_ERROR("NtsServerNtp::getServerAeadKeyId(): m_masterKey == nullptr");
		return 0;
	}
	return m_masterKey->getCurrentKeyId();
}

