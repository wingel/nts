#include "../Logger/Logger.h" // muss hier an erster stelle stehen! 
#include "NtsServer.h"
#include "../cryptoFunctions.h"

NtsServer::NtsServer(NtsSettings const& settings)
	:
	m_ioService(),
	m_tlsThread(),
	m_ntsKeyEstablishment(),
	m_masterKey(),
	m_ntpCommunication(),
	m_settings(settings)
{
}

NtsServer::~NtsServer()
{
	stopTlsServer();
}


void NtsServer::initialization()
{
	// generate MasterKey
	m_masterKey.setRotateInterval(m_settings.server.keyRotationIntervalSeconds);
	m_masterKey.setMaxRotations(m_settings.server.keyRotationHistory);
	m_masterKey.setKeyLengthInBytes(determineKeySize(m_settings.server.aeadAlgoForMasterKey));

	HKDF hkdf;
	hkdf.setMessageDigestAlgorithm(MessageDigestAlgo::SHA512); // todo: mach mal richtig. ....sich 
	hkdf.setKeyOutputLength(determineKeySize(m_settings.server.aeadAlgoForMasterKey));
	m_masterKey.setHkdf(hkdf);

	m_masterKey.forceRotate();
	m_masterKey.startAutoRotate();





	m_ntsKeyEstablishment.setMasterKey(&m_masterKey);
	m_ntpCommunication.setMasterKey(&m_masterKey);

	// start async TLS server
	if (startTlsServer(m_settings.server.tlsPort) == false)
	{
		LOG_WARN("NtsServer::initialization(): TLS Server runs already");
	}
}


void NtsServer::shutdown()
{
	m_masterKey.stopAutoRotate();
	m_masterKey.clearAllKeys();
	stopTlsServer();
}


void NtsServer::generateNtpResponse(NtpNtsInterface & receivedNtpRequest, NtpNtsInterface & newNtpResponse)
{
	m_ntpCommunication.generateNtpResponse(receivedNtpRequest, newNtpResponse);
}


bool NtsServer::startTlsServer(int port)
{
	if (m_tlsThread)
	{
		return false;
	}

	m_tlsServer.reset(new TlsServer(m_ioService, port, m_ntsKeyEstablishment, m_settings));
	m_tlsThread.reset(new boost::thread(boost::bind(&boost::asio::io_service::run, &m_ioService)));
	return true;
}


bool NtsServer::stopTlsServer()
{
	if (!m_tlsThread)
	{
		return false;
	}

	m_ioService.stop();
	m_tlsThread->join();
	m_ioService.reset();
	m_tlsThread.reset();
	return true;
}


