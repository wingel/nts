#include "../BaseConfig.h"
#include "TlsServerSession.h"
#include "../Logger/Logger.h"
#include <boost/bind.hpp>



TlsServerSession::TlsServerSession(boost::asio::io_service& io_service, boost::asio::ssl::context& context, NtsServerKe ntsServerKe)
	:
	m_isFinished(false),
	m_socket(io_service, context),
	m_context(context),
	m_dataBuffer(TLS_BUFFER_SIZE),
	// TODO: alpn auslagern (NtsConfig.h?)
	m_alpnList({7, 'n', 't', 's', 'k', 'e', '/', '1'}), // ALPN-Eintrag: {Stringlänge (max 255 byte) || String} --> Verkettung: ALPN-Eintrag || ALPN-Eintrag || ...
	m_ntsServerKe(ntsServerKe)
{
}


ssl_socket::lowest_layer_type& TlsServerSession::getSocket()
{
	return m_socket.lowest_layer();
}

void TlsServerSession::startup()
{
	m_socket.async_handshake(boost::asio::ssl::stream_base::server, boost::bind(&TlsServerSession::handshakeHandler, this, boost::asio::placeholders::error));
	SSL_CTX_set_alpn_select_cb(m_context.native_handle(), TlsServerSession::alpnSelectCallback, this);
}

void TlsServerSession::handshakeHandler(const boost::system::error_code& error)
{
	if (!error)
	{
		LOG_DEBUG("TLS server: handshake successful");
		m_socket.async_read_some(boost::asio::buffer(m_dataBuffer.data(), m_dataBuffer.size()),
								 boost::bind(&TlsServerSession::readHandler, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred)
		);
	}
	else
	{
		//LOG_ERROR("ServerSession ({}): handshakeHandler error msg: {}", this, error.message());
		setFinished(true);
	}
}


void TlsServerSession::readHandler(const boost::system::error_code& error, size_t bytesTransferred)
{
	if (!error)
	{
		LOG_DEBUG("TLS server: read application data --> successful");
		std::vector<unsigned char> request(m_dataBuffer.data(), m_dataBuffer.data() + bytesTransferred);
		std::vector<unsigned char> response = m_ntsServerKe.processTlsRequest(request, *m_socket.native_handle());

		boost::asio::async_write(m_socket,
								 boost::asio::buffer(response, response.size()),
								 boost::bind(&TlsServerSession::writeHandler, this, boost::asio::placeholders::error));


		LOG_DEBUG("TLS server: request received --> TLS payload size:  {} bytes", request.size());
		LOG_DEBUG("TLS server: response created --> TLS payload size:  {} bytes", response.size());
	}
	else
	{
		//LOG_ERROR("ServerSession ({}): readHandler error msg: {}", this, error.message());
		setFinished(true);
	}
}


void TlsServerSession::writeHandler(const boost::system::error_code& error)
{
	if (!error)
	{
		LOG_DEBUG("TLS server: send application data --> successful");
		m_socket.async_shutdown(boost::bind(&TlsServerSession::shutdownHandler, this, boost::asio::placeholders::error));
	}
	else
	{
		//LOG_ERROR("ServerSession ({}): writeHandler error msg: {}", this, error.message());
		setFinished(true);
	}
}


void TlsServerSession::shutdownHandler(const boost::system::error_code& error)
{
	if (error == boost::asio::ssl::error::stream_truncated)
	{
		LOG_DEBUG("TLS server: shutdown connection successful");
		// http://stackoverflow.com/questions/25587403/boost-asio-ssl-async-shutdown-always-finishes-with-an-error
		// no error
	}
	else
	{
		//LOG_ERROR("TLS server: shutdown failed: {}", error.message());
	}
	setFinished(true);
}



void TlsServerSession::setFinished(bool state)
{
	m_isFinished = state;
}

bool TlsServerSession::isFinished() const
{
	return m_isFinished;
}


int TlsServerSession::alpnSelectCallback(SSL *ssl,
										 const unsigned char **out,
										 unsigned char *outlen,
										 const unsigned char *in,		// vector in protocol-list format
										 unsigned int inlen,			// vector in protocol-list format
										 void *arg)					// beliebige daten (hier 'this')
{
	if (arg == nullptr)
	{
		return SSL_TLSEXT_ERR_NOACK; // Server bietet keine ALPN an
	}

	TlsServerSession* serverInstance = static_cast<TlsServerSession*>(arg);

	// INFO: 'out' und 'outlen' ZEIGEN bei Erfolg auf den gefundenen Eintrag in der Server Liste (KEINE Kopie)!!!!
	// daher nicht mit variblen im stack arbeiten
	int status = SSL_select_next_proto(const_cast<unsigned char **>(out),				// selected ALPN
									   outlen,											// ALPN entry size
									   serverInstance->m_alpnList.data(),			// list with supported ALPNs of the server 
									   static_cast<unsigned int>(serverInstance->m_alpnList.size()),			// list size (server)
									   in,												// received list with supported ALPNs of the client 
									   inlen);											// list size (client)

	switch (status)
	{
	case OPENSSL_NPN_NEGOTIATED:
		LOG_DEBUG("TLS server: received ALPN supported;  SSL_select_next_proto() successful");
		return SSL_TLSEXT_ERR_OK;

	case OPENSSL_NPN_UNSUPPORTED:
		LOG_ERROR("TLS server: ALPN unsupported");
		return SSL_TLSEXT_ERR_NOACK; // abort tls handshake

	case OPENSSL_NPN_NO_OVERLAP:
		LOG_ERROR("TLS server: ALPN no overlap");
		return SSL_TLSEXT_ERR_ALERT_FATAL; // abort tls handshake
	}
	return 0;
}

