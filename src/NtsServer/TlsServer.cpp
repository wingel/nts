#include "../BaseConfig.h"
#include "TlsServer.h"
#include "../Logger/Logger.h"


TlsServer::TlsServer(boost::asio::io_service& ioService, unsigned short port, NtsServerKe const &ntsServerKe, NtsSettings const& settings)
	:
	m_ioService(ioService),
	m_acceptor(ioService, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port)), // TODO: IPv6 support?
	m_context(boost::asio::ssl::context::tls_server),
	m_ntsServerKe(ntsServerKe),
	m_settings(settings)
{
	
	m_context.set_options(boost::asio::ssl::context::default_workarounds
						 | boost::asio::ssl::context::no_sslv2
						 | boost::asio::ssl::context::no_sslv3
						 | boost::asio::ssl::context::no_tlsv1
						 | boost::asio::ssl::context::no_tlsv1_1
						// | boost::asio::ssl::context::no_tlsv1_2
						 | boost::asio::ssl::context::single_dh_use);

	//SSL_CTX_set_cipher_list(m_context.native_handle(), "ECDH+AESGCM:DH+AESGCM:ECDH+AES256:DH+AES256:ECDH+AES128:DH+AES:RSA+AESGCM:RSA+AES:!aNULL:!MD5:!DSS");
	//SSL_CTX_clear_options(m_context.native_handle(), SSL_OP_NO_SSLv2)
	//SSL_CTX_set_cipher_list(m_context.native_handle(), "TLSv1:SSLv3:SSLv2");
	// todo debug infos
	m_context.use_certificate_chain_file(m_settings.server.serverCertChainFile);
	m_context.use_private_key_file(m_settings.server.serverPrivateKey, boost::asio::ssl::context::pem);

	// neue ServerSession anlegen und eingehende Verbindungen damit verknüpfen
	m_sessions.emplace_back(std::make_unique<TlsServerSession>(m_ioService, m_context, m_ntsServerKe));
	m_acceptor.async_accept(m_sessions.back()->getSocket(), [this](const auto& error)
	{
		acceptHandler(*m_sessions.back(), error);
	});

	LOG_DEBUG("TLS-Server: TLS channel opened (Port: {})", port);
}


void TlsServer::acceptHandler(TlsServerSession & newSession, const boost::system::error_code& error)
{
	if (!error)
	{
		newSession.startup();

		// delete all finished sessions
		m_sessions.erase(std::remove_if(m_sessions.begin(), m_sessions.end(), [](const auto& session)
		{
			return session->isFinished();
		}), m_sessions.end());

		LOG_DEBUG("TLS-Server: number of active sessions: {} (limit: {})", m_sessions.size(), c_maxSessions);

		if (m_sessions.size() >= c_maxSessions)
		{
			LOG_DEBUG("TLS-Server: upper TLS session limit reached. Current limit: {}", m_sessions.size());
			return;
		}

		m_sessions.emplace_back(std::make_unique<TlsServerSession>(m_ioService, m_context, m_ntsServerKe));
		m_acceptor.async_accept(m_sessions.back()->getSocket(), [this](const auto& error)
		{
			acceptHandler(*m_sessions.back(), error);
		});
	}
	else
	{
		LOG_ERROR("TLS-Server: new connection failed: {}", error.message());
	}
}

