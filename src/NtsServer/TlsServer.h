/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#pragma once
#include "NtsServerKe.h"
#include "TlsServerSession.h"
#include "../NtsSettings.h"

class TlsServer
{
public:
	TlsServer(boost::asio::io_service& ioService, unsigned short port, NtsServerKe const &ntsServerKe, NtsSettings const& settings);
	void acceptHandler(TlsServerSession & newSession, const boost::system::error_code& error);


private:
	const unsigned int c_maxSessions = 10;
	std::vector<std::unique_ptr<TlsServerSession>> m_sessions;

	boost::asio::io_service & m_ioService;
	boost::asio::ip::tcp::acceptor m_acceptor;
	boost::asio::ssl::context m_context;
	NtsServerKe const & m_ntsServerKe;
	const NtsSettings & m_settings;
};
