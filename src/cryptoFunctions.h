/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#pragma once

#include <vector>
#include <string>
#include "NtsConfig.h"

std::vector<unsigned char> getRandomNumber(unsigned int num);
void getRandomNumber(unsigned char *data, unsigned int num);
std::vector<unsigned char> getFingerprint(std::vector<unsigned char> const &data, MessageDigestAlgo msgDigest);

std::string getItemID(std::vector<unsigned char> const &data, unsigned int size = 3);

void secureErase(std::vector<unsigned char> &data); // overwrite with random values and resize to 0    //todo -> rename secureClear()?
unsigned int determineKeySize(AeadAlgorithm aeadAlgorithm);


AeadAlgorithm getAeadAlgorithmFromStr(std::string const & algorithmStr);
MessageDigestAlgo getMdAlgorithmFromStr(std::string const & algorithmStr);
NextProtocol getNextProtocolFromStr(std::string const & protocolStr);

std::string getAeadAlgorithmStr(AeadAlgorithm const & AeadAlgorithm);
std::string getMdAlgorithmStr(MessageDigestAlgo const & MessageDigestAlgo);
std::string getNextProtocolStr(NextProtocol const & NextProtocol);

bool isSupported(AeadAlgorithm algorithm);
bool isSupported(MessageDigestAlgo algorithm);
bool isSupported(NextProtocol protocol);
