#include "generalFunctions.h"
#include <iomanip>
#include <sstream>


void SetWindow(int Width, int Height, int WidthBuffer, int HeightBuffer)
{

#ifdef __linux__ 
	// ...
#elif _WIN32
	_COORD coord;
	coord.X = WidthBuffer;
	coord.Y = HeightBuffer;

	_SMALL_RECT Rect;
	Rect.Top = 0;
	Rect.Left = 0;
	Rect.Bottom = Height - 1;
	Rect.Right = Width - 1;

	HANDLE Handle = GetStdHandle(STD_OUTPUT_HANDLE);      // Get Handle 
	SetConsoleScreenBufferSize(Handle, coord);            // Set Buffer Size 
	SetConsoleWindowInfo(Handle, TRUE, &Rect);            // Set Window Size 
#else
	// ...
#endif
}



// convert binary data to text (hex view)
std::string binToHexStr(unsigned char const *buf, int num, std::string pairDelimiter, int newLineNum, bool uppercase)
{
	std::stringstream stream;

	// clear strings
	stream.str("");

	// check parameters
	if (buf == nullptr || num <= 0)
	{
		return "";
	}

	if (newLineNum < 0)
	{
		newLineNum = 0;
	}

	// convert binary data to string
	for (int i = 0; i <= num - 1; i++)
	{
		if (uppercase == true)
		{
			stream << std::setw(2) << std::setfill('0') << std::uppercase << std::hex << (int) buf [i] << std::flush;
		}
		else
		{
			stream << std::setw(2) << std::setfill('0') << std::hex << (int) buf [i] << std::flush;
		}


		// add delimiter
		if (i < num - 1)
		{
			stream << pairDelimiter.c_str() << std::flush;
		}

		// add word wrap
		if (newLineNum != 0)
		{
			if ((i + 1) % newLineNum == 0)
			{
				stream << std::endl << std::flush;
			}
		}
	}

	// return hex string
	return stream.str();
}



// convert binary data to text (hex view)
std::string binToHexStr(std::vector<unsigned char> const &data, std::string pairDelimiter, int newLineNum, bool uppercase)
{
	return binToHexStr(data.data(), static_cast<int>(data.size()), pairDelimiter, newLineNum, uppercase);
}


//
//std::string currentDateTime(std::string format)
//{
//	time_t     now = time(0);
//	struct tm  tstruct;
//	char       buf [80];
//	tstruct = *localtime(&now);
//
//	if (format == "")
//	{
//		format = "%d.%m.%Y  %H:%M:%S";
//	}
//	strftime(buf, sizeof(buf), format.c_str(), &tstruct);
//
//	return buf;
//}





///*
//* parameter "string" wird direkt manipuliert
//* case sensitive
//*/
//// ret: Anzahl der Ersetzungen
//int replaceString(std::string &string, std::string const &search, std::string const &replace)
//{
//	std::size_t pos = 0;
//	int replaceCount = 0;
//
//	while ((pos = string.find(search, pos)) != std::string::npos)
//	{
//		string.replace(pos, search.length(), replace);
//		pos += replace.length();
//		replaceCount++;
//	}
//
//	return replaceCount;
//}



/*
* [in] string
* [out] tokens
* [in] delimiters (z.B. ",; " --> 3 delimiters)
*/
void parseString(const std::string &string, std::vector<std::string> &tokens, const std::string &delimiter)
{

	tokens.clear();
	size_t lastPos = string.find_first_not_of(delimiter, 0); 		// skip delimiters at beginning
	size_t currentPos = string.find_first_of(delimiter, lastPos);	// find first non-delimiter
	while (std::string::npos != currentPos || std::string::npos != lastPos)
	{
		tokens.push_back(string.substr(lastPos, currentPos - lastPos));	// add a found token to the vector
		lastPos = string.find_first_not_of(delimiter, currentPos);		// skip delimiter
		currentPos = string.find_first_of(delimiter, lastPos);			// find next non-delimiter
	}
	return;
}

