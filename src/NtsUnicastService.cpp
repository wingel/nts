#include "NtsUnicastService.h"
#include "NtsUnicastServiceImpl.h"

NtsUnicastService::NtsUnicastService() // todo: benötigt?  hierfür eine zusätzliche initialization()-Methode definieren
	: 
	m_ntsUnicastServiceImpl(new NtsUnicastServiceImpl())
{
//	Logger::getInstance().init(); // preconfig (optional)
}

NtsUnicastService::~NtsUnicastService() = default;


void NtsUnicastService::initialization(std::string const &globalConfigFile)
{
//	Logger::getInstance().init(); // preconfig (optional)
	m_ntsUnicastServiceImpl->initialization(globalConfigFile);
	return;
}


void NtsUnicastService::reset()
{
	m_ntsUnicastServiceImpl->reset();
	return;
}


void NtsUnicastService::shutdown()
{
	m_ntsUnicastServiceImpl->shutdown();
	return;
}


void NtsUnicastService::processingNtpMessage(NtpNtsInterface &receivedPackage, NtpNtsInterface &newResponsePackage)
{
	m_ntsUnicastServiceImpl->processingNtpMessage(receivedPackage, newResponsePackage);
	return;
}


void NtsUnicastService::processingReceivedNtpMessage(NtpNtsInterface &receivedPackage)
{
	m_ntsUnicastServiceImpl->processingReceivedNtpMessage(receivedPackage);
	return;
}


void NtsUnicastService::createRequestMessage(NtpNtsInterface &newResponsePackage)
{
	m_ntsUnicastServiceImpl->createRequestMessage(newResponsePackage);
	return;
}


bool NtsUnicastService::isInit()
{
	return m_ntsUnicastServiceImpl->isInit();
}


bool NtsUnicastService::isClientMode()
{
	return m_ntsUnicastServiceImpl->isClientMode();
}


bool NtsUnicastService::isServerMode()
{
	return m_ntsUnicastServiceImpl->isServerMode();
}
