#include "NtpNtsInterfaceDemo.h"
#include <time.h>
#include "cryptoFunctions.h"


NtpNtsInterfaceDemo::NtpNtsInterfaceDemo():
	m_transmitTimestamp(0),
	m_originTimestamp(0),
	m_useMessageForTimeSync(0),
	m_sendResponseMessage(0),
	m_generateResponseMessage(0)
{
}


NtpNtsInterfaceDemo::~NtpNtsInterfaceDemo()
{
}

double NtpNtsInterfaceDemo::getTransmitTimestamp() const
{
	return m_transmitTimestamp;
}


double NtpNtsInterfaceDemo::getOriginTimestamp() const
{
	return m_originTimestamp;
}

std::string NtpNtsInterfaceDemo::getIpAddress() const
{
	return m_IPaddress;
}

void NtpNtsInterfaceDemo::updateTransmitTimestamp()
{
	m_randomNtpHeaderData = getRandomNumber(32); // 32 Bytes random data (for NTP offset, delay, ..)
	m_transmitTimestamp = static_cast<double>(time(nullptr));
}


unsigned int NtpNtsInterfaceDemo::getExtensionFieldCount() const
{
	return static_cast<unsigned int>(m_extensionFieldValues.size());
}

unsigned short NtpNtsInterfaceDemo::getExtensionFieldType(unsigned int index) const
{
	return m_extensionFieldTypes.at(index);
}

std::vector<unsigned char> NtpNtsInterfaceDemo::getExtensionFieldValue(unsigned int index) const
{
	return m_extensionFieldValues.at(index);
}

void NtpNtsInterfaceDemo::addExtensionField(unsigned short fieldType, std::vector<unsigned char> value)
{
	// add padding (on demand)
	size_t size = 2 + 2 + value.size(); // Ext Field Header: Type + Length
	size_t padding = size % 4;
	for (size_t i = 0; i < padding; i++)
	{
		value.push_back(0);
	}

	size = value.size();
	size_t minSize = 12; // min. Ext Field size: 16 Bytes (4 Bytes Header + min. 12 Bytes Value+Padding)

	if (size < minSize) 
	{
		size_t diff = minSize - size;

		for (size_t i = 1; i <= diff; i++)
		{
			value.push_back(0);
		}
	}

	m_extensionFieldTypes.push_back(fieldType);
	m_extensionFieldValues.push_back(value);



	//std::cout << "--> ExtField (add): Value: " << value.size() << std::endl;
	//std::cout << "--> ExtField (add): Padding: " << padding << std::endl;
	//std::cout << "--> ExtField (add): Header + Value + Padding: " << (size + padding) << std::endl;
	//std::cout << "--> ExtField (add): final value size: " << value.size() << std::endl;
}

void NtpNtsInterfaceDemo::delExtensionField(unsigned int index)
{
	if (index <= static_cast<unsigned int>(m_extensionFieldValues.size()) - 1)
	{
		m_extensionFieldValues.erase(m_extensionFieldValues.begin() + index);
		m_extensionFieldTypes.erase(m_extensionFieldTypes.begin() + index);
	}
}

std::vector<unsigned char> NtpNtsInterfaceDemo::getSerializedPackage() const
{
	std::vector<unsigned char> m_serialized;

	std::vector<unsigned char> transmitTimestamp;
	std::vector<unsigned char> originTimestamp;

	double transTime = m_transmitTimestamp;
	double origTime = m_originTimestamp;

	transmitTimestamp.assign(reinterpret_cast<unsigned char*>(&transTime), reinterpret_cast<unsigned char*>(&transTime) + sizeof(transTime));
	originTimestamp.assign(reinterpret_cast<unsigned char*>(&origTime), reinterpret_cast<unsigned char*>(&origTime) + sizeof(origTime));

	size_t size = 0;
	size = transmitTimestamp.size();
	size = size + originTimestamp.size();
	size = size + m_randomNtpHeaderData.size();
	for (std::vector<unsigned char>::size_type i = 1; i <= m_extensionFieldTypes.size(); i++)
	{
		size = size + sizeof(m_extensionFieldTypes.at(i-1));
		size = size + sizeof(unsigned char); //2 bytes for length field
		size = size + m_extensionFieldValues.at(i-1).size();
	}

	m_serialized.reserve(size); // preallocate memory

	m_serialized.insert(m_serialized.end(), m_randomNtpHeaderData.begin(), m_randomNtpHeaderData.end());
	m_serialized.insert(m_serialized.end(), transmitTimestamp.begin(), transmitTimestamp.end());
	m_serialized.insert(m_serialized.end(), originTimestamp.begin(), originTimestamp.end());
	

	for (std::vector<unsigned char>::size_type i = 1; i <= m_extensionFieldTypes.size(); i++)
	{
		std::vector<unsigned char> fieldTypeVec;
		std::vector<unsigned char> fieldLengthVec;

		unsigned short fieldType = m_extensionFieldTypes.at(i-1);
		unsigned short length = static_cast<unsigned short>(m_extensionFieldValues.at(i-1).size());

		fieldTypeVec.assign(reinterpret_cast<unsigned char*>(&fieldType), reinterpret_cast<unsigned char*>(&fieldType) + sizeof(fieldType));
		fieldLengthVec.assign(reinterpret_cast<unsigned char*>(&length), reinterpret_cast<unsigned char*>(&length) + sizeof(length));

		m_serialized.insert(m_serialized.end(),fieldTypeVec.begin(), fieldTypeVec.end());
		m_serialized.insert(m_serialized.end(), fieldLengthVec.begin(), fieldLengthVec.end());
		m_serialized.insert(m_serialized.end(), m_extensionFieldValues.at(i-1).begin(), m_extensionFieldValues.at(i-1).end());
	}

	return m_serialized;
}




void NtpNtsInterfaceDemo::setTransmitTimestamp(double timestamp)
{
	this->m_transmitTimestamp = timestamp;
}

void NtpNtsInterfaceDemo::setOriginTimestamp(double timestamp)
{
	this->m_originTimestamp = timestamp;
}

void NtpNtsInterfaceDemo::setIPaddress(std::string const &ip)
{
	this->m_IPaddress = ip;
}


bool NtpNtsInterfaceDemo::isUseMessageForTimeSync() const
{
	return getUseMessageForTimeSync();
}

bool NtpNtsInterfaceDemo::isSendResponseMessage() const
{
	return getSendResponseMessage();
}



void NtpNtsInterfaceDemo::createNewResponseMessage(std::string recipientIP)
{
	m_transmitTimestamp = 0;
	m_originTimestamp = 0;
	m_IPaddress.clear();

	m_extensionFieldTypes.clear();
	m_extensionFieldValues.clear();

	setUseMessageForTimeSync(false);
	setSendResponseMessage(false);


	m_originTimestamp = static_cast<double>(time(nullptr));
	setIPaddress(recipientIP);
	updateTransmitTimestamp();
	return;
}


void NtpNtsInterfaceDemo::createNewResponseMessage(NtpNtsInterface const &receivedPackage)
{
	m_transmitTimestamp = 0;
	m_originTimestamp = 0;
	m_IPaddress.clear();

	m_extensionFieldTypes.clear();
	m_extensionFieldValues.clear();

	this->setUseMessageForTimeSync(false);
	this->setSendResponseMessage(false);

	setOriginTimestamp(receivedPackage.getTransmitTimestamp());
	setIPaddress(receivedPackage.getIpAddress());
	updateTransmitTimestamp();
	return;

}

void NtpNtsInterfaceDemo::setUseMessageForTimeSync(bool state)
{
	m_useMessageForTimeSync = state;
}

bool NtpNtsInterfaceDemo::getUseMessageForTimeSync() const
{
	return m_useMessageForTimeSync;
}

void NtpNtsInterfaceDemo::setSendResponseMessage(bool state)
{
	m_sendResponseMessage = state;
}

bool NtpNtsInterfaceDemo::getSendResponseMessage() const
{
	return m_sendResponseMessage;
}

void NtpNtsInterfaceDemo::setGenerateResponseMessage(bool state)
{
	m_generateResponseMessage = state;
}

bool NtpNtsInterfaceDemo::getGenerateResponseMessage() const
{
	return m_generateResponseMessage;
}
// END OF FILE
