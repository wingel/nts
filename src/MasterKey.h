/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#pragma once

#include <mutex>
#include <thread>
#include <vector>
#include "HKDF.h"

/*
   Key generator with automatic key rotation and key derivation support.
   
   Short information:
   ----------------------------
   This class uses a cryptographically secure pseudo-random number generator (CSPRNG) and supports
   Keyed-Hash Message Authentication Code (HMAC) for key generations. This generation process 
   includes the secret key, as well as a public and unique key identifier. The number of rotations
   (amount of keys that can be stored at the same time) is limited to 65534 (2^16 - 2). If key 
   material ('Input Keying Material') is transferred via the HMAC parameters and the current list 
   contains no further keys, the key material is used as a seed and derived directly (HKDF operation).
   The rotation happens automatically (by a separate thread) and can additionally be forced manually.
*/

class MasterKey
{
public:
	MasterKey();
	MasterKey(unsigned long rotateInterval, unsigned long maxRotations, unsigned long keyLength);
	MasterKey(unsigned long rotateInterval, unsigned long maxRotations, HKDF const & hkdfSettings);
	MasterKey(MasterKey const &) = delete;            // prevent copying
	MasterKey &operator=(MasterKey const &) = delete; // prevent copying
	~MasterKey();

	// rotation interval [s]
	void setRotateInterval(unsigned int seconds);
	unsigned long getRotateInterval() const;

	// max. rotations
	void setMaxRotations(unsigned int maxRotations);
	unsigned long getMaxRotations() const;

	// key length
	void setKeyLengthInBytes(size_t keyLength);
	size_t getKeyLengthInBytes() const;

	// HKDF function
	void setHkdf(HKDF const & hkdfSettings);
	bool isHkdfEnabled() const;
	void removeHkdf();

	// key rotation
	bool startAutoRotate();
	bool stopAutoRotate();
	void forceRotate();

	// keys
	unsigned short getCurrentKeyId();
	std::vector<unsigned char> getKey(unsigned short keyId);
	bool isKeyAvailable(unsigned short keyId);
	unsigned int countKeys() const;
	void clearAllKeys();


private:

	// misc
	void eraseKey(unsigned int keyListIndex);
	std::vector<unsigned char> generateNewKey();
	unsigned short generateNewKeyID();
	void keyRotate();
	void timerThread();

	const unsigned int c_tickLength_ms = 1;  // TODO: so lange dauert es auch, bis dieser Thread beendet wird. --> auf 250 ms setzen?

	unsigned long long m_counter;
	unsigned long long m_counterLimit;
	unsigned int m_rotateIntervalSec;
	unsigned int m_maxRotations;
	size_t m_keyLengthInBytes;
	bool m_quitThread;
	bool m_useHkdf;

	HKDF m_hkdf;
	std::mutex m_mutex;
	std::unique_ptr<std::thread> m_timerThread;
	std::vector <std::pair<unsigned short, std::vector<unsigned char>>> m_keyList;  // pair: <keyID, key>
};

