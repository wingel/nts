#include "CookieContent.h"
#include "../Logger/Logger.h"
#include "../generalFunctions.h"

CookieContent::CookieContent()
	:
	m_aeadAlgorithm(0)
{}


/**
* @brief	Creates the content of a cookie as a tuple of the passed parameters.
*           The parameters are copied here.
*/
CookieContent::CookieContent(AeadAlgorithm aeadAlgorithm, std::vector<unsigned char> const & s2cKey, std::vector<unsigned char> const & c2sKey)
{
    printf("%s:\n", __PRETTY_FUNCTION__);
    printf("c2s %u %s\n", c2sKey.size(), binToHexStr(c2sKey, "", 1024, true).c_str());
    printf("s2c %u %s\n", s2cKey.size(), binToHexStr(s2cKey, "", 1024, true).c_str());

	this->setAeadAlgorithm(aeadAlgorithm);
	this->setS2CKey(s2cKey);
	this->setC2SKey(c2sKey);
}


/**
* @brief	Creates the content of a cookie as a tuple of the passed parameters.
*           The parameters are copied here.
*/
CookieContent::CookieContent(unsigned short aeadAlgorithm, std::vector<unsigned char> const & s2cKey, std::vector<unsigned char> const & c2sKey)
{
    printf("%s:\n", __PRETTY_FUNCTION__);
    printf("c2s %u %s\n", c2sKey.size(), binToHexStr(c2sKey, "", 1024, true).c_str());
    printf("s2c %u %s\n", s2cKey.size(), binToHexStr(s2cKey, "", 1024, true).c_str());

	this->setAeadAlgorithm(aeadAlgorithm);
	this->setS2CKey(s2cKey);
	this->setC2SKey(c2sKey);
}


/**
* @brief	Creates the content of the Cookie by parsing a binary data stream.
*/
CookieContent::CookieContent(std::vector<unsigned char> const & serializedData)
{
	this->fromSerializedData(serializedData);
}


unsigned short CookieContent::getAeadAlgorithmVal() const
{
	return m_aeadAlgorithm;
}


AeadAlgorithm CookieContent::getAeadAlgorithm() const
{
	return static_cast<AeadAlgorithm>(m_aeadAlgorithm);
}


void CookieContent::setAeadAlgorithm(unsigned short aeadAlgorithm)
{
	this->m_aeadAlgorithm = aeadAlgorithm;
}


void CookieContent::setAeadAlgorithm(AeadAlgorithm aeadAlgorithm)
{
	this->m_aeadAlgorithm = static_cast<unsigned short>(aeadAlgorithm);
}


unsigned short CookieContent::getS2CKeyLength() const
{
	return static_cast<unsigned short>(m_S2CKey.size());
}


const std::vector<unsigned char>& CookieContent::getS2CKey() const
{
	return m_S2CKey;
}


/**
* @brief	Sets the S2C (Server-to-Client) key as a copy of the passed parameter.
*/
void CookieContent::setS2CKey(std::vector<unsigned char> const & s2cKey)
{
	this->m_S2CKey = s2cKey;
}


unsigned short CookieContent::getC2SKeyLength() const
{
	return static_cast<unsigned short>(m_C2SKey.size());
}


const std::vector<unsigned char>& CookieContent::getC2SKey() const
{
	return m_C2SKey;
}


/**
* @brief	Sets the C2S (Client-to-Server) key as a copy of the passed parameter.
*/
void CookieContent::setC2SKey(std::vector<unsigned char> const & c2sKey)
{
	this->m_C2SKey = c2sKey;
}


/**
* @brief	Serializes the Cookie content into a binary data stream.
*/
std::vector<unsigned char> CookieContent::getSerializedData() const
{
	// write AEAD algorithm
	std::vector<unsigned char> serializedData(2);
	serializedData.at(0) |= (m_aeadAlgorithm >> 8) & 0xFF;
	serializedData.at(1) |= m_aeadAlgorithm & 0xFF;

	// write S2C key
	serializedData.insert(serializedData.end(), m_S2CKey.begin(), m_S2CKey.end());

	// write C2S key
	serializedData.insert(serializedData.end(), m_C2SKey.begin(), m_C2SKey.end());

	return serializedData;
}


/**
* @brief	Parses the given binary stream and sets the values of this CookieContent object.
*/
void CookieContent::fromSerializedData(std::vector<unsigned char> const & serializedData)
{
	if (serializedData.size() < 4) // 2 bytes (AEAD algorithm ID) + 1 byte (min size S2C key) + 1 byte (min size C2S key)
	{
		LOG_ERROR("invalid size of the binary data");
		return;
	}

	// parse binary data
	unsigned short aeadAlgorithm = ((serializedData.at(0) & 0xFF) << 8) | serializedData.at(1);
	std::vector<unsigned char> KeyPair(serializedData.begin() + 2, serializedData.end());

	std::size_t const halfSize = KeyPair.size() / 2;
	std::vector<unsigned char> S2C(KeyPair.begin(), KeyPair.begin() + halfSize);
	std::vector<unsigned char> C2S(KeyPair.begin() + halfSize, KeyPair.end());

	// check values
	if (S2C.size() != C2S.size())
	{
		LOG_ERROR("the length of the C2S/S2C keys are different: S2C: {} bytes; C2S: {} bytes", S2C.size(), C2S.size());
		return;
	}

	// all is fine -> save data
	m_aeadAlgorithm = aeadAlgorithm;
	m_S2CKey = std::move(S2C);
	m_C2SKey = std::move(C2S);
}


/**
* @brief	Checks if the content is complete and valid.
*/
bool CookieContent::isValid() // TODO: WARN statt ERROR?;  sollte so eine Funktion loggen?
{
	bool status = true;

	if (m_aeadAlgorithm == 0)  // undefined
	{
		LOG_ERROR("AEAD algorithm is not defined");
		status = false;
	}

	if (m_C2SKey.size() != m_S2CKey.size())
	{
		LOG_ERROR("the length of the C2S/S2C keys are different");
		status = false;
	}

	if (m_C2SKey.size() == 0)
	{
		LOG_ERROR("the length of the C2S key is zero");
		status = false;
	}

	if (m_S2CKey.size() == 0)
	{
		LOG_ERROR("the length of the S2C key is zero");
		status = false;
	}
	return status;
}


/**
* @brief	Deletes the content.
*/
void CookieContent::clear()
{
	m_aeadAlgorithm = 0;
	m_S2CKey.clear();
	m_C2SKey.clear();
}
