#include "Cookie.h"
#include "../NtsConfig.h"
#include "../Logger/Logger.h"


Cookie::Cookie()
	:
	m_keyId(0)
{}


/**
* @brief	Creates a Cookie as a tuple of the passed parameters. The parameters are copied here.
*
* @param [in]	keyId				The identifier of the master key.
* @param [in]	encCookieContent	The encrypted content of the cookie.
*/
Cookie::Cookie(unsigned short keyId, std::vector<unsigned char> const & nonce, std::vector<unsigned char> const & encCookieContent)
{
	this->setKeyid(keyId);
	this->setNonce(nonce);
	this->setEncCookieContent(encCookieContent);
}


/**
* @brief	Creates a Cookie by parsing a binary data stream.
*/
Cookie::Cookie(std::vector<unsigned char> const & serializedData, unsigned int nonceLength)
{
	this->fromSerializedData(serializedData, nonceLength);
}


unsigned short Cookie::getKeyId() const
{
	return m_keyId;
}


void Cookie::setKeyid(unsigned short keyId)
{
	this->m_keyId = keyId;
}


size_t Cookie::getNonceLength() const
{
	return m_nonce.size();
}


const std::vector<unsigned char>& Cookie::getNonce() const
{
	return m_nonce;
}


/**
* @brief	Sets the Nonce as a copy of the passed parameter.
*/
void Cookie::setNonce(std::vector<unsigned char> const & nonce)
{
	this->m_nonce = nonce;
}


size_t Cookie::getEncCookieContentLength() const
{
	return m_encCookieContent.size();
}


const std::vector<unsigned char>& Cookie::getEncCookieContent() const
{
	return m_encCookieContent;
}


/**
* @brief	Sets the encrypted Cookie content as a copy of the passed parameter.
*/
void Cookie::setEncCookieContent(std::vector<unsigned char> const & encCookieContent)
{
	this->m_encCookieContent = encCookieContent;
}


/**
* @brief	Serializes the Cookie into a binary data stream.
*/
std::vector<unsigned char> Cookie::getSerializedData() const
{
	// write Key ID
	std::vector<unsigned char> serializedData(2);
	serializedData.at(0) |= (m_keyId >> 8) & 0xFF;
	serializedData.at(1) |= m_keyId & 0xFF;

	// write nonce
	serializedData.insert(serializedData.end(), m_nonce.begin(), m_nonce.end());

	// write encrypted cookie
	serializedData.insert(serializedData.end(), m_encCookieContent.begin(), m_encCookieContent.end());

	return serializedData;
}


/**
* @brief	Parses the given binary stream and sets the values of this Cookie object.
*
* @param [in]	nonceLength		The length of the nonce in the data stream (set by the server).
*/
void Cookie::fromSerializedData(std::vector<unsigned char> const & serializedData, unsigned int nonceLength)
{
	if (serializedData.size() < (2 + nonceLength + 1)) // 2 bytes (key ID) + nonce size + cookie length (min. 1 byte)
	{
		LOG_ERROR("invalid size of the data stream");
		return;
	}

	// parse binary stream
	unsigned short keyId = ((serializedData.at(0) & 0xFF) << 8) | serializedData.at(1);
	std::vector<unsigned char> nonce(serializedData.begin() + 2, serializedData.begin() + 2 + nonceLength);
	std::vector<unsigned char> encCookieContent(serializedData.begin() + 2 + nonceLength, serializedData.end());

	// save data
	m_keyId = keyId;
	m_nonce = std::move(nonce);
	m_encCookieContent = std::move(encCookieContent);
}


/**
* @brief	Checks if the content is complete and valid.
*/
bool Cookie::isValid()
{
	bool status = true;

	if (m_keyId == 0)  // undefined
	{
		LOG_ERROR("Key ID is not defined");
		status = false;
	}

	if (m_nonce.size() == 0)
	{
		LOG_ERROR("the nonce length is zero");
		status = false;
	}

	if (m_encCookieContent.size() == 0)
	{
		LOG_ERROR("the length of the 'encrypted cookie' is zero");
		status = false;
	}

	return status;
}


/**
* @brief	Deletes the content of the Cookie.
*/
void Cookie::clear()
{
	m_keyId = 0;
	m_nonce.clear();
	m_encCookieContent.clear();
}
