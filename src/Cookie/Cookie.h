/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#pragma once

#include <vector>
#include <stdlib.h>

class Cookie
{
public:
	Cookie();
	Cookie(unsigned short keyId, std::vector<unsigned char> const &nonce, std::vector<unsigned char> const &encCookieContent);
	Cookie(std::vector<unsigned char> const &serializedData, unsigned int nonceLength);
	~Cookie() = default;

	// Key ID
	unsigned short getKeyId() const;
	void setKeyid(unsigned short keyId);

	// Nonce
	size_t getNonceLength() const;
	const std::vector<unsigned char> &getNonce() const;
	void setNonce(std::vector<unsigned char> const &nonce);

	// encrypted Cookie content
	size_t getEncCookieContentLength() const;
	const std::vector<unsigned char> &getEncCookieContent() const;
	void setEncCookieContent(std::vector<unsigned char> const &encCookieContent);

	// Misc
	std::vector<unsigned char> getSerializedData() const;
	void fromSerializedData(std::vector<unsigned char> const &serializedData, unsigned int nonceLength);
	bool isValid();
	void clear();

private:
	unsigned short m_keyId;
	std::vector<unsigned char> m_nonce;
	std::vector<unsigned char> m_encCookieContent;
};

