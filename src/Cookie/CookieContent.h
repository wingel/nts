/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences in cooperation
 *                with Physikalisch-Technische Bundesanstalt (PTB), Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */
#pragma once

#include <vector>
#include "../NtsConfig.h"

class CookieContent
{
public:
	CookieContent();
	CookieContent(AeadAlgorithm aeadAlgorithm, std::vector<unsigned char> const &s2cKey, std::vector<unsigned char> const &c2sKey);
	CookieContent(unsigned short aeadAlgorithm, std::vector<unsigned char> const &s2cKey, std::vector<unsigned char> const &c2sKey);
	CookieContent(std::vector<unsigned char> const &serializedData);
	~CookieContent() = default;

	// AEAD algorithm
	unsigned short getAeadAlgorithmVal() const;
	AeadAlgorithm getAeadAlgorithm() const;
	void setAeadAlgorithm(unsigned short aeadAlgorithm);
	void setAeadAlgorithm(AeadAlgorithm aeadAlgorithm);

	// S2C Key (Server-to-Client)
	unsigned short getS2CKeyLength() const;
	const std::vector<unsigned char> &getS2CKey() const;
	void setS2CKey(std::vector<unsigned char> const &s2cKey);

	// C2S Key (Client-to-Server)
	unsigned short getC2SKeyLength() const;
	const std::vector<unsigned char> &getC2SKey() const;
	void setC2SKey(std::vector<unsigned char> const &c2sKey);

	// Misc
	std::vector<unsigned char> getSerializedData() const;
	void fromSerializedData(std::vector<unsigned char> const &serializedData);
	bool isValid();
	void clear();

private:
	unsigned short m_aeadAlgorithm;
	std::vector<unsigned char> m_S2CKey;
	std::vector<unsigned char> m_C2SKey;
};

