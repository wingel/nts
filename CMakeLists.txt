cmake_minimum_required(VERSION 3.8)
project(nts VERSION 0.5.0 LANGUAGES CXX) #TODO: auto-generate version.h

option(NTS_STATIC_BUILD "Build NTS with all dependencies statically linked" OFF)

set(BOOST_ROOT /opt/boost-1.69)
set(OPENSSL_ROOT_DIR /opt/openssl-1.1.1a)
# set(NTS_STATIC_BUILD ON)

set(LIBAES_SIV_STATIC_BUILD ${NTS_STATIC_BUILD} CACHE BOOL "" FORCE)
add_subdirectory(3rdparty/libaes_siv-1.0.0)
add_subdirectory(3rdparty/spdlog)
add_subdirectory(3rdparty/rang-3.1.0)

add_subdirectory(src)
